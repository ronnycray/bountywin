import hashlib
import base64
import json
import requests
from datetime import datetime
import calendar
import string
from random import *
import hmac
import secrets


def get_generate_signature():
    pass


def create_invoice(order_amount: int):
    idempotency_key = str(secrets.token_hex(10))

    body = {
        "amount": order_amount,
        "complete_payment_url": "https://bountywin.com/api/successfully_payment/",
        "country": "RU",
        "currency": "RUB",
        "error_payment_url": "https://bountywin.com/api/fail/",
        "language": "ru"
    }

    body_for_headers = json.dumps(body, separators=(',', ':'))

    http_method = 'post'
    path = '/v1/checkout'

    # salt: randomly generated for each request.
    min_char = 8
    max_char = 12
    allchar = string.ascii_letters + string.punctuation + string.digits
    salt = "".join(choice(allchar) for x in range(randint(min_char, max_char)))

    # Current Unix time.
    d = datetime.utcnow()
    timestamp = calendar.timegm(d.utctimetuple())

    access_key = 'CCBB9577410A8982266C'  # The access key received from Rapyd.
    secret_key = '37b40825145e6871e22d5a224b54bccded7d79ad16b9f6d5d63e8ea234c10085b079ad954f26625c'  # Never transmit the secret key by itself.

    to_sign = http_method + path + salt + str(timestamp) + access_key + secret_key + str(body_for_headers)

    h = hmac.new(bytes(secret_key, 'utf-8'), bytes(to_sign, 'utf-8'), hashlib.sha256)

    signature = base64.urlsafe_b64encode(str.encode(h.hexdigest()))

    headers = {
        "access_key": access_key,
        "Content-Type": 'application/json',
        "salt": salt,
        "signature": signature,
        "timestamp": str(timestamp),
        "idempotency": idempotency_key
    }

    result = requests.post(url=f'https://sandboxapi.rapyd.net/v1/checkout', headers=headers, json=body)
    print(result)
    print(result.text)
    # print(result.json())


def get_countries():
    idempotency_key = str(secrets.token_hex(10))
    body = ""

    http_method = 'get'
    path = '/v1/data/countries'

    # salt: randomly generated for each request.
    min_char = 8
    max_char = 12
    allchar = string.ascii_letters + string.punctuation + string.digits
    salt = "".join(choice(allchar) for x in range(randint(min_char, max_char)))

    # Current Unix time.
    d = datetime.utcnow()
    timestamp = calendar.timegm(d.utctimetuple())

    access_key = 'CCBB9577410A8982266C'  # The access key received from Rapyd.
    secret_key = '37b40825145e6871e22d5a224b54bccded7d79ad16b9f6d5d63e8ea234c10085b079ad954f26625c'  # Never transmit the secret key by itself.

    to_sign = http_method + path + salt + str(timestamp) + access_key + secret_key + str(body)

    h = hmac.new(bytes(secret_key, 'utf-8'), bytes(to_sign, 'utf-8'), hashlib.sha256)

    signature = base64.urlsafe_b64encode(str.encode(h.hexdigest()))

    headers = {
        "access_key": access_key,
        "Content-Type": 'application/json',
        "salt": salt,
        "signature": signature,
        "timestamp": str(timestamp),
        "idempotency": idempotency_key
    }

    result = requests.get(url='https://sandboxapi.rapyd.net/v1/data/countries', headers=headers)
    print(result.json())


def create_invoice_new():
    pass


if __name__ == '__main__':
    create_invoice(order_amount=19)
