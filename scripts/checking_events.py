import requests
import schedule
import time
import json


domain = ""


def check():
    header = {"Authorization": "123123%1231231#23123!@12312312"}
    response = requests.get(url=f'{domain}/api/check_events_finished/', headers=header)
    print(response)


if __name__ == '__main__':
    schedule.every(30).seconds.do(check)

    while 1:
        schedule.run_pending()
        time.sleep(1)
