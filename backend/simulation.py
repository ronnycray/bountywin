import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()

from django.utils import timezone
from django.db.models import Q
from app.models import (
    Users, Tickets, Simulation, Answers, StepSimulation
)

import logging
from random import randrange
import json
import schedule
import time

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


def simulation():
    all_simulation = StepSimulation.objects.all()
    setting_simulation = Simulation.objects.get(pk=1)
    for event_simulation in all_simulation:
        now = timezone.datetime.now()
        if event_simulation.create_at + timezone.timedelta(minutes=setting_simulation.time_simulation) >= now:
            time.sleep(randrange(start=50, stop=120))
            quantity_users = randrange(start=setting_simulation.random_start, stop=setting_simulation.random_end)
            users = Users.objects.filter(group__short_name='simulators')
            if users:
                is_ready = []
                for cnt_quantity in range(quantity_users):
                    user = users[randrange(len(users) - 1)]
                    while user.id in is_ready:
                        user = users[randrange(len(users) - 1)]

                    is_ready.append(user.id)
                    tic = Tickets.objects.filter(Q(sell=False) & Q(booking=False) & Q(event=event_simulation.event.pk))
                    if tic:
                        tic = tic.first()
                        answer = Answers.objects.filter(Q(pk=tic.event.id) & Q(is_true=False)).first()
                        tic.sell = True
                        tic.answer = answer
                        tic.user = user
                        tic.save()
        else:
            event_simulation.delete()


if __name__ == '__main__':
    schedule.every(5).seconds.do(simulation)

    while 1:
        schedule.run_pending()
        time.sleep(1)
