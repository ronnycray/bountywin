# Generated by Django 3.1.7 on 2021-03-16 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0026_auto_20210311_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='referral_link',
            field=models.CharField(default='', max_length=100, verbose_name='Реф.ссылка'),
        ),
    ]
