# Generated by Django 3.1.7 on 2021-03-11 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_remove_users_company'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='texts',
            options={'verbose_name': 'Статическая страница', 'verbose_name_plural': 'Статические страницы'},
        ),
        migrations.AlterField(
            model_name='order',
            name='amount',
            field=models.FloatField(default=0, verbose_name='Сумма'),
        ),
    ]
