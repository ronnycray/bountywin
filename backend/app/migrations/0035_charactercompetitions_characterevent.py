# Generated by Django 3.1.7 on 2021-04-23 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0034_auto_20210409_2004'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterCompetitions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Характеристика компетенции')),
                ('image', models.ImageField(default='', upload_to='media/', verbose_name='Картинка')),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.competitions', verbose_name='Компетенция')),
            ],
            options={
                'verbose_name': 'Характеристика компетенций',
                'verbose_name_plural': 'Характеристика компетенций',
            },
        ),
        migrations.CreateModel(
            name='CharacterEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(default='', max_length=100, verbose_name='Значние')),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.charactercompetitions', verbose_name='Параметр')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.events', verbose_name='Событие')),
            ],
            options={
                'verbose_name': 'Параметр конкурса',
                'verbose_name_plural': 'Параметры конкурса',
            },
        ),
    ]
