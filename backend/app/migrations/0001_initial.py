# Generated by Django 3.1.7 on 2021-02-27 07:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.CharField(default='', max_length=50, verbose_name='Ответ')),
                ('is_true', models.BooleanField(default=False, verbose_name='Это верный ответ')),
            ],
            options={
                'verbose_name': 'Ответ',
                'verbose_name_plural': 'Ответы',
            },
        ),
        migrations.CreateModel(
            name='Codes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(default='', max_length=100, unique=True, verbose_name='Код')),
                ('active', models.BooleanField(default=False, verbose_name='Активен')),
            ],
            options={
                'verbose_name': 'Код',
                'verbose_name_plural': 'Коды',
            },
        ),
        migrations.CreateModel(
            name='Competitions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=200, verbose_name='Имя компетенции')),
                ('description', models.TextField(default='', max_length=2000, verbose_name='Описание компетенции')),
            ],
            options={
                'verbose_name': 'Компатенция',
                'verbose_name_plural': 'Компетенции',
            },
        ),
        migrations.CreateModel(
            name='Coupons',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Имя купона')),
                ('code', models.CharField(default='', max_length=10, verbose_name='Код')),
                ('discount_percent', models.IntegerField(default=1, verbose_name='Скидка в %')),
                ('discount_amount', models.IntegerField(default=100, verbose_name='Скидка в Р')),
                ('enable', models.BooleanField(default=True, verbose_name='Активна')),
            ],
            options={
                'verbose_name': 'Купон',
                'verbose_name_plural': 'Купоны',
            },
        ),
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=300, verbose_name='Имя события')),
                ('description', models.TextField(default='', max_length=5000, verbose_name='Описание события')),
                ('price', models.FloatField(default=0, verbose_name='Цена участия')),
                ('percent_change', models.IntegerField(default=0, verbose_name='Процент скидки')),
                ('time_end_event', models.DateTimeField(verbose_name='Дата окончания события')),
                ('ticket', models.IntegerField(default=50, verbose_name='Кол-во билетов')),
                ('limit', models.IntegerField(default=20, verbose_name='Кол-во билетов для отклчения скидки')),
                ('limit_person', models.IntegerField(default=1, verbose_name='Кол-во билетов на человека')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата начала события')),
                ('bhp', models.CharField(default='', max_length=100, verbose_name='Мощность двигателя')),
                ('speed', models.CharField(default='', max_length=100, verbose_name='Скорость')),
                ('mpg', models.CharField(default='', max_length=100, verbose_name='Расход')),
                ('miles', models.CharField(default='', max_length=100, verbose_name='Расстояние')),
                ('year', models.CharField(default='', max_length=100, verbose_name='Год')),
                ('sold', models.BooleanField(default=False, verbose_name='Продано')),
                ('finished', models.BooleanField(default=False, verbose_name='Розыгрыш окончен')),
                ('question', models.CharField(default='', max_length=100, verbose_name='Вопрос')),
                ('competition', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.competitions', verbose_name='Компетенция')),
            ],
            options={
                'verbose_name': 'Событие',
                'verbose_name_plural': 'События',
            },
        ),
        migrations.CreateModel(
            name='Regions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50, verbose_name='Город')),
            ],
            options={
                'verbose_name': 'Город',
                'verbose_name_plural': 'Города',
            },
        ),
        migrations.CreateModel(
            name='StaticInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('winners', models.CharField(default='', max_length=100, verbose_name='Победителей')),
                ('prizes', models.CharField(default='', max_length=100, verbose_name='Призов')),
                ('charity', models.CharField(default='', max_length=100, verbose_name='На благотворительности')),
                ('followers', models.CharField(default='', max_length=100, verbose_name='Подписчиков')),
            ],
            options={
                'verbose_name': 'Инфо',
                'verbose_name_plural': 'Инфо',
            },
        ),
        migrations.CreateModel(
            name='Texts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(default='', max_length=100, verbose_name='Ключ')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Имя')),
                ('text', models.CharField(default='', max_length=1000, verbose_name='Текст')),
            ],
            options={
                'verbose_name': 'Текст',
                'verbose_name_plural': 'Текста',
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(default='', max_length=100, verbose_name='Username')),
                ('email', models.CharField(default='', max_length=100, unique=True, verbose_name='Email')),
                ('password', models.CharField(default='', max_length=100, verbose_name='Пароль')),
                ('cash_back', models.IntegerField(default=0, verbose_name='Кеш-бек поинты')),
                ('first_name', models.CharField(default='', max_length=50, verbose_name='Имя')),
                ('second_name', models.CharField(default='', max_length=50, verbose_name='Фамилия')),
                ('company', models.CharField(default=0, max_length=50, verbose_name='Имя')),
                ('address', models.CharField(default='', max_length=50, verbose_name='Адресс')),
                ('postcode', models.CharField(default='', max_length=50, verbose_name='Код доставки')),
                ('phone', models.CharField(default='', max_length=50, verbose_name='Телефон')),
                ('day_birth', models.DateTimeField(default='', verbose_name='День рождение')),
                ('photo', models.ImageField(default='', upload_to='media/', verbose_name='Фото')),
                ('active', models.BooleanField(default=False, verbose_name='Подтвержденный аккаунт')),
                ('city', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.regions', verbose_name='Страна')),
            ],
            options={
                'verbose_name': 'Пользователь',
                'verbose_name_plural': 'Пользователи',
            },
        ),
        migrations.CreateModel(
            name='Tickets',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField(default=0, verbose_name='Порядковое число билета')),
                ('sell', models.BooleanField(default=False, verbose_name='Продан')),
                ('create_at', models.DateTimeField(verbose_name='Время покупки')),
                ('answer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.answers', verbose_name='Ответ')),
                ('event', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.events', verbose_name='Событие')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Билет',
                'verbose_name_plural': 'Билеты',
            },
        ),
        migrations.CreateModel(
            name='Passwords',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(default='', max_length=100, verbose_name='Новый пароль')),
                ('code_activate', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.codes', verbose_name='Код для активации')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Пользвоатель')),
            ],
            options={
                'verbose_name': 'Пароль',
                'verbose_name_plural': 'Пароли',
            },
        ),
        migrations.CreateModel(
            name='LastWinner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='media/', verbose_name='Фото')),
                ('content', models.TextField(default='', max_length=1000, verbose_name='Текст')),
                ('is_top', models.BooleanField(default=True, verbose_name='Лучший выйгрыш')),
                ('event', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.events', verbose_name='Событие')),
            ],
            options={
                'verbose_name': 'Победитель',
                'verbose_name_plural': 'Победители',
            },
        ),
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='media/', verbose_name='Фото')),
                ('main_image', models.BooleanField(default=False, verbose_name='Главное фото')),
                ('event', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.events', verbose_name='Событие')),
            ],
            options={
                'verbose_name': 'Фото',
                'verbose_name_plural': 'Фото',
            },
        ),
        migrations.AddField(
            model_name='events',
            name='winner',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Победитель'),
        ),
        migrations.CreateModel(
            name='Countries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Название страны')),
                ('region', models.ManyToManyField(to='app.Regions')),
            ],
            options={
                'verbose_name': 'Страна',
                'verbose_name_plural': 'Страны',
            },
        ),
        migrations.AddField(
            model_name='codes',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Пользвоатель'),
        ),
        migrations.AddField(
            model_name='answers',
            name='event',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.events', verbose_name='Событие'),
        ),
    ]
