# Generated by Django 3.1.7 on 2021-03-04 18:25

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20210303_1821'),
    ]

    operations = [
        migrations.CreateModel(
            name='Gifts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Название')),
                ('description', models.CharField(default='', max_length=1000, verbose_name='Описание')),
                ('amount', models.IntegerField(default=0, verbose_name='Цена')),
                ('bonus', models.IntegerField(default=0, verbose_name='Бонус')),
            ],
            options={
                'verbose_name': 'Подарочный набор',
                'verbose_name_plural': 'Подарочные наборы',
            },
        ),
        migrations.AddField(
            model_name='coupons',
            name='disposable',
            field=models.BooleanField(default=False, verbose_name='Одноразовый'),
        ),
        migrations.AlterField(
            model_name='users',
            name='day_birth',
            field=models.DateField(default=datetime.datetime(2021, 3, 4, 18, 25, 56, 583560, tzinfo=utc), verbose_name='День рождение'),
        ),
        migrations.AlterField(
            model_name='users',
            name='photo',
            field=models.ImageField(blank=True, default='', upload_to='media/', verbose_name='Фото'),
        ),
        migrations.CreateModel(
            name='UserGifts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(default='', max_length=100, verbose_name='Код')),
                ('gift', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.gifts', verbose_name='Подарочный купон')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Подарочная карта пользователя',
                'verbose_name_plural': 'Подарочные карты пользвоателей',
            },
        ),
        migrations.CreateModel(
            name='GiftOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(default=0, verbose_name='Сумма')),
                ('payment', models.BooleanField(default=False, verbose_name='Оплачено')),
                ('hash_token', models.CharField(default='', max_length=1000, verbose_name='Секретный токен')),
                ('gift', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.gifts', verbose_name='Подарочный купон')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='app.users', verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
        ),
    ]
