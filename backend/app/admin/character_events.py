from django.contrib import admin
from ..models import CharacterEvent


@admin.register(CharacterEvent)
class CharacterEventAdminForm(admin.ModelAdmin):
    list_display = ("event", "character", "value",)
    list_display_links = ("event", "character", "value",)
