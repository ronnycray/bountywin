from django.contrib import admin
from ..models import StepSimulation


@admin.register(StepSimulation)
class StepSimulationAdminForm(admin.ModelAdmin):
    list_display = ("event", "create_at",)
    list_display_links = ("event", "create_at",)
