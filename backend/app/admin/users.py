from django.contrib import admin
from ..models.users import Users


@admin.register(Users)
class UsersAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "email", "create_at",)
    list_display_links = ("id", "username", "email", "create_at",)
    list_filter = ("id", "username", "email",)
    exclude = ("password",)
    search_fields = ("username", "email")
