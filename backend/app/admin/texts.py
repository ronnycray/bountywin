from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from ..models.texts import Texts
from django import forms


class TextsAdminForm(forms.ModelForm):
    text = forms.CharField(label="Текст", widget=CKEditorUploadingWidget())

    class Meta:
        model = Texts
        fields = '__all__'


@admin.register(Texts)
class TextsAdmin(admin.ModelAdmin):
    list_display = ("name", "key",)
    list_display_links = ("name", "key",)
    form = TextsAdminForm
