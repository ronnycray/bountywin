from django.contrib import admin
from ..models.tickets import Tickets


@admin.register(Tickets)
class TicketsAdmin(admin.ModelAdmin):
    list_display = ("event", "number", "user", "sell", "booking")
    list_display_links = ("event", "number", "user", "sell", "booking")
    list_filter = ("sell", "booking",)
    search_fields = ("event__name", "user__username", "user__email")
