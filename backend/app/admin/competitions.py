from django import forms
from django.contrib import admin
from ..models.competitions import Competitions
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ..models.character_competitions import CharacterCompetitions
from django.utils.safestring import mark_safe


class DescriptionAdminForm(forms.ModelForm):
    description = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())

    class Meta:
        model = Competitions
        fields = '__all__'


class CharactersAdminInlines(admin.TabularInline):
    fields = ("competition", "name", "get_image",)
    readonly_fields = ("get_image",)
    model = CharacterCompetitions
    extra = 0
    min_num = 0

    def get_image(self, obj):
        character = CharacterCompetitions.objects.filter(pk=obj.pk)
        out = '-'
        if character:
            character = character.first()
            out = f'<img src={character.image.url} width="100" height="100">'
        return mark_safe(out)

    get_image.short_description = 'Картинка'


@admin.register(Competitions)
class CompetitionsAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = ("name",)
    form = DescriptionAdminForm
    inlines = [CharactersAdminInlines]
