from django.contrib import admin
from ..models.coupons import Coupons


@admin.register(Coupons)
class CouponsAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "enable",)
    list_display_links = ("name", "code", "enable",)
