from django.contrib import admin
from ..models.gifts import Gifts
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class GiftsDescriptionForm(forms.ModelForm):
    description = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())

    class Meta:
        model = Gifts
        fields = '__all__'


@admin.register(Gifts)
class GiftsAdmin(admin.ModelAdmin):
    list_display = ("name", "amount", "bonus")
    list_display_links = ("name", "amount", "bonus")
    form = GiftsDescriptionForm

