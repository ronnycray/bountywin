from django.contrib import admin
from ..models.simulation import Simulation


@admin.register(Simulation)
class SimulationAdminForm(admin.ModelAdmin):
    list_display = ("time_simulation", "random_start", "random_end")
    list_display_links = ("time_simulation", "random_start", "random_end")
