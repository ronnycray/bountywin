from django.contrib import admin
from ..models.last_winner import LastWinner
from django.utils.safestring import mark_safe


@admin.register(LastWinner)
class LastWinnerAdmin(admin.ModelAdmin):
    list_display = ("event", "content",)
    list_display_links = ("event", "content",)
    readonly_fields = ("get_photo",)

    def get_photo(self, obj):
        print('obj', obj)
        out = f'<div><img src={obj.photo.url} width="200" height="200"></div>'
        return mark_safe(out)

    get_photo.short_description = "Фото"
