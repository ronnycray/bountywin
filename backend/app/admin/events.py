from django import forms
from django.db.models import Q
from django.contrib import admin
from ..models.events import Events
from ..models.images import Images
from ..models.answers import Answers
from ..models.tickets import Tickets
from ..models.character_events import CharacterEvent
from ..models.broadcasts import Broadcasts
from django.utils.safestring import mark_safe
from ckeditor_uploader.widgets import CKEditorUploadingWidget


class ImagesAdminsInline(admin.TabularInline):
    fields = ("photo", "main_image", "get_photo")
    readonly_fields = ("get_photo",)
    model = Images
    extra = 0
    min_num = 0

    def get_photo(self, obj):
        print('obj', obj)
        out = f'<div><div>{obj.photo.url}</div><img src={obj.photo.url} width="200" height="200"></div>'
        return mark_safe(out)

    get_photo.short_description = "Фото"


class CharacterEventsInline(admin.TabularInline):
    fields = ("character", "value")
    model = CharacterEvent
    extra = 0
    min_num = 0


class DescriptionAdminForm(forms.ModelForm):
    description = forms.CharField(label="Описание", widget=CKEditorUploadingWidget())

    class Meta:
        model = Events
        fields = '__all__'


class AnswersAdminInlines(admin.TabularInline):
    fields = ("answer", "is_true",)
    model = Answers
    extra = 0
    min_num = 0


class BroadcastsAdminInlines(admin.TabularInline):
    fields = ("link", "live",)
    model = Broadcasts
    extra = 0
    can_delete = False
    min_num = 0


@admin.register(Events)
class EventsAdmin(admin.ModelAdmin):
    list_display = ("name", "price", "ticket", "count_sold", "sold", "finished")
    list_display_links = ("name", "price", "ticket", "count_sold", "sold", "finished")
    raw_id_fields = ("winner",)
    list_filter = ("sold", "finished", "simulation",)
    readonly_fields = ("get_list_winners", "get_photos",)
    exclude = ("doc",)
    search_fields = ("name", "winner")
    inlines = [AnswersAdminInlines, CharacterEventsInline, ImagesAdminsInline, BroadcastsAdminInlines, ]
    form = DescriptionAdminForm

    def count_sold(self, obj):
        tickets = Tickets.objects.filter(Q(event=obj.pk) & Q(sell=True)).count()
        return tickets

    def get_photos(self, obj):
        out = '<div class="">{}</div>'
        photo_out = ''
        print('Images.objects.filter(event=obj.pk), ', Images.objects.filter(event=obj.pk))
        print('obj', obj.pk)
        if obj.pk is not None:
            for photo in Images.objects.filter(event=obj.pk):
                photo_out += f'<div><img src={photo.photo.url} style="max-width: 100%; margin: 10px 0;"></div>'
            out = out.format(photo_out)

        if photo_out == "":
            return mark_safe('-')

        return mark_safe(out)

    def get_list_winners(self, obj):
        print(obj.finished)
        event = Events.objects.get(pk=obj.pk)
        if event.finished:
            print('event.doc.url', event.doc.url)
            print('event.doc.path', event.doc.path)
            out = f'<div><a href="{event.doc.url.replace("home/website/bountywin/backend/media/", "")}">Скачать список</a></div>'
        else:
            out = "-"
        return mark_safe(out)

    get_photos.short_description = "Фотографии события"
    get_list_winners.short_description = "Список участников"
    count_sold.short_description = "Продано билетов"
