from django.contrib import admin
from ..models.countries import Countries


@admin.register(Countries)
class CountriesAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = ("name",)
    filter_horizontal = ('region',)
