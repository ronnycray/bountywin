from .group_user import GroupUserAdminForm
from .users import UsersAdmin
from .competitions import CompetitionsAdmin
from .events import EventsAdmin
from .last_winner import LastWinnerAdmin
from .tickets import TicketsAdmin
from .static_info import StaticInfoAdmin
from .texts import TextsAdmin
from .regions import RegionsAdmin
from .countries import CountriesAdmin
from .coupons import CouponsAdmin
from .orders import OrderAdmin
from .configuration import ConfigurationAdmin
from .gifts import GiftsAdmin
from .orders_gifts import GiftOrderAdmin
from .user_gifts import UserGiftsAdmin
from .faq import FAQAdmin
# from .tickets_order import TicketsOrdersAdmin
from .history_points import HistoryPointsAdmin
from .character_events import CharacterEventAdminForm
from .character_competitions import CharacterCompetitionsAdminForm
from .simulation import SimulationAdminForm
from .step_simulation import StepSimulationAdminForm

from django.contrib import admin

admin.site.site_header = 'Административная панель BountyWins'
admin.site.site_title = 'Admin BountyWins'
