from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from ..models.faq import FAQ
from django import forms


class AnswerAdminForm(forms.ModelForm):
    answer = forms.CharField(label="Ответ", widget=CKEditorUploadingWidget())

    class Meta:
        model = FAQ
        fields = '__all__'


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ("question", "answer",)
    list_display_links = ("question", "answer",)
    form = AnswerAdminForm
