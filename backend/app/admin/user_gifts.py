from django.contrib import admin
from ..models.user_gifts import UserGifts


@admin.register(UserGifts)
class UserGiftsAdmin(admin.ModelAdmin):
    list_display = ("user", "gift", "code",)
    list_display_links = ("user", "gift", "code",)
    list_filter = ("user", "gift", "code",)
