from django.contrib import admin
from ..models.gifts_order import GiftOrder


@admin.register(GiftOrder)
class GiftOrderAdmin(admin.ModelAdmin):
    list_display = ("user", "amount", "payment", "gift",)
    list_display_links = ("user", "amount", "gift",)
    list_filter = ("amount",)
