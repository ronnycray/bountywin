from django.contrib import admin
from ..models.group_user import GroupUsers


@admin.register(GroupUsers)
class GroupUserAdminForm(admin.ModelAdmin):
    list_display = ("name_group", "short_name",)
    list_display_links = ("name_group", "short_name",)
