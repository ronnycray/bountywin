from django.contrib import admin
from ..models.configuration import Configuration


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):
    list_display = ("name", "value",)
    list_display_links = ("name", "value",)
