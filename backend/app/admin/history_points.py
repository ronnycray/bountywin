from django.contrib import admin
from ..models import HistoryPoints


@admin.register(HistoryPoints)
class HistoryPointsAdmin(admin.ModelAdmin):
    list_display = ("user", "points", "operation",)
    list_display_links = ("user", "points", "operation",)
