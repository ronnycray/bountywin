from django.contrib import admin
from ..models.tickets_orders import TicketsOrders


@admin.register(TicketsOrders)
class TicketsOrdersAdmin(admin.ModelAdmin):
    list_display = ("order", "ticket", "answer",)
    list_display_links = ("order", "ticket", "answer",)
