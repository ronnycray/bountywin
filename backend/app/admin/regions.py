from django.contrib import admin
from ..models.regions import Regions


@admin.register(Regions)
class RegionsAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_display_links = ('name',)
