from django.contrib import admin
from ..models.static_info import StaticInfo


@admin.register(StaticInfo)
class StaticInfoAdmin(admin.ModelAdmin):
    list_display = ("winners", "prizes", "charity", "followers",)
    list_display_links = ("winners", "prizes", "charity", "followers",)
