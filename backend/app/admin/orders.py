from django.contrib import admin
from ..models.orders import Order
from ..models.tickets_orders import TicketsOrders


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("user", "amount", "get_events", "payment",)
    list_display_links = ("user", "amount", "get_events")
    list_filter = ("payment", "cancel_payment",)

    def get_events(self, obj):
        out = ""
        list_event = []
        tickets_orders = TicketsOrders.objects.filter(pk=obj.pk)
        for ticket_order in tickets_orders:
            if ticket_order.ticket.event.name not in list_event:
                list_event.append(ticket_order.ticket.event.name)

        for event in list_event:
            if event != list_event[len(list_event) - 1]:
                out += f"{event}, "
            else:
                out += f"{event}"

        return out

    get_events.short_description = "Конкурсы"
