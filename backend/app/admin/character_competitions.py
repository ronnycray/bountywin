from ..models.character_competitions import CharacterCompetitions
from django.utils.safestring import mark_safe
from django.contrib import admin


@admin.register(CharacterCompetitions)
class CharacterCompetitionsAdminForm(admin.ModelAdmin):
    list_display = ("competition", "name", "get_image")
    list_display_links = ("competition", "name", "get_image")

    def get_image(self, obj):
        character = CharacterCompetitions.objects.get(pk=obj.pk)
        out = f'<img src={character.image.url} width="100" height="100">'
        return mark_safe(out)

    get_image.short_description = "Картинка"
