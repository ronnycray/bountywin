from django.db import models
from .users import Users
from .gifts import Gifts


class UserGifts(models.Model):
    user = models.ForeignKey(
        Users, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    gift = models.ForeignKey(
        Gifts, verbose_name="Подарочный купон", on_delete=models.SET_NULL, null=True
    )
    code = models.CharField(verbose_name='Код', max_length=100, default="")

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Подарочная карта пользователя'
        verbose_name_plural = 'Подарочные карты пользвоателей'
