from django.db import models
from .users import Users


class Codes(models.Model):
    user = models.ForeignKey(
        Users, verbose_name="Пользвоатель", on_delete=models.SET_NULL, null=True
    )
    code = models.CharField(verbose_name='Код', max_length=100, default="", unique=True)
    active = models.BooleanField(verbose_name='Активен', default=False)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = "Код"
        verbose_name_plural = 'Коды'
