from django.db import models
from .users import Users
from .competitions import Competitions
from django.db.models.signals import post_save
from django.dispatch import receiver
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


class Events(models.Model):
    name = models.CharField(verbose_name="Имя события", max_length=300, default="")
    description = models.TextField(verbose_name="Описание события", max_length=5000, default="")
    competition = models.ForeignKey(
        Competitions, verbose_name="Компетенция", on_delete=models.SET_NULL, null=True
    )
    price = models.FloatField(verbose_name='Цена участия', default=0)
    percent_change = models.IntegerField(verbose_name='Процент скидки', default=0)
    time_end_event = models.DateTimeField(verbose_name='Дата окончания события')
    ticket = models.IntegerField(verbose_name='Кол-во билетов', default=50)
    limit = models.IntegerField(verbose_name='Кол-во билетов для отклчения скидки', default=20)
    limit_person = models.IntegerField(verbose_name="Кол-во билетов на человека", default=1)
    create_at = models.DateTimeField(verbose_name="Дата начала события", auto_now_add=True)
    sold = models.BooleanField(verbose_name="Продано", default=False)
    winner = models.ForeignKey(
        Users, verbose_name="Победитель", on_delete=models.SET_NULL, null=True, blank=True
    )
    win_ticket = models.IntegerField(verbose_name='Номер выйгрышного билета', default=0, blank=True)
    finished = models.BooleanField(verbose_name='Розыгрыш окончен', default=False)
    question = models.CharField(verbose_name="Вопрос", max_length=100, default="")
    doc = models.FileField(blank=True, upload_to='media/list_files/')
    simulation = models.BooleanField(verbose_name="Симуляция покупки", default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        origin = self.__class__
        print('self.pk,', self.pk)
        print(origin)
        if self.pk is None:
            post_save.connect(receiver=create_event, sender=Events)
        super(Events, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = 'События'


@receiver(signal=post_save, sender=Events)
def create_event(sender, instance, created, **kwargs):
    logging.warning('create_event')
    if created:
        from .tickets import Tickets
        event = Events.objects.get(pk=int(instance.pk))
        for cnt in range(event.ticket):
            Tickets.objects.create(event=event, number=cnt + 1)
