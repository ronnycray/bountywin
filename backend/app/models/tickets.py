from django.db import models
from .users import Users
from .answers import Answers
from .events import Events
import time


class Tickets(models.Model):
    event = models.ForeignKey(
        Events, verbose_name="Событие", on_delete=models.CASCADE, null=True
    )
    number = models.IntegerField(verbose_name='Порядковое число билета', default=0)
    user = models.ForeignKey(
        Users, verbose_name="Пользователь", on_delete=models.SET_NULL, null=True, blank=True
    )
    answer = models.ForeignKey(
        Answers, verbose_name="Ответ", on_delete=models.SET_NULL, null=True, blank=True
    )
    sell = models.BooleanField(verbose_name='Продан', default=False)
    booking = models.BooleanField(verbose_name='В процессе оплаты', default=False)
    booking_time = models.FloatField(verbose_name='Время бронирования', default=0)
    create_at = models.DateTimeField(verbose_name='Время покупки', auto_now_add=True)

    def __str__(self):
        return str(self.number)

    def save(self, *args, **kwargs):
        print('self.booking', 'self.booking_time')
        print(self.booking, self.booking_time)
        # if self.pk is not None:
        #     if self.booking and self.booking_time == 0:
        #         self.booking_time = time.time()
        super(Tickets, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'
