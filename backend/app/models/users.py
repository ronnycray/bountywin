from django.db import models
from .regions import Regions
from .countries import Countries
from .group_user import GroupUsers
import secrets
from django.utils import timezone


class Users(models.Model):
    group = models.ForeignKey(
        GroupUsers, verbose_name='Группа пользователя', on_delete=models.SET_NULL, null=True
    )
    username = models.CharField(verbose_name="Username", max_length=100, default="", blank=False)
    email = models.CharField(verbose_name="Email", max_length=100, default="", blank=True, unique=True)
    password = models.CharField(verbose_name="Пароль", max_length=100, default="", blank=False)
    dream_points = models.IntegerField(verbose_name="Dream поинты", default=0)
    first_name = models.CharField(verbose_name='Имя', max_length=50, default="", blank=True)
    second_name = models.CharField(verbose_name='Фамилия', max_length=50, default="", blank=True)
    city = models.ForeignKey(
        Regions, verbose_name="Город", on_delete=models.SET_NULL, null=True, blank=True
    )
    country = models.ForeignKey(
        Countries, verbose_name="Страна", on_delete=models.SET_NULL, null=True, blank=True
    )
    address = models.CharField(verbose_name='Адресс', max_length=50, default="", blank=True)
    postcode = models.CharField(verbose_name='Код доставки', max_length=50, default="", blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=50, default="", blank=True)
    day_birth = models.DateField(verbose_name='День рождение', default=timezone.now, blank=True)
    create_at = models.DateTimeField(verbose_name="Дата регистрации", default=timezone.now)
    photo = models.ImageField(verbose_name='Фото', upload_to='media/', default="", blank=True)
    active = models.BooleanField(verbose_name="Подтвержденный аккаунт", default=False)
    referral_link = models.CharField(verbose_name='Реф.ссылка', max_length=100, default="", blank=True)
    invited = models.IntegerField(verbose_name='Пригласил людей', default=0, blank=True)

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        if self.pk is None:
            link = secrets.token_hex(25)
            is_have_link = Users.objects.filter(referral_link=link)
            while is_have_link:
                link = secrets.token_hex(25)
                is_have_link = Users.objects.filter(referral_link=link)

            self.referral_link = link
        super(Users, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = 'Пользователи'
