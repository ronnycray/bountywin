from django.db import models


class GroupUsers(models.Model):
    name_group = models.CharField(verbose_name="Имя группы", default="", max_length=100)
    short_name = models.CharField(verbose_name="Код группы", default="", max_length=100)

    def __str__(self):
        return self.name_group

    class Meta:
        verbose_name = "Группа пользователей"
        verbose_name_plural = "Группы пользователей"
