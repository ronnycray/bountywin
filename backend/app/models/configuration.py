from django.db import models


class Configuration(models.Model):
    key = models.CharField(verbose_name='Ключ', max_length=100, default="")
    name = models.CharField(verbose_name='Название', max_length=100, default="")
    value = models.CharField(verbose_name='Значение', max_length=1000, default="")

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = 'Конфигурация'
        verbose_name_plural = 'Конфигурации'
