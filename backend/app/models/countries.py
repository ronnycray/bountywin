from django.db import models
from .regions import Regions


class Countries(models.Model):
    name = models.CharField(verbose_name="Название страны", max_length=100, default="", blank=False)
    region = models.ManyToManyField(Regions)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Страна"
        verbose_name_plural = "Страны"
