from django.db import models
from .events import Events


class Broadcasts(models.Model):
    event = models.ForeignKey(
        Events, verbose_name='Событие', on_delete=models.SET_NULL, null=True, blank=False
    )
    link = models.CharField(verbose_name='Ссылка на трансляцию', max_length=2000, default="", blank=False)
    live = models.BooleanField(verbose_name="Прямая трансляция", default=True)

    def __str__(self):
        return self.link

    class Meta:
        verbose_name = 'Транслация'
        verbose_name_plural = 'Трансляции'
