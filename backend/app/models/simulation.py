from django.db import models
from django.utils import timezone


class Simulation(models.Model):
    time_simulation = models.IntegerField(verbose_name='Время симуляции', default=10,
                                          help_text='Указывать в минутах')
    random_start = models.IntegerField(verbose_name='Старт рандомного диапазона', default=1)
    random_end = models.IntegerField(verbose_name='Конец рандомного диапазона', default=5)
    create_at = models.DateTimeField(verbose_name='Создана', default=timezone.now)
    last_update = models.DateTimeField(verbose_name='Последнее обновление', default=timezone.now)

    def __str__(self):
        return str(self.time_simulation)

    class Meta:
        verbose_name = "Настройки симуляции"
        verbose_name_plural = 'Настройки симуляции'
