from django.db import models
from .orders import Order
from .answers import Answers
from .tickets import Tickets


class TicketsOrders(models.Model):
    order = models.ForeignKey(
        Order, verbose_name='Заказ', on_delete=models.CASCADE, null=True
    )
    ticket = models.ForeignKey(
        Tickets, verbose_name='Билет', on_delete=models.CASCADE, null=True
    )
    answer = models.ForeignKey(
        Answers, verbose_name='Ответ', on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Билет заказа'
        verbose_name_plural = 'Билеты заказа'
