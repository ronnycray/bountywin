from django.db import models


class FAQ(models.Model):
    question = models.CharField(verbose_name='Вопрос', max_length=100, default="")
    answer = models.TextField(verbose_name="Ответ", max_length=1000, default="")

    def __str__(self):
        return self.answer

    class Meta:
        verbose_name = "FAQ"
        verbose_name_plural = 'FAQ'
