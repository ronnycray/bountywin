from django.db import models
from .events import Events


class Answers(models.Model):
    event = models.ForeignKey(
        Events, verbose_name="Событие", on_delete=models.SET_NULL, null=True
    )
    answer = models.CharField(verbose_name='Ответ', max_length=50, default="")
    is_true = models.BooleanField(verbose_name='Это верный ответ', default=False)

    def __str__(self):
        return self.answer

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'
