from django.db import models
from .users import Users
from django.utils import timezone


class HistoryPoints(models.Model):
    class Operations(models.TextChoices):
        earned = 'earned', ('Заработал')
        buy = 'buy', ('Приобрел')
        spent = 'spent', ('Потратил')
        birthday = 'birthday', ('День рождение')
        cash_back = 'cash_back', ('Кэш-бек')
    user = models.ForeignKey(
        Users, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    points = models.IntegerField(verbose_name='Кол-во поинтов', default=0)
    operation = models.CharField(verbose_name='Операция', choices=Operations.choices,
                                 default=Operations.cash_back, max_length=100)
    create_at = models.DateTimeField(verbose_name='Время', default=timezone.now)

    def __str__(self):
        return self.operation

    class Meta:
        verbose_name = "История PowerPoint"
        verbose_name_plural = 'История PowerPoint'
