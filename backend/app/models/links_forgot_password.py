from django.db import models
from .users import Users


class LinksForgotPassword(models.Model):
    user = models.ForeignKey(
        Users, verbose_name='Пользвоатель', on_delete=models.CASCADE, null=True, default=None
    )
    code = models.CharField(verbose_name='Код', default="", max_length=100)

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'Код'
        verbose_name_plural = 'Коды'
