from django.db import models
from .events import Events


class Images(models.Model):
    event = models.ForeignKey(
        Events, verbose_name="Событие", on_delete=models.SET_NULL, null=True
    )
    photo = models.ImageField(verbose_name='Фото', upload_to='media/')
    main_image = models.BooleanField(verbose_name="Главное фото", default=False)

    def __str__(self):
        return self.photo.url

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фото'
