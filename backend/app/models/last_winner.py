from django.db import models
from .events import Events


class LastWinner(models.Model):
    event = models.ForeignKey(
        Events, verbose_name="Событие", on_delete=models.SET_NULL, null=True
    )
    photo = models.ImageField(verbose_name="Фото", upload_to='media/')
    content = models.TextField(verbose_name='Текст', default="", max_length=1000)
    is_top = models.BooleanField(verbose_name='Лучший выйгрыш', default=True)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Победитель'
        verbose_name_plural = 'Победители'
