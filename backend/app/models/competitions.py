from django.db import models


class Competitions(models.Model):
    name = models.CharField(verbose_name="Имя компетенции", max_length=200, default="")
    description = models.TextField(verbose_name="Описание компетенции", max_length=2000, default="")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Компатенция"
        verbose_name_plural = 'Компетенции'
