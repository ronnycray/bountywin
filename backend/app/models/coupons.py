from django.db import models


class Coupons(models.Model):
    name = models.CharField(verbose_name="Имя купона", max_length=100, default="", blank=False)
    code = models.CharField(verbose_name="Код", max_length=10, default="", blank=False)
    discount_percent = models.IntegerField(verbose_name="Скидка в %", default=0)
    discount_amount = models.IntegerField(verbose_name="Скидка в Р", default=0)
    enable = models.BooleanField(verbose_name='Активна', default=True)
    disposable = models.BooleanField(verbose_name="Одноразовый", default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Купон"
        verbose_name_plural = 'Купоны'
