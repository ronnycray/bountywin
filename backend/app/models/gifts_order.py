from django.db import models
from .users import Users
from .gifts import Gifts


class GiftOrder(models.Model):
    user = models.ForeignKey(
        Users, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    gift = models.ForeignKey(
        Gifts, verbose_name="Подарочный купон", on_delete=models.SET_NULL, null=True
    )
    amount = models.IntegerField(verbose_name='Сумма', default=0, blank=False)
    payment = models.BooleanField(verbose_name="Оплачено", default=False)
    hash_token = models.CharField(verbose_name='Секретный токен', default="", max_length=1000)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Заказ карточек'
        verbose_name_plural = 'Заказы карточек'
