from django.db import models


class StaticInfo(models.Model):
    winners = models.CharField(verbose_name="Победителей", max_length=100, default="")
    prizes = models.CharField(verbose_name='Призов', max_length=100, default="")
    charity = models.CharField(verbose_name="Cashback", max_length=100, default="")
    followers = models.CharField(verbose_name="Подписчиков", max_length=100, default="")

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Инфо'
        verbose_name_plural = 'Инфо'
