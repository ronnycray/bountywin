from .character_competitions import CharacterCompetitions
from django.db import models
from .events import Events


class CharacterEvent(models.Model):
    event = models.ForeignKey(
        Events, verbose_name="Событие", on_delete=models.CASCADE, null=False
    )
    character = models.ForeignKey(
        CharacterCompetitions, verbose_name='Параметр', on_delete=models.CASCADE, null=False
    )
    value = models.CharField(verbose_name="Значние", default="", max_length=100)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = "Параметр конкурса"
        verbose_name_plural = "Параметры конкурса"
