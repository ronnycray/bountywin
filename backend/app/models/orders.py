from django.db import models
from .users import Users
from .coupons import Coupons
from django.utils import timezone


class Order(models.Model):
    class StatusOrder(models.TextChoices):
        processing = 'processing', ('В процессе')
        cancel = 'cancel', ('Отменено')
        success = 'success', ('Оплачен')

    user = models.ForeignKey(
        Users, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    amount = models.FloatField(verbose_name='Сумма', default=0, blank=False)
    payment = models.BooleanField(verbose_name="Оплачено", default=False)
    status = models.CharField(verbose_name='Статус', choices=StatusOrder.choices, max_length=100,
                              default=StatusOrder.processing)
    cancel_payment = models.BooleanField(verbose_name="Отменено", default=False)
    checkout_id = models.CharField(verbose_name='Checkout ID', default="", max_length=1000)
    points = models.IntegerField(verbose_name='Оплачено поинтами', default=0)
    code = models.ForeignKey(
        Coupons, verbose_name="Использованный купон", on_delete=models.SET_NULL, null=True, blank=True
    )
    link_payment = models.CharField(verbose_name='Ссылка на оплату', default="", max_length=1000)
    create_at = models.DateTimeField(verbose_name='Время создания', default=timezone.now)
    hide = models.BooleanField(verbose_name='Скрыто из истории', default=False)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
