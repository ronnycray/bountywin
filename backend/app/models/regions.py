from django.db import models


class Regions(models.Model):
    name = models.CharField(verbose_name="Город", max_length=50, blank=False, default="")

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"
