from django.db import models
from .events import Events
from django.utils import timezone


class StepSimulation(models.Model):
    event = models.ForeignKey(
        Events, verbose_name='Конкурс', on_delete=models.CASCADE,
    )
    create_at = models.DateTimeField(verbose_name='Время', default=timezone.now)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = "Симуляция конкурсов"
        verbose_name_plural = "Симуляция конкурсов"
