from django.db import models


class Gifts(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100, default="")
    description = models.CharField(verbose_name="Описание", max_length=1000, default="")
    amount = models.IntegerField(verbose_name='Цена', default=0)
    bonus = models.IntegerField(verbose_name='Бонус', default=0)
    photo = models.ImageField(verbose_name='Фото', upload_to='media/', default="", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Подарочный набор"
        verbose_name_plural = 'Подарочные наборы'
