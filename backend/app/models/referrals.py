from django.db import models
from .users import Users


class Referrals(models.Model):
    user = models.ForeignKey(
        Users, verbose_name='Пользователь', related_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    up_liner = models.ForeignKey(
        Users, verbose_name='Кто пригласил', related_name='Пригласитель', on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = "Реф. система"
        verbose_name_plural = 'Реф. система'
