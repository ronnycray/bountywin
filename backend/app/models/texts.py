from django.db import models


class Texts(models.Model):
    key = models.CharField(verbose_name='Ключ', max_length=100, default="")
    name = models.CharField(verbose_name='Имя', max_length=100, default="")
    text = models.CharField(verbose_name='Текст', max_length=1000, default="")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Статическая страница'
        verbose_name_plural = 'Статические страницы'
