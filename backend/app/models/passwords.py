from django.db import models
from .users import Users
from .codes import Codes


class Passwords(models.Model):
    user = models.ForeignKey(
        Users, verbose_name="Пользвоатель", on_delete=models.SET_NULL, null=True
    )
    password = models.CharField(verbose_name='Новый пароль', max_length=100, default="")
    code_activate = models.ForeignKey(
        Codes, verbose_name="Код для активации", on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return self.password

    class Meta:
        verbose_name = 'Пароль'
        verbose_name_plural = 'Пароли'
