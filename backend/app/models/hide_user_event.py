from django.db import models
from .users import Users
from .events import Events


class HideUserEvent(models.Model):
    user = models.ForeignKey(
        Users, verbose_name='Пользвоатель', on_delete=models.SET_NULL, null=True
    )
    event = models.ForeignKey(
        Events, verbose_name='Событие', on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = "Скрытыий пользователем евент"
        verbose_name_plural = 'Скрытыий пользователем евент'
