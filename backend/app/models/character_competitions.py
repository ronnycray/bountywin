from django.db import models
from .competitions import Competitions


class CharacterCompetitions(models.Model):
    competition = models.ForeignKey(
        Competitions, verbose_name='Компетенция', on_delete=models.CASCADE, null=False
    )
    name = models.CharField(verbose_name='Характеристика компетенции', default="", max_length=100)
    image = models.ImageField(verbose_name="Картинка", upload_to='media/', default="")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Характеристика компетенций"
        verbose_name_plural = "Характеристика компетенций"
