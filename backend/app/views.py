from django.contrib.auth.hashers import make_password, check_password
from django.http.response import HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.db.models import ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.http import JsonResponse
from random import choice, randint
from django.core.files import File
from django.utils import timezone
from django.conf import settings
from coinpayments import CoinPaymentsAPI
from pycoingecko import CoinGeckoAPI
from datetime import datetime
from .models import *
import requests
import openpyxl
import calendar
import logging
import secrets
import hashlib
import base64
import string
import json
import time
import hmac
import jwt

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')
algorithm = "HS256"

RESPONSE_ERROR = HttpResponseServerError.status_code
RESPONSE_OK = 200
RESPONSE_UNAUTHORIZED = 401

domain = 'https://bountywinners.com'
ERROR_URL = ""
SUCCESSFULLY_CHANGE_PASSWORD_URL = ""
SUCCESSFULLY_ACTIVATE_USER_URL = ""

MAXIMUM_TOKEN_HAX = 20

ACCOUNT_PAYEER = "P97488791"
APPID_PAYEER = 298
API_PASSWORD_PAYEER = "Miramax1199"
ID_MERCHATNT_PAYEER = 1064061879


def make_password_user(password: str) -> str:
    hash_password = make_password(password=password)

    return hash_password


def is_right_password(password: str, email: str) -> bool:
    try:
        user = Users.objects.get(email=email)
        logging.warning(f"password, {password}")
        logging.warning(f"user.password, {user.password}")
        return check_password(password=password, encoded=user.password)
    except ObjectDoesNotExist:
        return False


def generate_jwt_token(email: str, password: str) -> str:
    payload = {
        "email": email,
        "password": password
    }
    token = jwt.encode(payload, settings.SECRET_KEY, algorithm)

    return token


def generate_jwt_token_for_change_password(email: str, password: str) -> str:
    payload = {
        "email": email,
        "password": password
    }
    token = jwt.encode(payload, settings.SECRET_KEY, algorithm)

    return token


def decode_jwt_token(request) -> dict:
    token = get_token_from_header(request=request)
    decode_token = jwt.decode(token, settings.SECRET_KEY, algorithms=[algorithm])
    return decode_token


def get_email_from_jwt_token(decode_token: dict) -> str:
    email = decode_token.get('email', "")
    return email


def get_password_from_jwt_token(decode_token: dict) -> str:
    password = decode_token.get('password', "")
    return password


def get_body(request) -> dict:
    payloads = json.loads(request.body.decode('utf-8'))
    return payloads


def get_token_from_header(request) -> str:
    token = request.headers.get('Authorization', "")
    return token


def email_is_free(email: str) -> bool:
    user = Users.objects.filter(email=email)
    if user:
        return False

    return True


def define_response(data: dict) -> int:
    if data["error"] != "":
        if data["error"] == "Неверный логин или пароль":
            return RESPONSE_UNAUTHORIZED
        else:
            return RESPONSE_ERROR
    else:
        return RESPONSE_OK


def generate_code() -> str:
    code = secrets.token_hex(nbytes=MAXIMUM_TOKEN_HAX)
    is_have = Codes.objects.filter(code=code)
    while is_have:
        code = secrets.token_hex(nbytes=MAXIMUM_TOKEN_HAX)
        is_have = Codes.objects.filter(code=code)
    return code


def send_activate_code(user: Users, subject: str, msg: str):
    print(send_mail(subject=subject,
                    message='',
                    html_message=msg,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    recipient_list=[user.email],
                    fail_silently=False))


@csrf_exempt
def registration_user(request):
    body = get_body(request=request)
    password = body.get('password', None)
    email = body.get('email', None)
    username = body.get('username', None)
    ref_link = body.get('ref_link', None)

    response = {
        "token": None,
        "email": None,
        "username": None,
        "error": ""
    }

    if password is not None and email is not None and username is not None:
        email = email.lower()
        if email_is_free(email=email):
            hash_password = make_password_user(password=password)
            new_user = Users.objects.create(email=email, password=hash_password, username=username)
            if ref_link is not None:
                ref_user = Users.objects.filter(referral_link=ref_link)
                if ref_user:
                    user_instance = Users.objects.get(pk=new_user.pk)
                    ref_user = ref_user[0]
                    user_instance.dream_points = user_instance.dream_points + int(Configuration.objects.get(key='ref_bonus').value)
                    user_instance.save()
                    ref_user.dream_points = ref_user.dream_points + int(Configuration.objects.get(key='ref_bonus').value)
                    ref_user.save()
                    Referrals.objects.create(user=user_instance, up_liner=ref_user)
                    HistoryPoints.objects.create(user=user_instance,
                                                 points=Configuration.objects.get(key='ref_bonus').value,
                                                 operation='earned')
                    HistoryPoints.objects.create(user=ref_user,
                                                 points=Configuration.objects.get(key='ref_bonus').value,
                                                 operation='earned')
            token = generate_jwt_token(email=email, password=password)
            response["token"] = token
            response["email"] = email
            response["username"] = username
            token_for_activate = generate_code()
            Codes.objects.create(user=new_user, code=token_for_activate)
            send_activate_code(user=new_user, subject='Активационный код',
                               msg=f'<b>Для активации аккаунта на сайте bountywin.com '
                                   f'<a href="{domain}/api/activate_user/{token_for_activate}/">'
                                   f'перейдите по этой ссылке</a></b>')
        else:
            response["error"] = f"Email {email} существует."
    else:
        if password is None:
            response["error"] = "Вы не указали пароль"
        elif email is None:
            response["error"] = "Вы не указали email"
        elif username is None:
            response["error"] = "Вы не указали username"
        else:
            response["error"] = "Не известная ошибка регистрации"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def activate_user(request, code):
    object_code = Codes.objects.filter(code=code, active=False)
    if object_code:
        object_code = Codes.objects.get(code=code)
        object_code.active = True
        object_code.save()
        user = Users.objects.get(pk=object_code.user.pk)
        user.active = True
        user.save()
        return redirect(to=f"{domain}/")

    return redirect(to=f"{domain}/")


@csrf_exempt
def authorization_user(request):
    body = get_body(request=request)
    password = body.get('password', None)
    username = body.get('username', None)

    response = {
        "token": "",
        "email": "",
        "username": "",
        "error": ""
    }

    print(password, username)

    if password is not None and username is not None:
        user = Users.objects.filter(email=username.lower())
        print('USER', user)
        email = None
        if user:
            email = user[0].email
        else:
            user = Users.objects.filter(username=username)
            if user:
                email = user[0].email
            else:
                response["error"] = "Пользователь не найден"

        if email is not None:
            user = Users.objects.get(email=email)
            if is_right_password(password=password, email=user.email):
                token = generate_jwt_token(email=user.email, password=password)
                response["token"] = token
                response["email"] = user.email
                response["username"] = user.username
            else:
                response["error"] = "Неверный пароль"
        else:
            response["error"] = "Пользователь не найден"
    else:
        response["error"] = "Введены пустые значения"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_last_events(request):
    response = {
        "events": [],
        "error": ""
    }
    plain_events = Events.objects.filter(sold=False).order_by('-create_at')[:10]
    print(plain_events)
    if len(plain_events) != 0:
        for event in plain_events:
            photos_event = Images.objects.filter(event=event.pk, main_image=True)
            if len(photos_event) == 0:
                photo = Images.objects.filter(event=event.pk).first()
            else:
                photo = photos_event[0]
            if photo:
                sell_tickets = Tickets.objects.filter(event=event, sell=True).count()
                left = event.ticket - sell_tickets
                if sell_tickets < event.limit:
                    now_price = event.price - ((event.price * event.percent_change) / 100)
                else:
                    now_price = event.price

                characters = []
                characters_event = CharacterEvent.objects.filter(event=event.pk)
                for character in characters_event:
                    characters.append({
                        "name": character.character.name,
                        "photo": character.character.image.url,
                        "value": character.value
                    })

                days = event.time_end_event - event.create_at
                last_days = event.time_end_event - timezone.now()
                print(last_days)
                response["events"].append({
                    "id": event.id,
                    "name": event.name,
                    "competition": event.competition.name,
                    "price": event.price,
                    "now_price": now_price,
                    "left": left,
                    "photo": photo.photo.url,
                    "characters": characters,
                    "finished": event.finished,
                    "sell": event.sold,
                    "days": days.days,
                    "last_days": last_days.days
                })

    if len(response["events"]) == 0:
        response["error"] = "На данный момент события отсутсвуют"

    print(response)

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_statistics(request):
    response = {
        "statistic": {},
        "error": ""
    }
    try:
        statistic = StaticInfo.objects.filter().order_by('-pk')
        if statistic:
            statistic = statistic[0]
            response["statistic"] = {
                "winners": statistic.winners,
                "prizes": statistic.prizes,
                "charity": statistic.charity,
                "followers": statistic.followers
            }
        else:
            response["error"] = "Ошибка!\nСтатистика не найдена."
    except ObjectDoesNotExist:
        response["error"] = "Ошибка!\nСтатистика не найдена."

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_stat_page(request):
    response = {
        "name": "",
        "text": "",
        "error": ""
    }
    body = get_body(request=request)
    key_page = body.get('key', None)
    if key_page is not None:
        page = Texts.objects.filter(key=key_page)
        if page:
            response["name"] = page[0].name
            response["text"] = page[0].text
        else:
            response["error"] = "Страница не найдена"
    else:
        response["error"] = "Страница не передана"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_winners(request):
    response = {
        "winners": [],
        "error": ""
    }
    winners = LastWinner.objects.all()
    for win in winners:
        response["winners"].append({
            "id": win.id,
            "photo": win.photo.url,
            "event": win.event.name,
            "finished_at": {
                "data": win.event.time_end_event.strftime('%B %d, %Y'),
                "time": win.event.time_end_event.strftime('%I:%M %p')
            },
            "content": win.content,
            "competition": win.event.competition.name,
            "is_top": win.is_top,
            "winner": f"{win.event.winner.first_name} {win.event.winner.second_name}",
            "win_ticket": win.event.win_ticket
        })

    if len(response["winners"]) == 0:
        response["error"] = "Победителей пока еще нет"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_top_winners(request):
    response = {
        "winners": [],
        "error": ""
    }
    winners = LastWinner.objects.filter(is_top=True)
    for win in winners:
        response["winners"].append({
            "id": win.id,
            "photo": win.photo.url,
            "event": win.event.name,
            "content": win.content,
            "competition": win.event.competition.name,
            "is_top": win.is_top
        })

    if len(response["winners"]) == 0:
        response["error"] = "Победителей пока еще нет"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_all_events(request):
    response = {
        "events": [],
        "error": ""
    }

    for event in Events.objects.filter(finished=False):
        photos_event = Images.objects.filter(event=event.pk).order_by('-main_image')
        if len(photos_event) != 0:
            array_photo = []
            for photo in photos_event:
                array_photo.append(photo.photo.url)

            sell_tickets = Tickets.objects.filter(event=event, sell=True).count()
            left = event.ticket - sell_tickets
            percent_of_sold = 0
            if sell_tickets != 0:
                percent_of_sold = int(event.ticket / (100 * sell_tickets))
            name_price = ""
            if sell_tickets < event.limit:
                now_price = event.price - ((event.price * event.percent_change) / 100)
                name_price = "LAUNCH PRICE!"
            else:
                now_price = event.price

            answers_array = []
            for answer in Answers.objects.filter(event=event.pk):
                answers_array.append({
                    "answer_id": answer.pk,
                    "text": answer.answer
                })

            if len(answers_array) != 0:

                days = event.time_end_event - event.create_at
                last_days = event.time_end_event - timezone.now()
                print(last_days)

                characters = []
                characters_event = CharacterEvent.objects.filter(event=event.pk)
                for character in characters_event:
                    characters.append({
                        "name": character.character.name,
                        "photo": character.character.image.url,
                        "value": character.value
                    })

                response["events"].append({
                    "id": event.id,
                    "name": event.name,
                    "description": event.description,
                    "competition_info": {
                        "name": event.competition.name,
                        "description": event.competition.description
                    },
                    "price": event.price,
                    "name_price": name_price,
                    "limit_off_sale": event.limit,
                    "limit_person": event.limit_person,
                    "tickets": event.ticket,
                    "left": left,
                    "percent_of_sold": percent_of_sold,
                    "photo": array_photo,
                    "characters": characters,
                    "date_create": event.create_at,
                    "date_create_unix": time.mktime(event.create_at.timetuple()),
                    "date_end_event": event.time_end_event,
                    "date_end_event_unix": time.mktime(event.time_end_event.timetuple()),
                    "days": days.days,
                    "last_days": last_days.days,
                    "question": event.question,
                    "answers": answers_array,
                    "finished": event.finished,
                    "sell": event.sold
                })

                if now_price != event.price:
                    response["events"][len(response["events"]) - 1]["now_price"] = now_price

            else:
                response["error"] = "Список вопросов не готов"

        else:
            response["error"] = "Событие на подходе!"

    if len(response['events']) == 0:
        response["error"] = "Запущенных розыгрышей нет!"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_event(request):
    response = {
        "event": {},
        "error": ""
    }
    body = get_body(request=request)
    id_event = body.get('id_event', None)

    if id_event is not None:
        event = Events.objects.filter(pk=id_event)
        if event:
            event = event[0]

            photos_event = Images.objects.filter(event=event.pk).order_by('-main_image')
            if len(photos_event) != 0:
                array_photo = []
                for photo in photos_event:
                    array_photo.append(photo.photo.url)

                sell_tickets = Tickets.objects.filter(event=event, sell=True).count()
                left = event.ticket - sell_tickets
                percent_of_sold = 0
                if sell_tickets != 0:
                    percent_of_sold = int(event.ticket / (100 * sell_tickets))
                name_price = ""
                if sell_tickets < event.limit:
                    now_price = event.price - ((event.price * event.percent_change) / 100)
                    name_price = "LAUNCH PRICE!"
                else:
                    now_price = event.price

                answers_array = []
                for answer in Answers.objects.filter(event=event.pk):
                    answers_array.append({
                        "answer_id": answer.pk,
                        "text": answer.answer
                    })

                if len(answers_array) != 0:

                    characters = []
                    characters_event = CharacterEvent.objects.filter(event=event.pk)
                    for character in characters_event:
                        characters.append({
                            "name": character.character.name,
                            "photo": character.character.image.url,
                            "value": character.value
                        })

                    days = event.time_end_event - event.create_at

                    response["event"] = {
                        "id": event.id,
                        "name": event.name,
                        "description": event.description,
                        "competition_info": {
                            "name": event.competition.name,
                            "description": event.competition.description
                        },
                        "price": event.price,
                        "name_price": name_price,
                        "limit_off_sale": event.limit,
                        "limit_person": event.limit_person,
                        "tickets": event.ticket,
                        "left": left,
                        "percent_of_sold": percent_of_sold,
                        "photo": array_photo,
                        "characters": characters,
                        "date_create": event.create_at,
                        "date_create_unix": time.mktime(event.create_at.timetuple()),
                        "date_end_event": event.time_end_event,
                        "date_end_event_unix": time.mktime(event.time_end_event.timetuple()),
                        "all_day": days.days,
                        "question": event.question,
                        "answers": answers_array,
                        "sell": event.sold,
                        "finished": event.finished
                    }

                    if event.finished and event.winner is not None:
                        response["event"]["winner"] = f"{event.winner.first_name} {event.winner.second_name}"
                        response["event"]["win_ticket"] = event.win_ticket

                    if now_price != event.price:
                        response["event"]["now_price"] = now_price

                else:
                    response["error"] = "Список вопросов не готов"

            else:
                response["error"] = "Событие на подходе!"

        else:
            response["error"] = 'Событие не найдено'

    else:
        response["error"] = 'Не определен ID события'

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def update_profile(request):
    response = {
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    password = get_password_from_jwt_token(decode_token=token)
    if is_right_password(password=password, email=email):
        body = get_body(request=request)
        first_name = body.get('first_name', False)
        second_name = body.get('second_name', False)
        city = body.get('city', False)
        country = body.get('country', False)
        address = body.get('address', False)
        postcode = body.get('postcode', False)
        phone = body.get('phone', False)
        day_birth = body.get('day_birth', False)
        user = Users.objects.get(email=email)
        if first_name:
            user.first_name = first_name
        if second_name:
            user.second_name = second_name
        if city:
            city_object = Regions.objects.get(pk=city)
            user.city = city_object
        if country:
            country_object = Countries.objects.get(pk=country)
            user.country = country_object
        if address:
            user.address = address
        if postcode:
            user.postcode = postcode
        if phone:
            user.phone = phone
        if day_birth:
            try:
                day_birth = day_birth.split('/')
                date = datetime(day=int(day_birth[0]),
                                month=int(day_birth[1]),
                                year=int(day_birth[2]))
                user.day_birth = date
            except Exception as e:
                print(e)
                response["error"] = "Дата рождения введена не корректно"

        if response["error"] == "":
            user.save()
            response["status"] = True
        else:
            response["status"] = False
    else:
        response["error"] = "Неверный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_info_user(request):
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)

    response = {
        "info": {},
        "error": ""
    }

    logging.warning(f"token: {token}")
    logging.warning(f"email: {email}")

    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        day_birth_year = int(str(user.day_birth).split('-')[0])
        now_year = datetime.today().year
        if int(now_year) - 15 < day_birth_year:
            day_birth = ""
        else:
            day_birth = user.day_birth

        try:
            photo = user.photo.url
        except ValueError:
            photo = ""

        response["info"] = {
            "username": user.username,
            "email": user.email,
            "dream_points": user.dream_points,
            "first_name": user.first_name,
            "second_name": user.second_name,
            "city": str(user.city),
            "country": str(user.country),
            "address": user.address,
            "postcode": user.postcode,
            "phone": user.phone,
            "day_birth": day_birth,
            "photo": photo,
            "active": user.active,
            "ref_link": user.referral_link
        }
    else:
        response["error"] = "Неверный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def change_password(request):
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)

    response = {
        "status": False,
        "token": "",
        "error": ""
    }

    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        body = get_body(request=request)
        password = body.get('password', None)
        new_password = body.get('new_password', None)
        logging.warning('check_pass', check_password(password=password, encoded=user.password))
        if check_password(password=password, encoded=user.password):
            token_for_activate = generate_code()
            new_code = Codes.objects.create(user=user, code=token_for_activate)
            Passwords.objects.create(user=user, password=new_password, code_activate=new_code)
            send_activate_code(user=user, subject='Подтверждение изменения пароля',
                               msg=f'<b>Для изменения текущего пароля на новый, Вам необходимо подтвердить это действие '
                               f'<a href="{domain}/api/approve_change_password/{token_for_activate}/">'
                               f'перейдя по данной ссылке</a></b>')
            response["status"] = True
        else:
            response["error"] = "Не верный текущий пароль"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def approve_change_password(request, code):
    object_code = Codes.objects.filter(code=code, active=False)
    if object_code:
        object_code = Codes.objects.get(code=code)
        object_password = Passwords.objects.filter(code_activate=object_code.pk)
        if object_password:
            object_password = Passwords.objects.get(code_activate=object_code.pk)
            object_code.active = True
            object_code.save()
            user = Users.objects.get(pk=object_password.user.pk)
            user.password = make_password_user(password=object_password.password)
            user.save()
            return redirect(to=f"{domain}/")

    return redirect(to=f"{domain}/")


@csrf_exempt
def change_email(request):
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)

    response = {
        "status": False,
        "token": None,
        "error": ""
    }

    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        body = get_body(request=request)
        new_email = body.get('email', None)
        password = body.get('password', None)
        if new_email is not None and password is not None:
            if check_password(password=password, encoded=user.password):
                user.email = new_email
                try:
                    user.save()
                    token = generate_jwt_token_for_change_password(email=user.email, password=password)
                    response["token"] = token
                    response["status"] = True
                except IntegrityError:
                    response["error"] = f"Email: {new_email} уже используется"
            else:
                response["error"] = "Введен неверный пароль"
        else:
            if new_email is not None:
                response["error"] = "Пустое значение email"
            elif password is not None:
                response["error"] = "Пустое значение пароля"
            else:
                response["error"] = "Вы передали пустые значения"
    else:
        response["error"] = "не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_entry_list(request):
    response = {
        "events": [],
        "error": ""
    }
    finished_events = Events.objects.filter(sold=True, finished=True).order_by('-pk')
    if len(finished_events) != 0:
        for event in finished_events:
            image = Images.objects.filter(main_image=True)
            if image:
                response["events"].append({
                    "id": event.id,
                    "name": event.name,
                    "create_at": {
                        "date": event.create_at.strftime('%B %d, %Y'),
                        "time": event.create_at.strftime('%I:%M %p'),
                    },
                    "finished_at": {
                        "data": event.time_end_event.strftime('%B %d, %Y'),
                        "time": event.time_end_event.strftime('%I:%M %p')
                    },
                    "finished": event.finished,
                    "sell": event.sold
                })
    else:
        response["error"] = "Список пуст"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_finished_events(request):
    response = {
        "events": [],
        "error": ""
    }
    all_finished_events = Events.objects.filter(finished=True).order_by('-time_end_event')
    for event in all_finished_events:
        finished_event = event
        images_objects = Images.objects.filter(event=finished_event.id).order_by('-main_image')
        images_array = []
        for image in images_objects:
            images_array.append(image.photo.url)
        characters = []
        characters_event = CharacterEvent.objects.filter(event=event.pk)
        for character in characters_event:
            characters.append({
                "name": character.character.name,
                "photo": character.character.image.url,
                "value": character.value
            })
        response["events"].append({
            "id": event.id,
            "name": event.name,
            "description": event.description,
            "competition_info": {
                "name": event.competition.name,
                "description": event.competition.description
            },
            "sold": event.sold,
            "finished": event.finished,
            "images": images_array,
            "characters": characters,
            "sell": event.sold
        })

    if len(all_finished_events) == 0:
        response["error"] = "События не найдены"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_finished_event(request):
    response = {
        "event": {},
        "error": ""
    }
    body = get_body(request=request)
    id_event = body.get('id', None)
    if id_event is not None:
        finished_event = Events.objects.filter(pk=id_event)
        if finished_event:
            event = finished_event[0]
            if event.sold:
                images_objects = Images.objects.filter(event=id_event).order_by('-main_image')
                images_array = []
                for image in images_objects:
                    images_array.append(image.photo.url)
                characters = []
                characters_event = CharacterEvent.objects.filter(event=event.pk)
                for character in characters_event:
                    characters.append({
                        "name": character.character.name,
                        "photo": character.character.image.url,
                        "value": character.value
                    })
                response["event"] = {
                    "id": event.id,
                    "name": event.name,
                    "description": event.description,
                    "competition_info": {
                        "name": event.competition.name,
                        "description": event.competition.description
                    },
                    "sold": event.sold,
                    "finished": event.finished,
                    "images": images_array,
                    "characters": characters,
                    "sell": event.sold
                }
            else:
                response["error"] = "Событие еще не завершено"
        else:
            response["error"] = "Событие не найдено"
    else:
        response["error"] = "Событие не найдено"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def related_competitions(request):
    response = {
        "events": [],
        "error": ""
    }
    body = get_body(request=request)
    id_event = body.get('id', None)
    if id_event is not None:
        event_instance = Events.objects.get(pk=id_event)
        array_events = []
        for event in Events.objects.filter(finished=False, sold=False):
            if event.price + 40 >= event_instance.price or event.price - 40 <= event_instance.price:
                sell_tickets = Tickets.objects.filter(event=event.id, sell=True).count()
                left = event.ticket - sell_tickets
                percent_of_sold = 0
                if sell_tickets != 0:
                    percent_of_sold = int(event.ticket / (100 * sell_tickets))
                name_price = ""
                if sell_tickets < event.limit:
                    now_price = event.price - ((event.price * event.percent_change) / 100)
                    name_price = "LAUNCH PRICE!"
                else:
                    now_price = event.price

                image = Images.objects.filter(event=event.id).order_by('-main_image')
                last_days = event.time_end_event - timezone.now()
                if image:
                    array_events.append({
                        "id": event.id,
                        "name": event.name,
                        "price": event.price,
                        "name_price": name_price,
                        "now_price": now_price,
                        "limit_person": event.limit_person,
                        "left": left,
                        "tickets": event.ticket,
                        "percent_of_sold": percent_of_sold,
                        "photo": image[0].photo.url,
                        "create_at": {
                            "date": event.create_at.strftime('%B %d, %Y'),
                            "time": event.create_at.strftime('%I:%M %p'),
                        },
                        "finished_at": {
                            "data": event.time_end_event.strftime('%B %d, %Y'),
                            "time": event.time_end_event.strftime('%I:%M %p')
                        },
                        "sold": event.sold,
                        "finished": event.finished,
                        "last_days": last_days.days
                    })

        response["events"] = array_events
        if len(response["events"]) == 0:
            response["error"] = "Событий нет"

    else:
        response["error"] = "Событие не найдено"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def check_booking_tickets_db(request):
    token = get_token_from_header(request=request)
    error = ""
    if token == '123123%1231231#23123!@12312312':
        minutes = 7 * 60
        time_now = time.time()
        for ticket in Tickets.objects.filter(sell=False, booking=True):
            if float(ticket.booking_time) + minutes <= time_now:
                ticket.booking_time = 0
                ticket.booking = False
                ticket.save()

        orders_users = Order.objects.filter(payment=False, cancel_payment=False)
        for order in orders_users:
            if order.create_at < timezone.now() - timezone.timedelta(minutes=10):
                order.status = 'cancel'
                order.cancel_payment = True
                order.save()
    else:
        error = "Error"

    return JsonResponse(data={"error": error}, status=define_response(data={"error": error}))


@csrf_exempt
def check_events_finished(request):
    token = get_token_from_header(request=request)
    error = ""
    if token == '123123%1231231#23123!@12312312':
        all_events = Events.objects.filter(finished=False)
        for event in all_events:
            print(event.time_end_event.date(), type(event.time_end_event.date()))
            if event.time_end_event < timezone.now():
                file = openpyxl.Workbook()
                naming = f'list_participants_event_{event.id}.xlsx'
                path = '/home/website/bountywin/backend/media/media/list_files/{}'.format(naming)
                file.save(path)
                wb = openpyxl.load_workbook(path)

                # grab the active worksheet
                sheet = wb.active
                sheet['A1'] = 'Номер билета'
                sheet['B1'] = 'Email пользователя'

                i = 2

                for item in Tickets.objects.filter(event=event.id):
                    sheet['A{}'.format(i)] = item.number
                    sheet['B{}'.format(i)] = item.user.email if item.user is not None else '-'

                    i += 1

                wb.save(path)
                doc = open(path, 'rb')
                form = File(doc)
                event.finished = True
                event.doc = form
                event.save()
    else:
        error = "Error"

    return JsonResponse(data={"error": error}, status=define_response(data={"error": error}))


@csrf_exempt
def get_tickets(request):
    response = {
        "events": [],
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        print(body)
        events_body = body.get('events', None)
        if events_body is not None:
            for event_element in events_body:
                tickets_array = []
                id_event = event_element.get('id_event', None)
                available_event = True
                if id_event is not None:
                    event = Events.objects.filter(pk=id_event)
                    if event:
                        event = event[0]
                        available_event = Tickets.objects.filter(event=event, sell=False, booking=False)
                        if available_event:
                            available_event = True
                            for ticket in Tickets.objects.filter(event=event).order_by('number'):
                                busy = False
                                if ticket.sell or ticket.booking:
                                    busy = True
                                tickets_array.append({
                                    "id": ticket.pk,
                                    "number": ticket.number,
                                    "busy": busy
                                })

                                if len(tickets_array) == 0:
                                    response["error"] = "Билеты на событие не найдены"
                        else:
                            available_event = False
                            response["error"] = "На данное событие проданы все билеты"
                    else:
                        response["error"] = "Событие не найдено"
                else:
                    response["error"] = "Пустое занчение события"

                response["events"].append({
                    "id_event": id_event,
                    "tickets": tickets_array,
                    "available_event": available_event
                })
        else:
            response["error"] = "Пустое значение событий"
    else:
        response["error"] = "Неверный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def buy_tickets(request):
    response = {
        "status": True,
        "events": [],
        "url": "",
        "error": ""
    }

    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        events_for_buy = body.get('events', None)
        amount = body.get('amount', None)
        code = body.get('code', None)
        points = body.get('points', None)
        payment = body.get('payment', None)
        if events_for_buy is not None and payment is not None:
            ticket_answer_array = []  # Массив для заполения базы OrderTickets
            for event in events_for_buy:
                tickets_array = []
                array_ids_events = []
                answers_ids = []
                for t in event:
                    tickets_array.append(t.get('tickets', None))
                for i in event:
                    array_ids_events.append(i.get('id_event', None))
                for a in event:
                    answers_ids.append(a.get('id_answer', None))

                if len(tickets_array) != 0 and len(array_ids_events) != 0 and len(answers_ids) != 0:

                    if len(array_ids_events) == array_ids_events.count(array_ids_events[0]) and \
                            len(answers_ids) == answers_ids.count(answers_ids[0]):
                        id_event = array_ids_events[0]
                        tickets_user = tickets_array
                        answer = answers_ids[0]

                        if id_event is not None and tickets_user is not None and amount is not None and answer is not None:
                            event = Events.objects.filter(pk=id_event)
                            if event:
                                event = event[0]
                                if event.sold is False or event.finished is False:
                                    tickets_array = []

                                    for id_ticket in tickets_user:
                                        if id_ticket is not None:
                                            info_ticket = Tickets.objects.filter(pk=id_ticket, event=event.pk)
                                            if info_ticket:
                                                busy = False
                                                info_ticket = info_ticket[0]
                                                if info_ticket.sell or info_ticket.booking:
                                                    response["status"] = False
                                                    busy = True

                                                tickets_array.append({
                                                    "id": info_ticket.id,
                                                    "number": info_ticket.number,
                                                    "busy": busy
                                                })

                                                ticket_answer_array.append({
                                                    "ticket_id": info_ticket.id,
                                                    "answer_id": answer
                                                })

                                            else:
                                                response["error"] = "Указаны не существующие билеты"
                                                response["status"] = False
                                                break
                                        else:
                                            response["error"] = "Передан не верный формат билета"
                                            response["status"] = False
                                            break

                                    response["events"].append({
                                        "id_event": event.id,
                                        "tickets": tickets_array
                                    })
                                else:
                                    response["error"] = "Событие завершено или все билеты проданы"
                            else:
                                response["error"] = 'Данное событие не найдено'
                        else:
                            if id_event is None:
                                response["error"] = "Пустое значение ID события"
                            elif tickets_user is None:
                                response["error"] = "Пустое значение билетов"
                            elif amount is None:
                                response["error"] = "Пустое значение суммы"
                            elif answer is None:
                                response["error"] = "Пустое значение овтета"
                            else:
                                response["error"] = "Не известная ошибка"
                    else:
                        if len(array_ids_events) == array_ids_events.count(array_ids_events[0]):
                            response["error"] = "Не верные параметры id события"
                        elif len(answers_ids) == answers_ids.count(answers_ids[0]):
                            response["error"] = "Не верные параметры id ответа"
                else:
                    if len(tickets_array) == 0:
                        response["error"] = "Пустые значения билетов"
                    elif len(array_ids_events) == 0:
                        response["error"] = "Пустые значения событий"
                    else:
                        response["error"] = "Пустые значения ответов"

            if response["error"] != "":
                response["status"] = False

            if response["error"] == '' and response["status"] is True:
                user = Users.objects.get(email=email)

                if points != 0:
                    if user.dream_points < points:
                        response["error"] = "На вашем счете недостаточно DreamPoints"

                if response["error"] == "" and response["status"] is True:
                    code_object = Coupons.objects.filter(code=code)
                    if code_object:
                        new_order = Order.objects.create(user=user, amount=amount, points=points, code=code_object)
                    else:
                        new_order = Order.objects.create(user=user, amount=amount, points=points)
                    for ticket_element in ticket_answer_array:
                        answer_object = Answers.objects.get(pk=ticket_element['answer_id'])
                        ticket = Tickets.objects.get(pk=ticket_element['ticket_id'])
                        ticket.booking = True
                        ticket.save()
                        TicketsOrders.objects.create(order=new_order, ticket=ticket, answer=answer_object)
                    url = create_checkout(order_id=new_order.pk, order_amount=amount, payment=payment)
                    order = Order.objects.get(pk=new_order.pk)
                    order.link_payment = url
                    order.save()
                    if url != "":
                        response["status"] = True
                        response["url"] = url
                    else:
                        response["error"] = "Ошибка создания страницы платежа"

        else:
            if payment is None:
                response["error"] = 'Платежная система не найдена'
            else:
                response["error"] = 'Событие не определено'
    else:
        response["error"] = "Неверный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_discount_coupon(request):
    response = {
        "status": False,
        "code": "",
        "coupon": None,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        code = body.get("code", None)
        coupon = Coupons.objects.filter(code=code)
        if coupon:
            coupon = coupon[0]
            response["coupon"] = {}
            response["coupon"]["discount_percent"] = coupon.discount_percent
            response["coupon"]["discount_amount"] = coupon.discount_amount
            response["coupon"]["code"] = coupon.code
            response["coupon"]["name"] = coupon.name
            response["code"] = coupon.code
            response["status"] = True
        else:
            response["code"] = code
            response["error"] = "Купон не был найден"
    else:
        response["error"] = "Логин или пароль не верны"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_bonus_gift_cart(request):
    response = {
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        code = body.get('code', None)
        if code is not None:
            gift = UserGifts.objects.filter(code=code)
            if gift:
                gift = gift[0]
                user = Users.objects.get(email=email)
                user.dream_points = user.dream_points + gift.gift.bonus
                user.save()
                gift.delete()
                HistoryPoints.objects.create(user=user, points=gift.gift.bonus, operation='buy')
                response["status"] = True
            else:
                response["error"] = "Данный код не существует"
        else:
            response["error"] = "Не передал параметр кода"
    else:
        response["error"] = "Неверный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def buy_gift_cart(request):
    response = {
        "status": False,
        "url": "",
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        id_gift = body.get('id', None)
        qnt = body.get('qnt', None)
        if id_gift is None:
            response["error"] = "Подарочная карта не определена"
        elif qnt is None:
            response["error"] = "Кол-во подарочных карт не определено"
        else:
            object_gift = Gifts.objects.filter(pk=id_gift)
            if object_gift:
                gift = object_gift[0]
                total_amount = gift.amount * qnt
                user = Users.objects.get(email=email)
                hash_token = secrets.token_hex(60)
                new_order = GiftOrder.objects.create(user=user, gift=gift, amount=total_amount,
                                                     hash_token=hash_token)
                url = create_checkout_on_gift(order_id=new_order.pk, amount=total_amount, token_hash=hash_token)
                if url != "":
                    response["url"] = url
                    response["status"] = True
                else:
                    response["error"] = "Ошибка создания страницы платежа"
            else:
                response["error"] = 'Подарочная карта не найдена'
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


def create_checkout(order_id: int, order_amount: int, payment: str) -> str:
    order = Order.objects.get(pk=order_id)
    if payment == 'rapyd':
        body = {
            "amount": order_amount,
            "complete_checkout_url": f"https://bountywin.com/api/successfully_payment/?token={order_id}&payment=rapid",
            "complete_payment_url": f"https://bountywin.com/api/successfully_payment/?token={order_id}&payment=rapid",
            "country": "RU",
            "currency": "RUB",
            "error_payment_url": f"https://bountywin.com/api/fail/?token={order_id}",
            "language": "ru",
            "merchant_website": "https://bountywin.com/"
        }

        body_for_headers = json.dumps(body, separators=(',', ':'))

        http_method = 'post'
        path = '/v1/checkout'

        # salt: randomly generated for each request.
        min_char = 8
        max_char = 12
        allchar = string.ascii_letters + string.punctuation + string.digits
        salt = "".join(choice(allchar) for x in range(randint(min_char, max_char)))

        # Current Unix time.
        d = datetime.utcnow()
        timestamp = calendar.timegm(d.utctimetuple())

        access_key = 'CCBB9577410A8982266C'  # The access key received from Rapyd.
        secret_key = '37b40825145e6871e22d5a224b54bccded7d79ad16b9f6d5d63e8ea234c10085b079ad954f26625c'  # Never transmit the secret key by itself.

        to_sign = http_method + path + salt + str(timestamp) + access_key + secret_key + str(body_for_headers)

        h = hmac.new(bytes(secret_key, 'utf-8'), bytes(to_sign, 'utf-8'), hashlib.sha256)

        signature = base64.urlsafe_b64encode(str.encode(h.hexdigest()))

        headers = {
            "access_key": access_key,
            "Content-Type": 'application/json',
            "salt": salt,
            "signature": signature,
            "timestamp": str(timestamp)
        }

        result = requests.post(url=f'https://sandboxapi.rapyd.net/v1/checkout', headers=headers, json=body)
        print(result.json())
        response = result.json()
        if response['status']['status'] == 'SUCCESS':
            checkout_id = response['data']['id']
            order.checkout_id = checkout_id
            order.save()
            return response['data']['redirect_url']
    else:
        api = CoinPaymentsAPI(public_key='c0f53b218d90d996803632be84ed1b584da8062782c42d8f115117f5cd5a385c',
                              private_key='5c91c31885a37e81129F7e8D6ca355a67A78F30250f8de4bb537eaC9c7Cf2997')
        # cg = CoinGeckoAPI()
        # coin = cg.get_coin_by_id(id='bitcoin')
        # price_to_currency_for_each = coin['market_data']['current_price']['rub']
        # price_to_currency_for_each = round(price_to_currency_for_each, 2)
        # quantity_crypto_sell = order_amount / price_to_currency_for_each
        # quantity_crypto_sell = round(quantity_crypto_sell, 2)
        # amount = price_to_currency_for_each * quantity_crypto_sell
        response = api.create_transaction(amount=order_amount, currency1='USD', currency2='BTC',
                                          buyer_email=order.user.email,
                                          success_url=f"https://bountywin.com/api/successfully_payment/?token={order_id}"
                                          f"&payment=coinpayments",
                                          cancel_url=f"https://bountywin.com/api/successfully_payment/?token={order_id}"
                                          f"&payment=coinpayments")
        logging.warning(f'response coin: {response}')
        if response['error'] == 'ok':
            return response['result']['checkout_url']
    return ''


@csrf_exempt
def create_checkout_on_gift(order_id: int, amount: int, token_hash: str) -> str:
    body = {
        "amount": amount,
        "complete_checkout_url": f"https://bountywin.com/api/successfully_payment_gift/?token={token_hash}",
        "complete_payment_url": f"https://bountywin.com/api/successfully_payment_gift/?token={token_hash}",
        "country": "RU",
        "currency": "RUB",
        "error_payment_url": f"https://bountywin.com/api/fail_gift/?token={token_hash}",
        "language": "ru",
        "merchant_website": "https://bountywin.com/"
    }

    body_for_headers = json.dumps(body, separators=(',', ':'))

    http_method = 'post'
    path = '/v1/checkout'

    # salt: randomly generated for each request.
    min_char = 8
    max_char = 12
    allchar = string.ascii_letters + string.punctuation + string.digits
    salt = "".join(choice(allchar) for x in range(randint(min_char, max_char)))

    # Current Unix time.
    d = datetime.utcnow()
    timestamp = calendar.timegm(d.utctimetuple())

    access_key = 'CCBB9577410A8982266C'  # The access key received from Rapyd.
    secret_key = '37b40825145e6871e22d5a224b54bccded7d79ad16b9f6d5d63e8ea234c10085b079ad954f26625c'  # Never transmit the secret key by itself.

    to_sign = http_method + path + salt + str(timestamp) + access_key + secret_key + str(body_for_headers)

    h = hmac.new(bytes(secret_key, 'utf-8'), bytes(to_sign, 'utf-8'), hashlib.sha256)

    signature = base64.urlsafe_b64encode(str.encode(h.hexdigest()))

    headers = {
        "access_key": access_key,
        "Content-Type": 'application/json',
        "salt": salt,
        "signature": signature,
        "timestamp": str(timestamp)
    }

    result = requests.post(url=f'https://sandboxapi.rapyd.net/v1/checkout', headers=headers, json=body)
    print(result.json())
    response = result.json()
    if response['status']['status'] == 'SUCCESS':
        checkout_id = response['data']['id']
        order = GiftOrder.objects.get(pk=order_id)
        order.checkout_id = checkout_id
        order.save()
        return response['data']['redirect_url']

    return ''


@csrf_exempt
def get_regions(request):
    response = {
        "regions": [],
        "error": ""
    }

    countries_array = []
    print(Countries.objects.all())
    for country in Countries.objects.all():
        regions_array = []
        print(country.region.all())
        for region in country.region.all():
            regions_array.append({
                "region_id": region.pk,
                "city": region.name
            })
        countries_array.append({
            "country_id": country.pk,
            "country": country.name,
            "regions": regions_array
        })

    if len(countries_array) == 0:
        response["error"] = "Страны и регионы не доступны"
    else:
        response["regions"] = countries_array

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def successfully_payment(request):
    print('GET', request.GET)
    token = request.GET.get('token', None)
    response = {
        "status": False,
        "error": ""
    }
    if token is not None:
        print('token', token)
        order = Order.objects.filter(pk=token)
        print('ord', order)
        if order:
            order = order[0]
            if order.payment is False:
                user = Users.objects.get(pk=order.user.pk)
                if order.points != 0:
                    user.dream_points = user.dream_points - order.points
                    if user.dream_points < 0:
                        user.dream_points = 0
                    user.save()
                    HistoryPoints.objects.create(user=user, points=order.points, operation='spent')
                else:
                    user.dream_points = user.dream_points + (order.amount * int(Configuration.objects.get(key='cash_back').value))
                    user.save()
                    HistoryPoints.objects.create(user=user, points=int(Configuration.objects.get(key='cash_back').value), operation='cash_back')

                tickets_order_array = TicketsOrders.objects.filter(order=order.id)
                for ticket_order in tickets_order_array:
                    try:
                        answer = Answers.objects.get(pk=ticket_order.answer.id)
                        tic = Tickets.objects.get(pk=ticket_order.ticket.id)
                        tic.sell = True
                        tic.answer = answer
                        tic.user = user
                        tic.save()
                    except Exception as e:
                        print(e)
                        response["error"] = 'Произошла ошибка при утвреждении заказа'
                        break

                if response["error"] == "":
                    response["status"] = True
                    order.payment = True
                    order.status = 'success'
                    order.save()
                    return redirect(to=f"{domain}/profile/entries/clear")
            else:
                response["error"] = "Заказ был оплачен"
        else:
            response["error"] = "Заказ не был найден"
    else:
        response["error"] = 'Платеж не определен'
    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def successfully_payment_gift(request):
    print('GET', request.GET)
    token = request.GET.get('token', None)
    response = {
        "status": False,
        "error": "",
        "code": ""
    }
    if token is not None:
        print('token', token)
        order = GiftOrder.objects.filter(hash_token=token)
        print('ord', order)
        if order:
            order = order[0]
            if order.payment is False:
                code = secrets.token_hex(5)
                coupon_object = Coupons.objects.filter(code=code)
                user_gift_objects = UserGifts.objects.filter(code=code)
                while coupon_object or user_gift_objects:
                    code = secrets.token_hex(5)
                    coupon_object = Coupons.objects.filter(code=code)
                    user_gift_objects = UserGifts.objects.filter(code=code)
                user = Users.objects.get(pk=order.user.pk)
                gift = Gifts.objects.get(pk=order.gift.pk)
                UserGifts.objects.create(user=user, gift=gift, code=code)
                order.payment = True
                order.save()
                response["status"] = True
                response["code"] = code
            else:
                response["error"] = "Заказ был оплачен"
        else:
            response["error"] = "Заказ не был найден"
    else:
        response["error"] = 'Платеж не определен'

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def fail_payment(request):
    return JsonResponse(data={'status': False}, status=200)


@csrf_exempt
def get_broadcasts(request):
    response = {
        "broadcasts": [],
        "error": ""
    }

    for broadcast in Broadcasts.objects.all():
        try:
            response["broadcasts"].append({
                "link": broadcast.link,
                "id_video": broadcast.link.replace('https://www.youtube.com/watch?v=', ""),
                "event": broadcast.event.name,
                "event_id": broadcast.event.id,
                "live": broadcast.live
            })
        except Exception as e:
            print(e)

    if response["broadcasts"] == 0:
        response["error"] = "Нет трансляций"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_gifts(request):
    response = {
        "gifts": [],
        "error": ""
    }

    for gift in Gifts.objects.all():
        response["gifts"].append({
            "id": gift.id,
            "name": gift.name,
            "description": gift.description,
            "amount": gift.amount,
            "bonus": gift.bonus,
            "photo": gift.photo.url
        })

    if len(response["gifts"]) == 0:
        response["error"] = "На данный момент нет подарочных карт"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_gift_cart(request):
    response = {
        "gift": {},
        "error": ""
    }
    body = get_body(request=request)
    id_cart = body.get('id', None)
    gift_cart = Gifts.objects.filter(pk=id_cart)
    if gift_cart:
        gift_cart = gift_cart[0]
        response["gift"] = {
            "id": gift_cart.id,
            "name": gift_cart.name,
            "description": gift_cart.description,
            "amount": gift_cart.amount,
            "bonus": gift_cart.bonus,
            "photo": gift_cart.photo.url
        }
    else:
        response["error"] = "Карта не найдена"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_gifts_user(request):
    response = {
        "gifts": [],
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        print(user.username)
        gifts_user = UserGifts.objects.filter(user=user.pk)
        for gift in gifts_user:
            try:
                response["gifts"].append({
                    "name": gift.gift.name,
                    "describe": gift.gift.description,
                    "code": gift.code,
                    "photo": gift.gift.photo.url
                })
            except Exception as e:
                print(e)

        if len(response["gifts"]) == 0:
            response["error"] = "У вас нет активных сущесутвующих подарочных карт"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_faq(request):
    response = {
        "faqs": [],
        "error": ""
    }
    for element in FAQ.objects.filter():
        response["faqs"].append({
            "title": element.question,
            "text": element.answer
        })

    if len(response["faqs"]) == 0:
        response["error"] = "Нет элементов вопрос-ответ"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_my_orders(request):
    response = {
        "orders": [],
        "error": "",
        "status": False
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        print(Order.objects.filter(payment=True, user=user.pk))
        for order in Order.objects.filter(user=user.pk, hide=False):
            events_array = []
            ready_events = []
            print(TicketsOrders.objects.filter(order=order.id))
            for ticket_order in TicketsOrders.objects.filter(order=order.id):
                if ticket_order.ticket.event.id not in ready_events:
                    ready_events.append(ticket_order.ticket.event.id)
                    tickets_array = []

                    for ticket_element_order in TicketsOrders.objects.filter(order=order.id):
                        tickets_array.append({
                            "id": ticket_element_order.ticket.id,
                            "number": ticket_element_order.ticket.number
                        })

                    if len(tickets_array) != 0:
                        events_array.append({
                            "id": ticket_order.ticket.event.id,
                            "name": ticket_order.ticket.event.name,
                            "tickets": tickets_array,
                            "question": ticket_order.ticket.event.question,
                            "answer": ticket_order.answer.answer
                        })
                    else:
                        response["error"] = "Заказанные билеты на событие не найдены"

            if len(events_array) != 0:
                response["orders"].append({
                    "id": order.id,
                    "create_at": {
                        "date": order.create_at.strftime('%B %d, %Y'),
                        "time": order.create_at.strftime('%I:%M %p'),
                    },
                    "status": order.status,
                    "total": order.amount,
                    "events": events_array
                })

        if len(response["orders"]) == 0:
            response["error"] = "Вы еще не сделали ни один заказ"
        else:
            response["status"] = True
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def hide_order_user(request):
    response = {
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        body = get_body(request=request)
        id_order = body.get('id_order', None)
        if id_order is not None:
            order = Order.objects.filter(pk=id_order, user=user)
            if order:
                order = order[0]
                order.hide = True
                order.save()
                response["status"] = True
            else:
                response["error"] = "Заказ не найден"
        else:
            response["error"] = "Не передан ID заказа"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def hide_event_user(request):
    response = {
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        body = get_body(request=request)
        id_event = body.get('id_event', None)
        if id_event is not None:
            event = Events.objects.filter(pk=id_event)
            if event:
                event = event[0]
                HideUserEvent.objects.create(user=user, event=event)
                response["status"] = True
            else:
                response["error"] = "Событие не найдено"
        else:
            response["error"] = "Не передан ID события"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_my_events(request):
    response = {
        "await_events": [],
        "completed_events": [],
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        hide_events_user = HideUserEvent.objects.filter(user=user.pk)
        array_hides = []
        for ev in hide_events_user:
            try:
                array_hides.append(ev.event.id)
            except Exception as e:
                logging.warning(e)
        tickets_user = Tickets.objects.filter(user=user.pk)
        ready_event = []
        for ticket in tickets_user:
            try:
                if ticket.event.id not in ready_event and ticket.event.id not in array_hides:
                    ready_event.append(ticket.event.id)
            except Exception as e:
                logging.warning(e)

        for event_id in ready_event:
            event = Events.objects.get(pk=event_id)

            tickets_event_user = []
            answer = ""
            for ticket_user in Tickets.objects.filter(event=event.id, user=user.pk):
                tickets_event_user.append({
                    "id": ticket_user.pk,
                    "number": ticket_user.number,
                })
                if answer == "":
                    answer = ticket_user.answer.answer

            image = Images.objects.filter(event=event.id).order_by('-main_image')
            info_event = {
                "id": event.id,
                "name": event.name,
                "competition": {
                    "name": event.competition.name
                },
                "tickets": tickets_event_user,
                "question": event.question,
                "answer": answer,
                "photo": image[0].photo.url,
                "create_at": {
                    "date": event.create_at.strftime('%B %d, %Y'),
                    "time": event.create_at.strftime('%I:%M %p'),
                },
                "finished_at": {
                    "data": event.time_end_event.strftime('%B %d, %Y'),
                    "time": event.time_end_event.strftime('%I:%M %p')
                },
                "sold": event.sold,
                "finished": event.finished,
                "sell": event.sold
            }

            if event.finished:
                info_event['is_winner'] = True if event.winner.id == user.pk else False
                response["completed_events"].append(info_event)
            else:
                response["await_events"].append(info_event)

        if len(tickets_user) != 0:
            response["status"] = True
        else:
            response["error"] = "Ваши события не найдены"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_list_participants(request):
    # print("GET", request.GET)
    # print("POST", request.POST)
    print("body", request.body)

    return JsonResponse(data={"status": True})


@csrf_exempt
def forgot_password(request):
    response = {
        "status": False,
        "error": ""
    }
    body = get_body(request=request)
    email = body.get('email', None)
    logging.warning(f'email forgot password {email}')
    if email is not None:
        user = Users.objects.get(email=email)

        code_link = secrets.token_hex(nbytes=40)
        LinksForgotPassword.objects.create(user=user, code=code_link)
        full_link_recovery = domain + f"/api/forgot-link-password/{code_link}/"
        send_activate_code(user=user, subject='Восстановление пароля',
                           msg=f'<b>Поступила заявка измнеения пароля на Ваш аккаунт.</b><br><br>'
                           f'Если это сделали не вы, то немедленно обратитесь к администрации сайта.<br><br>'
                           f'Ссылка для отправки нового пароля по этому адрессу предоставлена ниже:<br>'
                           f'{full_link_recovery}')
        response["status"] = True
    else:
        response["error"] = "Параметр email не передан"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def recovery_password(request, code):
    new_password = secrets.token_hex(7)
    hash_password = make_password_user(password=new_password)
    link_forgot_password = LinksForgotPassword.objects.filter(code=code)
    if link_forgot_password:
        logging.warning(link_forgot_password.values())
        link_forgot_password = link_forgot_password.first()
        user = Users.objects.filter(pk=link_forgot_password.user.pk)
        if user:
            user = user.first()
            user.password = hash_password
            user.save()
            send_activate_code(user=user, subject='Восстановление пароля',
                               msg=f'<b>Ваш новый пароль от учетной записи на bountywin.com представлен ниже.\n\n'
                               f'Новый пароль: {new_password}</b>')
    return redirect(to=f"{domain}/")


@csrf_exempt
def put_photo_user(request):
    # print("GET", request.GET)
    # print("POST", request.POST)
    print("body", request.body)
    print("file", request.FILES)
    response = {
        "status": False,
        "error": ""
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        name_file = f"{secrets.token_hex(6)}-{int(time.time())}"
        Users.objects.filter(pk=user.pk).update(photo=f'media/{name_file}')
        with open(f'/home/website/bountywin/backend/media/media/{name_file}', 'wb') as new_file:
            new_file.write(request.FILES['photo'].read())
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_order(request):
    response = {
        "status": False,
        "error": "",
        "order": {}
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        body = get_body(request=request)
        id_order = body.get('id_order', None)
        if id_order is not None:
            order = Order.objects.filter(pk=id_order)
            if order:
                order = order[0]
                events_array = []
                ready_events = []
                for ticket_order in TicketsOrders.objects.filter(order=order.id):
                    if ticket_order.ticket.event.id not in ready_events:
                        ready_events.append(ticket_order.ticket.event.id)
                        tickets_array = []

                        for ticket_element_order in TicketsOrders.objects.filter(order=order.id):
                            tickets_array.append({
                                "id": ticket_element_order.ticket.id,
                                "number": ticket_element_order.ticket.number
                            })

                        if len(tickets_array) != 0:
                            events_array.append({
                                "id": ticket_order.ticket.event.id,
                                "name": ticket_order.ticket.event.name,
                                "price": ticket_order.ticket.event.price,
                                "competition_info": {
                                    "name": ticket_order.ticket.event.competition.name,
                                    "description": ticket_order.ticket.event.competition.description
                                },
                                "tickets": tickets_array,
                                "question": ticket_order.ticket.event.question,
                                "answer": ticket_order.answer.answer
                            })
                        else:
                            response["error"] = "Заказанные билеты на событие не найдены"

                response["order"] = {
                    "id": order.id,
                    "create_at": {
                        "date": order.create_at.strftime('%B %d, %Y'),
                        "time": order.create_at.strftime('%I:%M %p'),
                    },
                    "status": order.status,
                    "total": order.amount,
                    "events": events_array,
                    "coupon": {},
                    "points": order.points,
                    "link_payment": order.link_payment
                }
                if order.code is not None:
                    response["order"]["coupon"] = {
                        "code": order.code.name,
                        "discount_percent": order.code.discount_percent,
                        "discount_amount": order.code.discount_amount
                    }
            else:
                response["error"] = "Заказ не найден"
        else:
            response["error"] = "Не передан параметр ID"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_history_points(request):
    response = {
        "status": False,
        "error": "",
        "history": []
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        all_history = HistoryPoints.objects.filter(user=user.pk).order_by('-create_at')
        for history in all_history:
            response["history"].append({
                "points": history.points,
                "operation": history.operation,
                "create_at": {
                    "date": history.create_at.strftime('%B %d, %Y'),
                    "time": history.create_at.strftime('%I:%M %p'),
                }
            })

        if len(response["history"]) == 0:
            response["error"] = "У вас еще не было действий с Power Points"
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))


@csrf_exempt
def get_my_referrals(request):
    response = {
        "status": False,
        "error": "",
        "referrals": []
    }
    token = decode_jwt_token(request=request)
    email = get_email_from_jwt_token(decode_token=token)
    if is_right_password(password=get_password_from_jwt_token(decode_token=token),
                         email=email):
        user = Users.objects.get(email=email)
        referrals_user = Referrals.objects.filter(up_liner=user.pk)
        for referral in referrals_user:
            response["referrals"].append({
                "user": {
                    "email": referral.user.email,
                    "username": referral.user.username
                },
                "create_at": {
                    "date": referral.user.create_at.strftime('%B %d, %Y'),
                    "time": referral.user.create_at.strftime('%I:%M %p'),
                }
            })
    else:
        response["error"] = "Не верный логин или пароль"

    return JsonResponse(data=response, status=define_response(data=response))
