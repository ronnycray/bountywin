import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()

from django.utils import timezone
from django.db.models import Q
from app.models import (
    Users, Tickets, Order
)

import logging
from random import randrange
import json
import schedule
import time

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


def check():
    logging.warning('start check')
    minutes = 7 * 60
    time_now = time.time()
    for ticket in Tickets.objects.filter(Q(sell=False) & Q(booking=True)):
        logging.warning(f"ticket id: {ticket.id}, {float(ticket.booking_time) + minutes <= time_now}")
        if float(ticket.booking_time) + minutes <= time_now:
            ticket.booking_time = 0
            ticket.booking = False
            ticket.save()

    orders_users = Order.objects.filter(Q(payment=False) & Q(cancel_payment=False))
    for order in orders_users:
        if order.create_at < timezone.now() - timezone.timedelta(minutes=10):
            order.status = 'cancel'
            order.cancel_payment = True
            order.save()


if __name__ == '__main__':
    schedule.every(60).seconds.do(check)

    while 1:
        schedule.run_pending()
        time.sleep(1)
