import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()

from django.utils import timezone
from django.core.files import File
from django.db.models import Q
from app.models import (
    Users, Tickets, Order, Events
)

import logging
from random import randrange
import openpyxl
import schedule
import time

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


def check():
    all_events = Events.objects.filter(Q(finished=False))
    for event in all_events:
        print(event.time_end_event.date(), type(event.time_end_event.date()))
        if event.time_end_event < timezone.now():
            file = openpyxl.Workbook()
            naming = f'list_participants_event_{event.id}.xlsx'
            path = '/home/website/bountywin/backend/media/media/list_files/{}'.format(naming)
            file.save(path)
            wb = openpyxl.load_workbook(path)

            # grab the active worksheet
            sheet = wb.active
            sheet['A1'] = 'Номер билета'
            sheet['B1'] = 'Email пользователя'

            i = 2

            for item in Tickets.objects.filter(event=event.id):
                sheet['A{}'.format(i)] = item.number
                sheet['B{}'.format(i)] = item.user.email if item.user is not None else '-'

                i += 1

            wb.save(path)
            doc = open(path, 'rb')
            form = File(doc)
            event.finished = True
            event.doc = form
            event.save()


if __name__ == '__main__':
    schedule.every(30).seconds.do(check)

    while 1:
        schedule.run_pending()
        time.sleep(1)
