import React from "react";
import { Grid, Box } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import logo2 from "../../../constants/bountywiners_mini.png";
import { Link } from "react-router-dom";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";

const Footer = ({ classes }) => {
  return (
    <Grid container className={classes.container}>
      <Grid item xs={2} className={classes.footer_logo}>
        <Link to={"/"} className={classes.logo_link}>
          <img style={{ width: "80%" }} src={logo2} alt={logo2} />
        </Link>
      </Grid>
      <Grid item xs={10} className={classes.footer}>
        <Grid container style={{ justifyContent: "space-between" }}>
          <Grid item xs={6} className={classes.footer_navbar}>
            <Grid container>
              <Grid item xs={6} className={classes.navbar}>
                <Grid container style={{ flexDirection: "column" }}>
                  <Link to={"/"} className={classes.navbar_link}>
                    Домашнаяя страница
                  </Link>
                  <Link
                    to={"/current-competitions"}
                    className={classes.navbar_link}
                  >
                    Текущие розыгрыши
                  </Link>
                  <Link to={"/entry-lists"} className={classes.navbar_link}>
                    Списки участников
                  </Link>
                  <Link
                    to={"/competition-winners"}
                    className={classes.navbar_link}
                  >
                    Победители розыгрышей
                  </Link>
                  <Link to={"/live-draws"} className={classes.navbar_link}>
                    Трансляции розыгрышей
                  </Link>
                </Grid>
              </Grid>
              <Grid item xs={6} className={classes.navbar}>
                <Grid
                  container
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <Link to={"/how-it-works"} className={classes.navbar_link}>
                    Как это работает
                  </Link>
                  <Link to={"/faq"} className={classes.navbar_link}>
                    FAQs
                  </Link>
                  <Link to={"/about"} className={classes.navbar_link}>
                    О нас
                  </Link>
                  <Link to={"/contact"} className={classes.navbar_link}>
                    Связаться с нами
                  </Link>
                  <Link to={"/profile"} className={classes.navbar_link}>
                    Мой Аккаунт
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={4}>
            <Box className={classes.social_links}>
              <a href={"https://instagram.com"} className={classes.link_icon}>
                <InstagramIcon className={classes.icon} />
              </a>
              <a href={"https://youtube.com"} className={classes.link_icon}>
                <YouTubeIcon className={classes.icon} />
              </a>
            </Box>
            <ul className={classes.right_navbar}>
              <li>
                <Link to={"/terms-conditions"} className={classes.navbar_link}>
                  Правила и Условия
                </Link>
              </li>
              <li>
                <Link to={"/privacy-policy"} className={classes.navbar_link}>
                  Политика Конфиденциальности
                </Link>
              </li>
            </ul>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.footer_info}>
        <span>© Copyright Bounty Win 2021</span>
        <span>Developed by The Mildronat Devs</span>
      </Grid>
    </Grid>
  );
};
//политика конфиденциальности
export default withStyles(styles)(Footer);
