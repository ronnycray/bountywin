import React, { useState, useEffect, Fragment } from "react";
import {
  AppBar,
  Grid,
  Paper,
  Toolbar,
  Typography,
  Button,
  Box,
  Badge,
  useScrollTrigger,
  Slide,
} from "@material-ui/core";
import { Link, NavLink } from "react-router-dom";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";
import { styles } from "./styles";
import cart from "../../../constants/basket512.png";
import cashback from "../../../constants/cb512.png";
import profile from "../../../constants/profile512.png";
import logo from "../../../constants/bountywiners.png";

function HideOnScroll({ children, window }) {
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

const Header = (props) => {
  const {
    classes,
    auth: { isAuthenticate },
    cart: { items },
  } = props;
  const [badge, setBadge] = useState(0);
  useEffect(() => {
    setBadge(items.reduce((acc, item) => acc + item.quantity * 1, 0));
  }, []);
  return (
    <HideOnScroll {...props}>
      <AppBar className={classes.appbar}>
        <Toolbar className={classes.first_toolbar}>
          <Grid container>
            <Grid item style={{ margin: "15px 10px", width: "10vw" }}>
              <Link to={"/"}>
                <img src={logo} alt={logo} style={{ width: "100%" }} />
              </Link>
            </Grid>
            <Grid item xs={10} className={classes.first_part_toolbar}>
              <Grid item xs={12} className={classes.link_container}>
                <NavLink
                  className={classes.navLink}
                  activeClassName={classes.active_link}
                  to={"/latest-competitions"}
                >
                  Текущие Розыгрыши
                </NavLink>
                <NavLink
                  className={classes.navLink}
                  activeClassName={classes.active_link}
                  to={"/entry-lists"}
                >
                  Списки Участников
                </NavLink>
                <NavLink
                  className={classes.navLink}
                  activeClassName={classes.active_link}
                  to={"/live-draws"}
                >
                  Трансляции Розыгрышей
                </NavLink>
                <NavLink
                  className={classes.navLink}
                  activeClassName={classes.active_link}
                  to={"/competition-winners"}
                >
                  Наши Победители
                </NavLink>
                <NavLink
                  className={classes.navLink}
                  activeClassName={classes.active_link}
                  to={"/how-it-works"}
                >
                  Как это работает
                </NavLink>
              </Grid>
            </Grid>
            <Grid
              item
              style={{
                position: "absolute",
                display: "flex",
                top: 0,
                bottom: 0,
                right: 0,
                width: "186px",
                padding: "0 24px",
              }}
            >
              <Grid item xs={12} className={classes.link_container_cart}>
                {isAuthenticate ? (
                  <Fragment>
                    <NavLink
                      className={classes.cart_link}
                      activeClassName={classes.active_link}
                      to={"/profile"}
                    >
                      <div>
                        <img
                          style={{ width: "2.25vw" }}
                          src={profile}
                          alt={profile}
                        />
                      </div>
                    </NavLink>
                    <NavLink
                      className={classes.cart_link}
                      activeClassName={classes.active_link}
                      to={"/profile/points"}
                    >
                      <div>
                        <img
                          style={{ width: "2.25vw" }}
                          src={cashback}
                          alt={cashback}
                        />
                      </div>
                    </NavLink>
                  </Fragment>
                ) : (
                  <NavLink
                    className={classes.cart_link}
                    activeClassName={classes.active_link}
                    to={"/login"}
                  >
                    <div>
                      <img
                        style={{ width: "2.25vw" }}
                        src={profile}
                        alt={profile}
                      />
                    </div>
                  </NavLink>
                )}
                <NavLink
                  className={classes.cart_link}
                  activeClassName={classes.active_link}
                  to={"/cart"}
                >
                  <Badge badgeContent={badge} showZero color="error">
                    <div>
                      <img style={{ width: "2.25vw" }} src={cart} alt={cart} />
                    </div>
                  </Badge>
                </NavLink>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
        <Toolbar className={classes.second_toolbar}>
          <Box>
            <a
              href={"https://instagram.com"}
              style={{ textDecoration: "none", color: "white" }}
            >
              <InstagramIcon className={classes.icon} />
            </a>
            <a
              href={"https://youtube.com"}
              style={{ textDecoration: "none", color: "white" }}
            >
              <YouTubeIcon className={classes.icon} />
            </a>
          </Box>
          <Box className={classes.blike_text}>
            Будьте на связи! Если вы выиграли мы сразу попытаемся с вами
            связаться!
          </Box>
          <Box className={classes.auth}>
            <PersonOutlineIcon
              fontSize={"small"}
              className={classes.auth_icon}
            />
            {isAuthenticate ? (
              <Link className={classes.link} to={"/profile"}>
                Профиль
              </Link>
            ) : (
              <Link className={classes.link} to={"/login"}>
                Авторизация
              </Link>
            )}
          </Box>
        </Toolbar>
      </AppBar>
    </HideOnScroll>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  cart: state.cart,
});

export default connect(mapStateToProps)(withStyles(styles)(Header));
