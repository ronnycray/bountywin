export const styles = (theme) => ({
  appbar: {
    backgroundColor: "#1c1b21",
    boxShadow: "unset",
    position: "sticky",
  },
  first_toolbar: {
    borderBottom: "2px solid #55a187",
  },
  first_part_toolbar: {
    backgroundColor: "#1c1b21",
    display: "flex",
    flexDirection: "row",
    padding: "0 150px 0 30px",
  },
  link_container: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
  },
  second_toolbar: {
    display: "flex",
    justifyContent: "space-between",
    padding: "0 50px",

    borderBottom: "2px solid #55a187",
  },
  icon: {
    transition: "0.3s",
    margin: "0 5px",
    "&:hover": {
      fill: "#55a187",
      opacity: 1,
    },
  },
  badge: {
    color: "white",
  },
  auth: {
    display: "flex",
    alignItems: "center",
  },
  auth_icon: {
    margin: "0 5px",
  },
  link: {
    textDecoration: "none",
    fontSize: "14px",
    color: "white",
    transition: "0.3s",
    "&:hover": {
      color: "#55a187",
      opacity: 1,
    },
  },
  navLink: {
    textDecoration: "none",
    fontSize: "1vw",
    color: "white",
    transition: "0.3s",
    fontWeight: "bold",
    "&:hover": {
      opacity: 0.5,
    },
  },
  active_link: {
    color: "#55a187",
  },
  link_container_cart: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    "&::after": {
      content: '""',
      transform: "skew(-18deg)",
      position: "absolute",
      height: "100%",
      // backgroundColor: "white",
      bottom: 0,
      zIndex: 0,
      width: "89%",
      borderLeft: "15px solid #55a187",
      boxSizing: "border-box",
      top: 0,
      left: 0,
    },
    "&::before": {
      content: '""',
      height: "100%",
      // backgroundColor: "white",
      right: 0,
      width: "21%",
      position: "absolute",
      bottom: 0,
      boxSizing: "border-box",
    },
  },
  cart_link: {
    color: "#1c1b21",
    textDecoration: "none",
    transition: "0.3s",
    zIndex: 1,
    fontWeight: "bold",
    "&:hover": {
      opacity: 0.5,
    },
  },
  show: {
    transform: "translate(0, 0)",
    transition: "transform .5s",
  },
  hide: {
    transform: "translate(0, -70px)",
    transition: "transform .5s",
  },
  blike_text: {
    display: "inline-block",
    color: "#55A187",
    fontSize: "18px",
    WebkitAnimation: "blink 2s forwards infinite",
    animation: "blink 2s forwards infinite",
  },
});
