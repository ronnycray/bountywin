import React, { Fragment } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import ProfilePage from "../../../../pages/ProfilePage";

const ProfileRoute = ({
  component: Component,
  auth: { isAuthenticate, loading },
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      !isAuthenticate && !loading ? (
        <Redirect to="/login" />
      ) : (
        <ProfilePage>
          <Component {...props} />
        </ProfilePage>
      )
    }
  />
);

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(ProfileRoute);
