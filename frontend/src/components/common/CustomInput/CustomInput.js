import React, { Fragment } from "react";
import { TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";

const CustomInput = (props) => {
  const {
    label,
    labelColor,
    isReq,
    classes,
    name,
    value,
    onChange,
    error,
    helperText,
    type,
  } = props;

  return (
    <Fragment>
      <div
        className={
          labelColor === "white"
            ? classes.input_label_white
            : classes.input_label_black
        }
      >
        {label} {isReq && <span className={classes.required}>*</span>}
      </div>
      <TextField
        InputProps={{
          className: classes.input,
        }}
        variant="outlined"
        fullWidth
        type={type}
        autoComplete="new-password"
        value={value}
        name={name}
        margin="dense"
        error={error}
        onChange={onChange}
        helperText={helperText}
        {...props}
        label={""}
      />
    </Fragment>
  );
};

export default withStyles(styles)(CustomInput);
