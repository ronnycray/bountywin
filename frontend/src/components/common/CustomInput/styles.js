export const styles = (theme) => ({
  input_label_white: {
    padding: "10px 0",
    color: "white",
    fontSize: "16px",
  },
  input_label_black: {
    padding: "10px 0",
    color: "black",
    fontSize: "16px",
  },
  required: {
    fontWeight: "700",
    color: "#55a187",
    textDecoration: "none",
    border: 0,
  },
  input: {
    borderRadius: "8px",
    backgroundColor: "white",
    fontSize: "16px",
    border: "1px solid gainsboro",
  },
});
