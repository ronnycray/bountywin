import React from "react";
import { Checkbox, Box } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

const GreenCheckbox = withStyles({
  root: {
    color: "#55a187",
    "&$checked": {
      color: "#55a187",
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

const CustomCheckbox = (props) => {
  return (
    <Box style={{ display: "flex", alignItems: "center", marginTop: "5px" }}>
      <GreenCheckbox {...props} />
      <span
        style={
          props.labelColor === "white"
            ? { color: "white", fontSize: "16px" }
            : { color: "black", fontSize: "16px" }
        }
      >
        {props.label}
      </span>
    </Box>
  );
};

export default CustomCheckbox;
