import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  CLEAR_CART,
  SET_ALERT,
  UPDATE_CART_ITEM,
  ADD_COUPON,
  ADD_COUPON_SUCCESS,
  ADD_COUPON_FAILURE,
} from "./types";
import axios from "axios";

const URL = "https://bountywin.com/api";

export const addToCart = (item) => (dispatch, getState) => {
  dispatch({
    type: ADD_TO_CART,
    payload: item,
  });
  dispatch({
    type: SET_ALERT,
    payload: { message: "Товар добавлен в корзину", alertType: "success" },
  });
  localStorage.setItem("cart", JSON.stringify(getState().cart.items));
};

export const removeFromCart = (item) => (dispatch, getState) => {
  dispatch({
    type: REMOVE_FROM_CART,
    payload: item,
  });
  dispatch({
    type: SET_ALERT,
    payload: { message: "Товар удален", alertType: "error" },
  });

  localStorage.setItem("cart", JSON.stringify(getState().cart.items));
};

export const updateCartItem = (item) => (dispatch, getState) => {
  dispatch({
    type: UPDATE_CART_ITEM,
    payload: item,
  });

  localStorage.setItem("cart", JSON.stringify(getState().cart.items));
};

export const clearCart = () => (dispatch) => {
  dispatch({
    type: CLEAR_CART,
  });
  localStorage.removeItem("cart");
};

export const addCoupon = (coupon) => async (dispatch, getState) => {
  const {
    auth: { token },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  };
  const body = {
    code: coupon,
  };
  try {
    dispatch({
      type: ADD_COUPON,
    });
    const res = await axios.post(`${URL}/get_discount_coupon/`, body, config);

    dispatch({
      type: ADD_COUPON_SUCCESS,
      payload: res.data.coupon,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: "Купон применен",
        alertType: "success",
      },
    });
  } catch (err) {
    dispatch({
      type: ADD_COUPON_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

// export const createOrder = (info, items, coupon)
