import axios from "axios";
import { setAlert } from "./alertActions";
import {
  SET_ALERT,
  LOGIN,
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  REGISTER,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  LOGOUT,
  CLEAR_CART,
} from "./types";

const URL = "https://bountywin.com/api";

export const login = ({ username, password, isRemember }) => async (
  dispatch
) => {
  const body = {
    username: username,
    password: password,
  };

  console.log("login body", body);
  try {
    dispatch({
      type: LOGIN,
    });
    const response = await axios.post(`${URL}/authorization_user/`, body);

    const user = {
      username: response.data.username,
      email: response.data.email,
    };
    const payload = {
      user: user,
      token: response.data.token,
    };
    isRemember && localStorage.setItem("user", JSON.stringify(user));
    isRemember && localStorage.setItem("auth", true);
    isRemember &&
      localStorage.setItem("token", JSON.stringify(response.data.token));
    dispatch({
      type: LOGIN_SUCCESS,
      payload: payload,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: `Добро пожаловать ${response.data.email}`,
        alertType: "success",
      },
    });
  } catch (err) {
    dispatch({
      type: LOGIN_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const register = ({ username, email, password }) => async (dispatch) => {
  const body = {
    username: username,
    email: email,
    password: password,
  };

  console.log("register body", body);

  try {
    dispatch({
      type: REGISTER,
    });
    const response = await axios.post(`${URL}/registration_user/`, body);
    const user = {
      username: response.data.username,
      email: response.data.email,
    };
    localStorage.setItem("auth", true);
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("token", JSON.stringify(response.data.token));
    dispatch({
      type: REGISTER_SUCCESS,
      payload: response.data,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: `Вы успешно зарегестрировались. Добро пожаловать ${response.data.username}`,
        alertType: "success",
      },
    });
  } catch (err) {
    dispatch({
      type: REGISTER_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const forgotPassword = ({ email }) => async (dispatch) => {
  const body = {
    email: email,
  };

  console.log("forgot body", body);
  try {
    dispatch({
      type: FORGOT_PASSWORD,
    });
    const res = await axios.post(`${URL}/forgot-password/`, body);

    dispatch({
      type: FORGOT_PASSWORD_SUCCESS,
    });

    setAlert(`Письмо отправлено.`, "success");
  } catch (err) {
    dispatch({
      type: FORGOT_PASSWORD_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const resetPassword = ({ password }) => async (dispatch) => {
  const body = {
    password: password,
  };

  console.log("reset body", body);

  try {
    dispatch({
      type: RESET_PASSWORD,
    });
    const res = await axios.post(`${URL}/reset-password/`, body);

    dispatch({
      type: RESET_PASSWORD_SUCCESS,
    });

    setAlert(`Пароль успешно изменен.`, "success");
  } catch (err) {
    dispatch({
      type: RESET_PASSWORD_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT,
  });
  dispatch({
    type: CLEAR_CART,
  });
  localStorage.clear();
};
