import axios from "axios";
import {
  GET_LAST_EVENTS,
  GET_LAST_EVENTS_SUCCESS,
  GET_LAST_EVENTS_FAILURE,
  SET_ALERT,
  GET_ALL_EVENTS,
  GET_ALL_EVENTS_SUCCESS,
  GET_ALL_EVENTS_FAILURE,
  GET_EVENT,
  GET_EVENT_SUCCESS,
  GET_EVENT_FAILURE,
  GET_ENTRY_LIST,
  GET_ENTRY_LIST_SUCCESS,
  GET_ENTRY_LIST_FAILURE,
  GET_FINISHED_EVENTS,
  GET_FINISHED_EVENTS_SUCCESS,
  GET_FINISHED_EVENTS_FAILURE,
} from "./types";

const URL = "https://bountywin.com/api";

export const getLastEvents = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_LAST_EVENTS,
    });
    const response = await axios.get(`${URL}/get_last_events/`, config);

    dispatch({
      type: GET_LAST_EVENTS_SUCCESS,
      payload: response.data.events,
    });
  } catch (err) {
    dispatch({
      type: GET_LAST_EVENTS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getAllEvents = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_ALL_EVENTS,
    });
    const response = await axios.get(`${URL}/get_all_events/`, config);

    dispatch({
      type: GET_ALL_EVENTS_SUCCESS,
      payload: response.data.events,
    });
  } catch (err) {
    dispatch({
      type: GET_ALL_EVENTS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getEndedEvents = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_FINISHED_EVENTS,
    });
    const response = await axios.get(`${URL}/get_finished_events/`, config);

    dispatch({
      type: GET_FINISHED_EVENTS_SUCCESS,
      payload: response.data.events,
    });
  } catch (err) {
    dispatch({
      type: GET_FINISHED_EVENTS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getEvent = (id) => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_EVENT,
    });
    const body = {
      id_event: id,
    };
    const response = await axios.post(`${URL}/get_event/`, body);

    dispatch({
      type: GET_EVENT_SUCCESS,
      payload: response.data.event,
    });
  } catch (err) {
    dispatch({
      type: GET_EVENT_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getEntryList = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_ENTRY_LIST,
    });
    const response = await axios.get(`${URL}/get_entry_list/`);

    dispatch({
      type: GET_ENTRY_LIST_SUCCESS,
      payload: response.data.events,
    });
  } catch (err) {
    dispatch({
      type: GET_ENTRY_LIST_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};
