import axios from "axios";
import {
  GET_STATISTICS,
  GET_STATISTICS_SUCCESS,
  GET_STATISTICS_FAILURE,
  SET_ALERT,
} from "./types";

const URL = "https://bountywin.com/api";

export const getStatistics = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_STATISTICS,
    });
    const response = await axios.get(`${URL}/get_statistics/`, config);

    dispatch({
      type: GET_STATISTICS_SUCCESS,
      payload: response.data.statistic,
    });
  } catch (err) {
    dispatch({
      type: GET_STATISTICS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};
