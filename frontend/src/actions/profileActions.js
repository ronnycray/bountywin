import axios from "axios";
import {
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_FAILURE,
  CHANGE_PASSWORD_SUCCESS,
  GET_PROFILE_INFO,
  GET_PROFILE_INFO_SUCCESS,
  GET_PROFILE_INFO_FAILURE,
  GET_REGIONS,
  GET_REGIONS_SUCCESS,
  GET_REGIONS_FAILURE,
  CHANGE_EMAIL,
  CHANGE_EMAIL_SUCCESS,
  CHANGE_EMAIL_FAILURE,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAILURE,
  SET_ALERT,
} from "./types";

const URL = "https://bountywin.com/api";

export const getProfile = () => async (dispatch, getState) => {
  const {
    auth: { token },
  } = getState();
  console.log("TOKEN", token);
  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `${token}`,
    },
  };
  try {
    dispatch({
      type: GET_PROFILE_INFO,
    });
    const res = await axios.get(`${URL}/get_info_user/`, config);

    dispatch({
      type: GET_PROFILE_INFO_SUCCESS,
      payload: res.data.info,
    });
  } catch (err) {
    dispatch({
      type: GET_PROFILE_INFO_FAILURE,
    });

    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getRegions = () => async (dispatch, getState) => {
  const {
    auth: { token },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  };
  try {
    dispatch({
      type: GET_REGIONS,
    });

    const res = await axios.get(`${URL}/get_regions/`, config);
    dispatch({
      type: GET_REGIONS_SUCCESS,
      payload: res.data.regions,
    });
  } catch (err) {
    dispatch({
      type: GET_REGIONS_FAILURE,
    });

    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const changePassword = (old_password, password) => async (
  dispatch,
  getState
) => {
  const {
    auth: { token },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  };
  const body = {
    password: old_password,
    new_password: password,
  };
  try {
    dispatch({
      type: CHANGE_PASSWORD,
    });
    const res = await axios.post(`${URL}/change_password/`, body, config);

    dispatch({
      type: CHANGE_PASSWORD_SUCCESS,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: "Вам отправлено письмо с подтверждением смены пароля на почту",
        alertType: "success",
      },
    });
  } catch (err) {
    dispatch({
      type: CHANGE_PASSWORD_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const changeEmail = (email, password) => async (dispatch, getState) => {
  const {
    auth: { token },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  };
  const body = {
    email: email,
    password: password,
  };
  try {
    dispatch({
      type: CHANGE_EMAIL,
    });
    const res = await axios.post(`${URL}/change_email/`, body, config);

    dispatch({
      type: CHANGE_EMAIL_SUCCESS,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: "E-mail успешно изменен",
        alertType: "success",
      },
    });
    getProfile();
  } catch (err) {
    dispatch({
      type: CHANGE_EMAIL_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const updateProfile = (values, photo) => async (dispatch, getState) => {
  const {
    auth: { token },
  } = getState();

  const config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  };
  console.log("VAAAALUUUES", values);
  console.log("PHOTO", photo);
  const body = {
    first_name: values.first_name,
    second_name: values.last_name,
    company: values.company,
    country: values.country.country_id,
    city: values.city.region_id,
    address: values.address,
    postcode: values.postal,
    phone: values.phone,
    day_birth: values.date_of_birth,
  };
  try {
    dispatch({
      type: UPDATE_PROFILE,
    });
    const res = await axios.post(`${URL}/update_profile/`, body, config);

    dispatch({
      type: UPDATE_PROFILE_SUCCESS,
    });

    dispatch({
      type: SET_ALERT,
      payload: {
        message: "Вашин данные успешно изменены",
        alertType: "success",
      },
    });
    getProfile();
  } catch (err) {
    dispatch({
      type: UPDATE_PROFILE_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};
