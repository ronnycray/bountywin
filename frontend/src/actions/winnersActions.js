import axios from "axios";
import {
  GET_WINNERS,
  GET_WINNERS_SUCCESS,
  GET_WINNERS_FAILURE,
  GET_TOP_WINNERS,
  GET_TOP_WINNERS_SUCCESS,
  GET_TOP_WINNERS_FAILURE,
  SET_ALERT,
} from "./types";

const URL = "https://bountywin.com/api";

export const getWinners = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_WINNERS,
    });
    const response = await axios.get(`${URL}/get_winners/`, config);

    dispatch({
      type: GET_WINNERS_SUCCESS,
      payload: response.data.winners,
    });
  } catch (err) {
    dispatch({
      type: GET_WINNERS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};

export const getTopWinners = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_TOP_WINNERS,
    });
    const response = await axios.get(`${URL}/get_top_winners/`, config);

    dispatch({
      type: GET_TOP_WINNERS_SUCCESS,
      payload: response.data.winners,
    });
  } catch (err) {
    dispatch({
      type: GET_TOP_WINNERS_FAILURE,
    });
    dispatch({
      type: SET_ALERT,
      payload: { message: err.response.data.error, alertType: "error" },
    });
  }
};
