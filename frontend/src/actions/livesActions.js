import axios from "axios";
import {
  GET_LIVES,
  GET_LIVES_SUCCESS,
  GET_LIVES_FAILURE,
  SET_ALERT,
} from "./types";

const URL = "https://bountywin.com/api";

export const getLives = () => async (dispatch) => {
  const config = {};
  try {
    dispatch({
      type: GET_LIVES,
    });
    const response = await axios.get(`${URL}/get_broadcasts/`, config);
    console.log("RESPONSE DATA", response.data.broadcasts);
    dispatch({
      type: GET_LIVES_SUCCESS,
      payload: response.data.broadcasts,
    });
  } catch (err) {
    // dispatch({
    //   type: GET_LIVES_FAILURE,
    // });
    // dispatch({
    //   type: SET_ALERT,
    //   payload: { message: err.response.data.error, alertType: "error" },
    // });
  }
};
