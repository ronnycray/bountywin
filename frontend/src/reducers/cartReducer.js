import { initial } from "lodash";
import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  CLEAR_CART,
  UPDATE_CART_ITEM,
  ADD_COUPON,
  ADD_COUPON_SUCCESS,
  ADD_COUPON_FAILURE,
} from "../actions/types";

const cartFromStorage = localStorage.getItem("cart")
  ? JSON.parse(localStorage.getItem("cart"))
  : [];

const initialState = {
  items: cartFromStorage,
  coupon: {},
  loading: false,
};

export default function cartReducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case ADD_TO_CART:
      const existItem = state.items.find((x) => x.id === payload.id);
      if (existItem) {
        return {
          ...state,
          items: state.items.map((x) => (x.id === existItem.id ? payload : x)),
        };
      } else {
        return {
          ...state,
          items: [...state.items, payload],
        };
      }
    case REMOVE_FROM_CART:
      return {
        ...state,
        items: state.items.filter((x) => x.id !== payload.id),
      };
    case UPDATE_CART_ITEM:
      const newItems = state.items.map((item) => {
        if (item.id === payload.id) {
          item.quantity = payload.quantity;
          return item;
        }
        return item;
      });
      return {
        ...state,
        items: newItems,
      };
    case ADD_COUPON:
      return {
        ...state,
        loading: true,
      };
    case ADD_COUPON_SUCCESS:
      return {
        ...state,
        loading: false,
        coupon: payload,
      };
    case ADD_COUPON_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case CLEAR_CART:
      return {
        ...state,
        items: [],
      };
    default:
      return state;
  }
}
