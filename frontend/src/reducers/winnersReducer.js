import {
  GET_WINNERS,
  GET_WINNERS_SUCCESS,
  GET_WINNERS_FAILURE,
  GET_TOP_WINNERS,
  GET_TOP_WINNERS_SUCCESS,
  GET_TOP_WINNERS_FAILURE,
} from "../actions/types";

const initialState = {
  winners: [],
  top_winners: [],
  loading: true,
  top_loading: true,
};

export default function winnersReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_WINNERS:
      return {
        ...state,
        loading: true,
      };
    case GET_WINNERS_SUCCESS:
      return {
        ...state,
        loading: false,
        winners: payload,
      };
    case GET_WINNERS_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case GET_TOP_WINNERS:
      return {
        ...state,
        top_loading: true,
      };
    case GET_TOP_WINNERS_SUCCESS:
      return {
        ...state,
        top_loading: false,
        top_winners: payload,
      };
    case GET_TOP_WINNERS_FAILURE:
      return {
        ...state,
        top_loading: false,
      };
    default:
      return state;
  }
}
