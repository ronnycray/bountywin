import {
  GET_LIVES,
  GET_LIVES_SUCCESS,
  GET_LIVES_FAILURE,
} from "../actions/types";

const initialState = {
  lives: [],
  loading: false,
};

export default function livesReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_LIVES:
      return {
        ...state,
        loading: true,
      };
    case GET_LIVES_SUCCESS:
      return {
        ...state,
        loading: false,
        lives: payload,
      };
    case GET_LIVES_FAILURE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}
