import {
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_FAILURE,
  CHANGE_PASSWORD_SUCCESS,
  GET_PROFILE_INFO,
  GET_PROFILE_INFO_SUCCESS,
  GET_PROFILE_INFO_FAILURE,
  GET_REGIONS,
  GET_REGIONS_SUCCESS,
  GET_REGIONS_FAILURE,
  UPDATE_PROFILE,
  UPDATE_PROFILE_SUCCESS,
  UPDATE_PROFILE_FAILURE,
  CHANGE_EMAIL,
  CHANGE_EMAIL_SUCCESS,
  CHANGE_EMAIL_FAILURE,
} from "../actions/types";

const initialState = {
  info: {},
  regions: [],
  loading: false,
  loading_regions: false,
};

export default function profileReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_PROFILE_INFO:
    case UPDATE_PROFILE:
    case CHANGE_PASSWORD:
    case CHANGE_EMAIL:
      return {
        ...state,
        loading: true,
      };
    case GET_REGIONS:
      return {
        ...state,
        loading_regions: true,
      };
    case GET_PROFILE_INFO_SUCCESS:
      return {
        ...state,
        loading: false,
        info: payload,
      };
    case UPDATE_PROFILE_SUCCESS:
    case CHANGE_PASSWORD_SUCCESS:
    case CHANGE_EMAIL_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case GET_REGIONS_SUCCESS:
      return {
        ...state,
        loading_regions: false,
        regions: payload,
      };
    case UPDATE_PROFILE_FAILURE:
    case CHANGE_PASSWORD_FAILURE:
    case CHANGE_EMAIL_FAILURE:
    case GET_PROFILE_INFO_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case GET_REGIONS_FAILURE:
      return {
        ...state,
        loading_regions: false,
      };
    default:
      return state;
  }
}
