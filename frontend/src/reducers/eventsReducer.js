import {
  GET_LAST_EVENTS,
  GET_LAST_EVENTS_SUCCESS,
  GET_LAST_EVENTS_FAILURE,
  GET_ALL_EVENTS,
  GET_ALL_EVENTS_SUCCESS,
  GET_ALL_EVENTS_FAILURE,
  GET_EVENT,
  GET_EVENT_SUCCESS,
  GET_EVENT_FAILURE,
  GET_ENTRY_LIST,
  GET_ENTRY_LIST_SUCCESS,
  GET_ENTRY_LIST_FAILURE,
  GET_FINISHED_EVENTS,
  GET_FINISHED_EVENTS_SUCCESS,
  GET_FINISHED_EVENTS_FAILURE,
} from "../actions/types";

const initialState = {
  event: {},
  all_events: [
    // {
    //   id: 1,
    //   name: "",
    //   competition_info: {
    //     name: "",
    //     description: "",
    //   },
    //   price: 1.0,
    //   photo: "",
    //   bhp: "",
    //   speed: "",
    //   mpg: "",
    //   miles: "",
    //   year: "",
    // },
  ],
  last_events: [],
  entry_list: [],
  ended_events: [],
  loading: false,
  last_loading: true,
  all_loading: true,
  entry_loading: true,
};

export default function eventsReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_LAST_EVENTS:
      return {
        ...state,
        last_loading: true,
      };
    case GET_LAST_EVENTS_SUCCESS:
      return {
        ...state,
        last_loading: false,
        last_events: payload,
      };
    case GET_LAST_EVENTS_FAILURE:
      return {
        ...state,
        last_loading: false,
      };
    case GET_ALL_EVENTS:
    case GET_FINISHED_EVENTS:
      return {
        ...state,
        all_loading: true,
      };
    case GET_ALL_EVENTS_SUCCESS:
      return {
        ...state,
        all_loading: false,
        all_events: payload,
      };
    case GET_FINISHED_EVENTS_SUCCESS:
      return {
        ...state,
        all_loading: false,
        ended_events: payload,
      };
    case GET_ALL_EVENTS_FAILURE:
    case GET_FINISHED_EVENTS_FAILURE:
      return {
        ...state,
        all_loading: false,
      };
    case GET_EVENT:
      return {
        ...state,
        loading: true,
      };
    case GET_EVENT_SUCCESS:
      return {
        ...state,
        loading: false,
        event: payload,
      };
    case GET_EVENT_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case GET_ENTRY_LIST:
      return {
        ...state,
        entry_loading: true,
      };
    case GET_ENTRY_LIST_SUCCESS:
      return {
        ...state,
        entry_loading: false,
        entry_list: payload,
      };
    case GET_ENTRY_LIST_FAILURE:
      return {
        ...state,
        entry_loading: false,
      };
    default:
      return state;
  }
}
