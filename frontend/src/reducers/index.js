import { combineReducers } from "redux";
import authReducer from "./authReducer";
import alertReducer from "./alertReducer";
import statisticsReducer from "./statisticsReducer";
import eventsReducer from "./eventsReducer";
import winnersReducer from "./winnersReducer";
import cartReducer from "./cartReducer";
import profileReducer from "./profileReducer";
import livesReducer from "./livesReducer";

export default combineReducers({
  auth: authReducer,
  alert: alertReducer,
  statistic: statisticsReducer,
  events: eventsReducer,
  winners: winnersReducer,
  cart: cartReducer,
  profile: profileReducer,
  lives: livesReducer,
});
