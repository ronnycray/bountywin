import {
  GET_STATISTICS,
  GET_STATISTICS_SUCCESS,
  GET_STATISTICS_FAILURE,
} from "../actions/types";

const initialState = {
  statistic: {},
  loading: true,
};

export default function statisticsReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_STATISTICS:
      return {
        ...state,
        loading: true,
      };
    case GET_STATISTICS_SUCCESS:
      return {
        ...state,
        loading: false,
        statistic: payload,
      };
    case GET_STATISTICS_FAILURE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}
