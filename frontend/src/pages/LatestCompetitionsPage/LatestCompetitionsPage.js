import React, { Fragment, useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Container,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CircularProgress,
  SvgIcon,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { styles } from "./styles";
import TimerIcon from "@material-ui/icons/Timer";
import ConfirmationNumberIcon from "@material-ui/icons/ConfirmationNumber";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import { getAllEvents, getEndedEvents } from "../../actions/eventsActions";

// const data = [
//   {
//     id: "1",
//     name: "Ferrari 458 Spider & £10,000 OR £110,000 Tax Free Cash!!",
//     newAmount: "10.48",
//     amount: "11.49",
//     time: "8d",
//     tickets: "0",
//     maxpp: "10",
//     date: "26/02/2021 9:15 pm",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF6785-1076x806.jpg",
//   },
//   {
//     id: "2",
//     name: "Audi A1-The Learner Package",
//     amount: "1.35",
//     time: "2d",
//     tickets: "3034",
//     maxpp: "80",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5060-1076x806.jpg",
//   },
//   {
//     id: "3",
//     name: "Audi A1-The Learner Package",
//     newAmount: "2.89",
//     amount: "3.49",
//     time: "2d",
//     tickets: "3972",
//     maxpp: "35",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5273-1076x806.jpg",
//   },
//   {
//     id: "4",
//     name: "Audi A1-The Learner Package",
//     amount: "1.35",
//     time: "2d",
//     tickets: "3034",
//     maxpp: "80",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5060-1076x806.jpg",
//   },
//   {
//     id: "3",
//     name: "Audi A1-The Learner Package",
//     newAmount: "2.89",
//     amount: "3.49",
//     time: "2d",
//     tickets: "3972",
//     maxpp: "35",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5273-1076x806.jpg",
//   },
//   {
//     id: "1",
//     name: "Ferrari 458 Spider & £10,000 OR £110,000 Tax Free Cash!!",
//     newAmount: "10.48",
//     amount: "11.49",
//     time: "8d",
//     tickets: "10716",
//     maxpp: "10",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF6785-1076x806.jpg",
//   },
//   {
//     id: "3",
//     name: "Audi A1-The Learner Package",
//     newAmount: "2.89",
//     amount: "3.49",
//     time: "2d",
//     tickets: "3972",
//     maxpp: "35",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5273-1076x806.jpg",
//   },
//   {
//     id: "1",
//     name: "Ferrari 458 Spider & £10,000 OR £110,000 Tax Free Cash!!",
//     newAmount: "10.48",
//     amount: "11.49",
//     time: "8d",
//     tickets: "10716",
//     maxpp: "10",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF6785-1076x806.jpg",
//   },
//   {
//     id: "2",
//     name: "Audi A1-The Learner Package",
//     amount: "1.35",
//     time: "2d",
//     tickets: "3034",
//     maxpp: "80",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5060-1076x806.jpg",
//   },
// ];
const LatestCompetitionsPage = ({
  classes,
  events,
  getAllEvents,
  history,
  getEndedEvents,
}) => {
  const { all_loading, all_events, ended_events } = events;
  const [isFirstActive, setIsFirstActive] = useState(true);
  const [isSecondActive, setIsSecondActive] = useState(false);
  const [newEvents, setNewEvents] = useState(all_events);

  useEffect(async () => {
    await getAllEvents();
    await getEndedEvents();

    setNewEvents(all_events);
  }, []);

  const filteredEvents = all_events.filter((item) => {
    console.log("ITEMS", item);
    return item.competition_info.name.toLowerCase() === "машины";
  });

  const handleFirstClick = () => {
    console.log("FIRST CLICK");
    setIsFirstActive(true);
    setIsSecondActive(false);
    setNewEvents(all_events);
  };

  console.log("FILWINNERS", newEvents);

  const handleSecondClick = () => {
    console.log("SECOND CLICK");
    setIsFirstActive(false);
    setIsSecondActive(true);
    setNewEvents(filteredEvents);
  };
  const handleClick = (item) => {
    console.log("item", item);
    history.push(`/competition/${item}`);
  };
  return (
    <Grid container>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>ТЕКУЩИЕ РОЗЫГРЫШИ</Typography>
        </Grid>
      </Grid>
      <Container maxWidth="xl" style={{ padding: "0 100px 110px" }}>
        <Grid container>
          <Grid item xs={2} className={classes.switcher_container}>
            <div onClick={handleFirstClick}>
              <Link
                className={`${classes.switcher_link} ${
                  isFirstActive && classes.link_active
                }`}
              >
                ВСЕ
              </Link>
            </div>
            <div onClick={handleSecondClick}>
              <Link
                className={`${classes.switcher_link} ${
                  isSecondActive && classes.link_active
                }`}
              >
                АВТОМОБИЛИ
              </Link>
            </div>
          </Grid>
          {all_loading ? (
            <Grid
              item
              xs={12}
              style={{
                padding: "48px 0",
                display: "flex",
                justifyContent: "center",
                marginTop: "70px",
              }}
            >
              <CircularProgress color="secondary" size={100} />
            </Grid>
          ) : (
            <Grid item xs={12}>
              <Grid container style={{ marginTop: "10px" }}>
                {newEvents[0] &&
                  newEvents.map((item) => {
                    return (
                      <Grid
                        xs={4}
                        style={{ padding: "5px 15px", marginBottom: "10px" }}
                      >
                        <Card className={classes.card_root}>
                          <CardActionArea onClick={() => handleClick(item.id)}>
                            <CardContent
                              style={{ padding: "0", position: "relative" }}
                            >
                              <CardMedia
                                className={classes.media}
                                image={`${item.photo[0]}`}
                                title="Contemplative Reptile"
                                style={{
                                  aspectRatio: "auto 588 / 424",
                                  height: "20vw",
                                }}
                              />
                              {item.tickets * 1 === 0 && (
                                <span className={classes.span}>
                                  <span className={classes.sold_span_body}>
                                    ПРОДАНО!
                                  </span>
                                </span>
                              )}
                              {item.tickets * 1 !== 0 &&
                                item.now_price * 1 > 0 && (
                                  <span className={classes.span}>
                                    <span className={classes.launch_span_body}>
                                      СКИДКА!
                                    </span>
                                  </span>
                                )}
                              {item.tickets * 1 === 0 && (
                                <div className={classes.today_span}>
                                  Розыгрыш сегодня в 9:15 pm!
                                </div>
                              )}
                            </CardContent>
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h5"
                                component="h2"
                                className={classes.card_name}
                              >
                                {item.name}
                              </Typography>
                              {item.tickets * 1 !== 0 && (
                                <Fragment>
                                  {" "}
                                  {item.now_price ? (
                                    <span className={classes.price}>
                                      <del>
                                        <span className={classes.del}>
                                          {item.price}
                                          <span>₽</span>
                                        </span>
                                      </del>
                                      <ins className={classes.ins}>
                                        <span>
                                          {item.now_price}
                                          <span>₽</span>
                                        </span>
                                      </ins>
                                    </span>
                                  ) : (
                                    <span className={classes.price}>
                                      <span>
                                        {item.price}
                                        <span>₽</span>
                                      </span>
                                    </span>
                                  )}
                                </Fragment>
                              )}
                            </CardContent>
                            {item.tickets * 1 === 0 ? (
                              <CardContent className={classes.cart_footer}>
                                <div className={classes.footer_text}>
                                  <TimerIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  Draw Date: {item.date}
                                </div>
                              </CardContent>
                            ) : (
                              <CardContent className={classes.cart_footer}>
                                <div className={classes.footer_text}>
                                  <TimerIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  {item.time}
                                </div>
                                <div className={classes.footer_text}>
                                  <SvgIcon
                                    style={{
                                      fontSize: "18px",
                                      marginRight: "5px",
                                    }}
                                  >
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M448.678,128.219l-10.607,10.608c-8.667,8.667-20.191,13.44-32.449,13.44c-12.258,0-23.78-4.773-32.448-13.44
			c-8.667-8.667-13.44-20.191-13.44-32.448s4.773-23.781,13.44-32.449l10.608-10.608L320.459,0L0,320.459l63.322,63.322
			l10.608-10.608c8.667-8.667,20.191-13.44,32.449-13.44c12.258,0,23.78,4.773,32.448,13.44c8.667,8.667,13.44,20.191,13.44,32.448
			s-4.773,23.781-13.44,32.449l-10.608,10.608L191.541,512L512,191.541L448.678,128.219z M169.61,447.636
			c8.237-12.343,12.662-26.839,12.662-42.015c0-20.272-7.894-39.33-22.229-53.664c-14.334-14.335-33.393-22.229-53.664-22.229
			c-15.176,0-29.672,4.425-42.015,12.662l-21.932-21.931L320.459,42.432l21.931,21.932c-8.237,12.343-12.662,26.839-12.662,42.015
			c0,20.272,7.894,39.33,22.229,53.664c14.334,14.335,33.393,22.229,53.664,22.229c15.176,0,29.672-4.425,42.015-12.662
			l21.932,21.931L191.541,469.568L169.61,447.636z"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="284.001"
                                            y="197.94"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -63.0395 273.8137)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="241.404"
                                            y="155.325"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -45.3819 231.2119)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="326.607"
                                            y="240.541"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -80.684 316.4184)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                    </svg>
                                  </SvgIcon>
                                  {item.tickets} Осталось
                                </div>
                                <div className={classes.footer_text}>
                                  <PersonOutlineIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  макс. {item.limit_person} н.ч.
                                </div>
                              </CardContent>
                            )}
                          </CardActionArea>
                        </Card>
                      </Grid>
                    );
                  })}
              </Grid>
            </Grid>
          )}
        </Grid>
      </Container>
      <Grid container>
        <Grid
          item
          xs={12}
          style={{
            borderTop: "2px solid #55a187",
            position: "relative",
            paddingBottom: "110px",
            backgroundColor: "#1c1b21",
          }}
        >
          <Grid container>
            <Grid item>
              <Typography className={classes.second_slider_header}>
                Завершившиеся розыгрыши
              </Typography>
            </Grid>
          </Grid>
          {all_loading ? (
            <Grid
              item
              xs={12}
              style={{
                padding: "48px 0",
                display: "flex",
                justifyContent: "center",
                marginTop: "70px",
              }}
            >
              <CircularProgress color="secondary" size={100} />
            </Grid>
          ) : (
            <Grid item xs={12}>
              <Grid container style={{ marginTop: "10px", padding: "0 40px" }}>
                {ended_events[0] &&
                  ended_events.map((item) => {
                    return (
                      <Grid
                        xs={4}
                        style={{ padding: "5px 15px", marginBottom: "10px" }}
                      >
                        <Card className={classes.card_root}>
                          <CardActionArea onClick={() => handleClick(item.id)}>
                            <CardContent
                              style={{ padding: "0", position: "relative" }}
                            >
                              <CardMedia
                                className={classes.media}
                                image={`${item.images[0]}`}
                                title="Contemplative Reptile"
                                style={{
                                  aspectRatio: "auto 588 / 424",
                                  height: "20vw",
                                }}
                              />
                              <span className={classes.span}>
                                <span className={classes.sold_span_body}>
                                  ЗАКОНЧЕН!
                                </span>
                              </span>
                            </CardContent>
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h5"
                                component="h2"
                                className={classes.card_name}
                              >
                                {item.name}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </Grid>
                    );
                  })}
              </Grid>
            </Grid>
          )}
          <Link to={"/past-competitions"} className={"link-to"}>
            Все прошедшие розыгрыши
          </Link>
        </Grid>
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  events: state.events,
});

export default connect(mapStateToProps, { getAllEvents, getEndedEvents })(
  withStyles(styles)(LatestCompetitionsPage)
);
