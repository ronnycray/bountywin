import React, { Fragment, useEffect } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { styles } from "./styles";
import "./style.css";
import MainSlider from "./sections/MainSlider/MainSlider";
import Statistics from "./sections/Statistics/Statistics";
import CompetitionsSlider from "./sections/CompetitionsSlider/CompetitionsSlider";
import WinYourCar from "./sections/WInYourCar/WinYourCar";
// import TopWinners from "./sections/TopWinners/TopWinners";
import NewWinners from "./sections/NewWinners/NewWinners";

// const items = [
//   {
//     id: "1",
//     name: "Ferrari 458 Spider & £10,000 OR £110,000 Tax Free Cash!!",
//     newAmount: "10.48",
//     amount: "11.49",
//     time: "8d",
//     tickets: "0",
//     maxpp: "10",
//     date: "26/02/2021 9:15 pm",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF6785-1076x806.jpg",
//   },
//   {
//     id: "2",
//     name: "Audi A1-The Learner Package",
//     amount: "1.35",
//     time: "2d",
//     tickets: "3034",
//     maxpp: "80",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5060-1076x806.jpg",
//   },
//   {
//     id: "3",
//     name: "Audi A1-The Learner Package",
//     newAmount: "2.89",
//     amount: "3.49",
//     time: "2d",
//     tickets: "3972",
//     maxpp: "35",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5273-1076x806.jpg",
//   },
//   {
//     id: "4",
//     name: "Audi A1-The Learner Package",
//     amount: "1.35",
//     time: "2d",
//     tickets: "3034",
//     maxpp: "80",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5060-1076x806.jpg",
//   },
//   {
//     id: "3",
//     name: "Audi A1-The Learner Package",
//     newAmount: "2.89",
//     amount: "3.49",
//     time: "2d",
//     tickets: "3972",
//     maxpp: "35",
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/DSCF5273-1076x806.jpg",
//   },
// ];

// const items2 = [
//   {
//     id: 1,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2020/12/Ally-Watson.jpg",
//     comment:
//       "I won the Audi S3 a few weeks ago. Been buying tickets almost from the start, so to finally land a winner was an absolutely surreal feeling. Won with only 1 ticket bought. The car is incredible. A proper little rocket. It really shifts!",
//     author: "Ally Watson",
//   },
//   {
//     id: 2,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2020/12/Paul-Wilmott-1024x768.jpg",
//     comment:
//       "Thank you Dream Car Giveaways. Still can’t believe I won a dream car ",
//     author: "Paul Wilmoutt",
//   },
//   {
//     id: 3,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2020/07/GTR-1024x768.jpg",
//     comment:
//       "I managed to win the amazing R34 GTR V-Spec a little while back with only 1 ticket, the car was absolutely faultless, the guys were amazing to deal with and made sure the car was perfect, when I got that call it was just the best feeling in the world, feel very lucky, absolutely life changing moment. Been recommending everybody to get on this, thanks again and I’m still buying tickets! ",
//     author: "James Seeley",
//   },
// ];

// const items3 = [
//   {
//     id: 1,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     img:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
// ];

// const statistic = {
//   winners: "1000+",
//   prizes: "1500+",
//   charity: "123К",
//   followers: "123К",
// };

const HomePage = ({ statistic, events, winners, history }) => {
  console.log("statistic", statistic);
  console.log("events", events);
  console.log("winners", winners);

  return (
    <Fragment>
      <Grid container style={{ backgroundColor: "#1c1b21" }}>
        <MainSlider items={events} />
        <Statistics statistics={statistic} />
        <CompetitionsSlider items={events} history={history} />
      </Grid>
      <WinYourCar />
      {/* <TopWinners items={winners} /> */}
      <NewWinners items={winners} />
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  statistic: state.statistic,
  events: state.events,
  winners: state.winners,
});

export default connect(mapStateToProps)(withStyles(styles)(HomePage));
