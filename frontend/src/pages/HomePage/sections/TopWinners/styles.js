export const styles = (theme) => ({
  header_dark: {
    fontSize: "34px",
    padding: "5px 8px 5px 40px",
    backgroundColor: "#1c1b21",
    color: "white",
    zIndex: 2,
    position: "absolute",
    "&::after": {
      content: '""',
      transform: "skew(-18deg)",
      position: "absolute",
      width: "40%",
      height: "100%",
      backgroundColor: "#1c1b21",
      zIndex: -1,
      boxSizing: "border-box",
      display: "block",
      top: 0,
      bottom: 0,
      right: "-12%",
    },
  },
  topwinner_body: {
    padding: "32px 128px 16px 32px",
    position: "relative",
  },
  topwinner_body_text: {
    fontSize: "22px",
    marginBottom: "8px",
    lineHeight: 1.2,
  },
  topwinner_body_author: {
    fontSize: "15px",
    fontWeight: "bold",
  },
});
