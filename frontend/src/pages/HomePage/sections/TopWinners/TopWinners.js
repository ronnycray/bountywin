import React, { useState, useEffect, Fragment } from "react";
import { Grid, Typography, CircularProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Carousel, {
  Dots,
  slidesToShowPlugin,
  autoplayPlugin,
} from "@brainhubeu/react-carousel";
import { styles } from "./styles";
import { Link } from "react-router-dom";

const TopWinners = ({ classes, items }) => {
  const { top_winners, top_loading } = items;
  console.log("top_winners", top_winners);
  console.log("top_loading", top_loading);
  return (
    <Grid
      container
      style={{
        position: "relative",
        backgroundColor: "#55A187",
        color: "white",
      }}
    >
      <Typography className={classes.header_dark}>ТОП ПОБЕДИТЕЛИ</Typography>
      {top_loading ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <CircularProgress color="primary" size={100} />
        </Grid>
      ) : (
        <Fragment>
          <Carousel
            plugins={[
              "infinite",
              {
                resolve: autoplayPlugin,
                options: {
                  interval: 2000,
                },
              },
              {
                resolve: slidesToShowPlugin,
                options: {
                  numberOfSlides: 1,
                },
              },
            ]}
            animationSpeed={1000}
          >
            {top_winners[0] &&
              top_winners.map((item) => {
                return (
                  <div
                    style={{
                      position: "relative",
                      display: "flex",
                      alignItems: "center",
                      width: "100%",
                    }}
                  >
                    <div
                      className={"topwinner-img"}
                      style={{
                        backgroundImage: `url(${item.photo})`,
                      }}
                    />
                    <div className={classes.topwinner_body}>
                      <div className={classes.topwinner_body_text}>
                        "{item.content}"
                      </div>
                      <div className={classes.topwinner_body_author}>
                        - {item.author}
                      </div>
                    </div>
                  </div>
                );
              })}
          </Carousel>

          <Link to={"/competition-winners"} className={"link-to"}>
            Все победители
          </Link>
        </Fragment>
      )}
    </Grid>
  );
};

export default withStyles(styles)(TopWinners);
