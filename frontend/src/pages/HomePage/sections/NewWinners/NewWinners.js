import React, { Fragment, useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Container,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  CircularProgress,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import { Link } from "react-router-dom";
import Carousel, {
  slidesToShowPlugin,
  autoplayPlugin,
} from "@brainhubeu/react-carousel";

const NewWinners = ({ classes, items }) => {
  const [isFirstActive, setIsFirstActive] = useState(true);
  const [isSecondActive, setIsSecondActive] = useState(false);

  const [newWinners, setNewWinners] = useState([]);
  const { winners, loading } = items;
  useEffect(() => {
    setNewWinners(winners);
  }, []);
  const filteredWinners = winners.filter((item) => {
    console.log("ITEMS", item);
    return item.competition.toLowerCase() === "машины";
  });

  const handleFirstClick = () => {
    console.log("FIRST CLICK");
    setIsFirstActive(true);
    setIsSecondActive(false);
    setNewWinners(winners);
  };

  console.log("FILWINNERS", filteredWinners);

  const handleSecondClick = () => {
    console.log("SECOND CLICK");
    setIsFirstActive(false);
    setIsSecondActive(true);
    setNewWinners(filteredWinners);
  };

  return (
    <Fragment>
      {newWinners[0] && (
        <Grid
          container
          style={{
            backgroundColor: "#1c1b21",
            borderTop: "2px solid #1c1b21",
            position: "relative",
            padding: "0 0 110px 0",
          }}
        >
          <Grid item xs={3} style={{ position: "relative" }}>
            <Typography className={classes.second_slider_header}>
              Последние победители
            </Typography>
          </Grid>
          {loading ? (
            <Grid
              item
              xs={12}
              style={{
                padding: "48px 0",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <CircularProgress color="secondary" size={100} />
            </Grid>
          ) : (
            <Fragment>
              <Grid item xs={12}>
                <Container>
                  <Grid item xs={2} className={classes.switcher_container}>
                    <div onClick={handleFirstClick}>
                      <div
                        className={`${classes.switcher_link} ${
                          isFirstActive && classes.link_active
                        }`}
                      >
                        ВСЕ
                      </div>
                    </div>
                    <div onClick={handleSecondClick}>
                      <div
                        className={`${classes.switcher_link} ${
                          isSecondActive && classes.link_active
                        }`}
                      >
                        АВТОМОБИЛИ
                      </div>
                    </div>
                  </Grid>
                  <Grid item xs={12} style={{ margin: "20px 0" }}>
                    <Carousel
                      plugins={[
                        "infinite",
                        {
                          resolve: autoplayPlugin,
                          options: {
                            interval: 2000,
                          },
                        },
                        {
                          resolve: slidesToShowPlugin,
                          options: {
                            numberOfSlides: 4,
                          },
                        },
                      ]}
                      animationSpeed={1000}
                    >
                      {newWinners[0] &&
                        newWinners.map((item) => {
                          return (
                            <Card
                              className={classes.second_card_root}
                              key={item.id}
                            >
                              <CardActionArea>
                                <CardMedia
                                  className={classes.card_media}
                                  image={`${item.photo}`}
                                />
                                <CardContent>
                                  <Typography
                                    gutterBottom
                                    variant="h5"
                                    component="h2"
                                  >
                                    {item.event}
                                  </Typography>
                                  <Typography
                                    variant="body2"
                                    color="textSecondary"
                                    component="p"
                                  >
                                    {item.date}
                                  </Typography>
                                </CardContent>
                              </CardActionArea>
                            </Card>
                          );
                        })}
                    </Carousel>
                  </Grid>
                </Container>
              </Grid>
              <Link to={"/competition-winners"} className={"link-to"}>
                Все победители
              </Link>
            </Fragment>
          )}
        </Grid>
      )}
    </Fragment>
  );
};

export default withStyles(styles)(NewWinners);
