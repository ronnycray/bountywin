import React, { Fragment } from "react";
import { Grid, Container, Box, CircularProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import winnersImg from "../../../../constants/winners.png";
import raisedImg from "../../../../constants/givenaway.png";
import cashBackImg from "../../../../constants/cback.png";
import subImg from "../../../../constants/followers.png";

const Statistics = ({ classes, statistics }) => {
  const { statistic, loading } = statistics;
  return (
    <Fragment>
      {loading ? (
        <Grid
          item
          xs={12}
          style={{
            borderTop: "2px solid #55a187",
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Grid
          item
          xs={12}
          style={{ borderTop: "2px solid #55a187", paddingTop: "48px" }}
        >
          <Container>
            <Grid container>
              <Grid item xs={3} className={classes.statistic}>
                <Box className={classes.img_box}>
                  <img
                    style={{ width: "100%" }}
                    src={winnersImg}
                    alt={winnersImg}
                  />
                </Box>
                <Box
                  className={classes.statistic_card}
                  style={{ marginLeft: "10px" }}
                >
                  <span className={classes.statistic_header}>
                    {statistic.winners}
                  </span>
                  <span className={classes.statistic_body}>
                    Победителей за все время
                  </span>
                </Box>
              </Grid>
              <Grid
                item
                xs={3}
                className={classes.statistic}
                style={{ marginTop: "5px" }}
              >
                <Box className={classes.img_box} style={{ marginTop: "17px" }}>
                  <img
                    style={{ width: "90%" }}
                    src={raisedImg}
                    alt={raisedImg}
                  />
                </Box>
                <Box className={classes.statistic_card}>
                  <span className={classes.statistic_header}>
                    {statistic.prizes}
                  </span>
                  <span className={classes.statistic_body}>
                    Разыграно призов на сумму
                  </span>
                </Box>
              </Grid>
              <Grid item xs={3} className={classes.statistic}>
                <Box
                  className={classes.img_box}
                  style={{ marginRight: "14px", marginTop: "23px" }}
                >
                  <img
                    style={{ width: "90%" }}
                    src={cashBackImg}
                    alt={cashBackImg}
                  />
                </Box>
                <Box className={classes.statistic_card}>
                  <span className={classes.statistic_header}>
                    {statistic.charity}
                  </span>
                  <span className={classes.statistic_body}>
                    Выплачено CashBack'а
                  </span>
                </Box>
              </Grid>
              <Grid item xs={3} className={classes.statistic}>
                <Box
                  className={classes.img_box}
                  style={{ marginRight: "14px", marginTop: "23px" }}
                >
                  <img style={{ width: "90%" }} src={subImg} alt={subImg} />
                </Box>
                <Box className={classes.statistic_card}>
                  <span className={classes.statistic_header}>
                    {statistic.followers}
                  </span>
                  <span className={classes.statistic_body}>
                    Подписчиков в Соц. Сетях
                  </span>
                </Box>
              </Grid>
            </Grid>
          </Container>
        </Grid>
      )}
    </Fragment>
  );
};

export default withStyles(styles)(Statistics);
