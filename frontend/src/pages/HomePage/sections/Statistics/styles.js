export const styles = (theme) => ({
  img_box: {
    height: "80px",
    width: "80px",
  },
  statistic: {
    display: "flex",
    alignItems: "center",
    marginBottom: "48px",
  },
  statistic_card: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
  },
  statistic_header: {
    color: "white",
    fontSize: "45px",
    marginBottom: "-10px",
  },
  statistic_body: {
    fontSize: "16px",
    color: "#55a187",
  },
});
