export const styles = (theme) => ({
  link: {
    color: "#55A187",
    textDecoration: "none",
    backgroundColor: "transparent",
    height: "100%",
    width: "100%",
  },
  del: {
    textDecoration: "line-through",
    opacity: 0.8,
    marginLeft: "7px",
    float: "right",
    fontSize: "17px",
  },
  ins: {
    textDecoration: "none",
  },
});
