import React, { Fragment, useState } from "react";
import { Grid, SvgIcon, CircularProgress } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Carousel, {
  Dots,
  slidesToShowPlugin,
  autoplayPlugin,
  infinitePlugin,
} from "@brainhubeu/react-carousel";
import { styles } from "./styles";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import { Link } from "react-router-dom";

const MainSlider = ({ classes, items }) => {
  const { last_events, last_loading } = items;
  const [value, setValue] = useState(0);
  const onChange = (value) => {
    setValue(value);
  };
  const prevClick = () => {
    setValue(value * 1 - 1);
  };
  const nextClick = () => {
    setValue(value * 1 + 1);
  };
  return (
    <Fragment>
      {last_loading ? (
        <Grid
          item
          xs={12}
          style={{
            borderTop: "2px solid #55a187",
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Grid item xs={12}>
          {last_events[0] && (
            <div className={"wrapper"}>
              <div className={"hero-slider-wrapper"}>
                <Carousel
                  plugins={[
                    "infinite",
                    {
                      resolve: autoplayPlugin,
                      options: {
                        interval: 3000,
                      },
                    },
                  ]}
                  animationSpeed={500}
                  value={value}
                  onChange={(value) => onChange(value)}
                >
                  {last_events[0] &&
                    last_events.map((item) => (
                      <Link
                        to={`/competition/${item.id}`}
                        className={classes.link}
                        key={item.id}
                      >
                        <div className={"hero-slide-info"}>
                          <div
                            className={"hero-slide-img"}
                            style={{
                              background: `url(${item.photo})`,
                            }}
                          />
                          <div className={"hero-info"}>
                            <p className={"hero-slide-tag"}>Шанс выиграть</p>
                            <h3 className={"hero-slide-product-title"}>
                              {item.name}
                            </h3>
                            <p className={"hero-slide-price"}>
                              Цена Билета:{" "}
                              {item.now_price ? (
                                <Fragment>
                                  <del className={classes.del}>
                                    <span>
                                      {item.price}
                                      <span>₽</span>
                                    </span>
                                  </del>
                                  <ins className={classes.ins}>
                                    <span>
                                      {item.now_price}
                                      <span>₽</span>
                                    </span>
                                  </ins>
                                </Fragment>
                              ) : (
                                <Fragment>
                                  <span>
                                    {item.price}
                                    <span>₽</span>
                                  </span>
                                </Fragment>
                              )}
                            </p>
                            <div className={"hero-slide-btn"}>
                              <span className={"hero-slide-btn-link"}>
                                Показать Розыгрыш
                                <ArrowForwardIcon />
                              </span>
                            </div>
                          </div>
                        </div>
                        <div
                          className={"product-stats hero-slide-product-stats"}
                        >
                          <div className={"product-stat stat-bhp"}>
                            <div className={"stat-icon stat-bhp"} />
                            <div className={"stat-inner"}>
                              <span className={"stat-info"}>л.с.</span>
                              <span className={"stat-value"}>{item.bhp}</span>
                            </div>
                          </div>
                          <div className={"product-stat stat-0-60"}>
                            <div className={"stat-icon stat-0-60"} />
                            <div className={"stat-inner"}>
                              <span className={"stat-info"}>0-100</span>
                              <span className={"stat-value"}>{item.speed}</span>
                            </div>
                          </div>
                          <div className={"product-stat stat-miles"}>
                            <div className={"stat-icon stat-miles"} />
                            <div className={"stat-inner"}>
                              <span className={"stat-info"}>км.</span>
                              <span className={"stat-value"}>{item.miles}</span>
                            </div>
                          </div>
                          <div className={"product-stat stat-year"}>
                            <div className={"stat-icon stat-year"} />
                            <div className={"stat-inner"}>
                              <span className={"stat-info"}>год</span>
                              <span className={"stat-value"}>{item.year}</span>
                            </div>
                          </div>
                        </div>
                      </Link>
                    ))}
                </Carousel>

                <div className={"hero-slider-corner"}>
                  <h1>
                    Может быть <br /> ты следующий{" "}
                    <span style={{ color: "#55A187" }}>победитель?</span>
                  </h1>
                </div>
                <div className={"hero-slider-controls"}>
                  <div>
                    <Dots
                      value={value}
                      onChange={(value) => onChange(value)}
                      number={last_events.length}
                    />
                  </div>
                  <div className={"hero-slider-arrows"}>
                    <button
                      className={"slick-prev slick-arrow"}
                      style={{ display: "block" }}
                      onClick={() => prevClick()}
                    >
                      <SvgIcon style={{ color: "white", fontSize: "14px" }}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="3 5 32 32"
                          width="32"
                          height="32"
                        >
                          <path
                            fill="#fff"
                            d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                          />
                        </svg>
                      </SvgIcon>
                    </button>
                    <button
                      className={"slick-next slick-arrow"}
                      style={{ display: "block" }}
                      onClick={() => nextClick()}
                    >
                      <SvgIcon style={{ color: "white", fontSize: "14px" }}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="3 5 32 32"
                          width="32"
                          height="32"
                        >
                          <path
                            fill="#fff"
                            d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                          />
                        </svg>
                      </SvgIcon>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </Grid>
      )}
    </Fragment>
  );
};

export default withStyles(styles)(MainSlider);
