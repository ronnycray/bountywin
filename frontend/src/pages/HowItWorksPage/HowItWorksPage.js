import React from "react";
import { Grid, Container, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";

const HowItWorksPage = ({ classes }) => {
  return (
    <Grid container className={"body-page-how-it-works"}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>КАК ЭТО РАБОТАЕТ</Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.body}>
        <Grid item xs={12}>
          <Container maxWidth={"xl"} className={classes.container}>
            <Grid container>
              <Grid item xs={6}>
                <Typography className={classes.second_header}>
                  <span className={classes.span}>ВЫИГРАЙ</span> МАШИНУ СВОЕЙ
                  МЕЧТЫ ЗА НЕБОЛЬШУЮ ЧАСТЬ ЕЕ СТОИМОСТИ
                </Typography>
                <Typography className={classes.third_header}>
                  Вот как это работает:
                </Typography>
                <ul className={classes.list}>
                  <li className={classes.list_item1}>
                    <span className={classes.list_number_span}>1</span>
                    <span className={classes.list_text_span}>
                      Выберите конкурс и желаемое количество билетов.
                    </span>
                  </li>
                  <li className={classes.list_item2}>
                    <span className={classes.list_number_span}>2</span>
                    <span className={classes.list_text_span}>
                      Ответьте на уточняющий вопрос (обязательно ответьте
                      правильно!) и завершите оплату.
                    </span>
                  </li>
                  <li className={classes.list_item3}>
                    <span className={classes.list_number_span}>3</span>
                    <span className={classes.list_text_span}>
                      Билеты на все правильные ответы участвуют в розыгрыше,
                      который транслируется в прямом эфире на Facebook с
                      использованием генератора случайных чисел Google.
                    </span>
                  </li>
                  <li className={classes.list_item4}>
                    <span className={classes.list_number_span}>4</span>
                    <span className={classes.list_text_span}>
                      Если вы выиграете, мы можем попытаться связаться с вами в
                      прямом эфире на нашем сайте и Facebook, так что будьте
                      готовы!
                    </span>
                  </li>
                </ul>
              </Grid>
            </Grid>
          </Container>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(HowItWorksPage);
