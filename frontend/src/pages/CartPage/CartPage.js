import React, { Fragment, useState } from "react";
import { Grid, Typography, SvgIcon, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ReportProblemOutlinedIcon from "@material-ui/icons/ReportProblemOutlined";
import { styles } from "./styles";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  updateCartItem,
  removeFromCart,
  addCoupon,
} from "../../actions/cartActions";
import CustomInput from "../../components/common/CustomInput";
import LoadingButton from "@material-ui/lab/LoadingButton";
import { Info } from "@material-ui/icons";

const CartPage = ({
  classes,
  cart: { items, coupon, loading },
  updateCartItem,
  removeFromCart,
  addCoupon,
  profile: { info },
  auth: { isAuthenticate },
  history,
}) => {
  const { discount_percent, discount_amount } = coupon;
  const [couponText, setCouponText] = useState("");
  const handleClick = () => {
    console.log("ADD");
    addCoupon(couponText);
  };
  console.log("IS AUTH", isAuthenticate);
  const createOrderClick = () => {};
  const authClick = () => {
    history.push("/login");
  };
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>КОРЗИНА</Typography>
        </Grid>
      </Grid>
      <Grid container style={{ margin: "0 52px", padding: "0 25px" }}>
        {!items[0] && (
          <Grid item xs={12}>
            <div
              style={{
                width: "100%",
                backgroundColor: "rgba(85, 161, 135, 0.9)",
                color: "white",
                padding: "20px",
                display: "flex",
                alignItems: "center",
                borderRadius: "7px",
              }}
            >
              <ReportProblemOutlinedIcon style={{ fontSize: "30px" }} />{" "}
              <span style={{ marginLeft: "10px", fontSize: "20px" }}>
                Ваша корзина пуста!
              </span>
            </div>
            <div className={classes.back_button}>
              <Link to={"/"} style={{ textDecoration: "none", color: "white" }}>
                Вернуться к каталогу
              </Link>
            </div>
          </Grid>
        )}
        {items[0] && (
          <Grid container style={{ justifyContent: "space-between" }}>
            <Grid item xs={8}>
              <Typography className={classes.title}>ТОВАРЫ</Typography>
              <Grid
                container
                style={{
                  backgroundColor: "black",
                  color: "white",
                  textAlign: "center",
                  boxShadow: "0 10px 15px rgb(27 28 32 / 10%)",
                }}
              >
                <Grid container>
                  <Grid item xs={8} className={classes.table_head}>
                    Конкурс
                  </Grid>
                  <Grid item xs={2} className={classes.table_head}>
                    Билеты
                  </Grid>
                  <Grid item xs={2} className={classes.table_head}>
                    Сумма
                  </Grid>
                </Grid>
                {items.map((item) => {
                  const addOne = () => {
                    item.quantity = item.quantity * 1 + 1;
                    updateCartItem(item);
                  };
                  const removeOne = () => {
                    item.quantity = item.quantity * 1 - 1;
                    updateCartItem(item);
                  };
                  return (
                    <Grid
                      container
                      style={{
                        backgroundColor: "white",
                        borderBottom: "1px solid #c5c5c5",
                        color: "black",
                        textAlign: "left !imporant",
                        alignItems: "center",
                      }}
                    >
                      <Grid
                        item
                        xs={8}
                        style={{ display: "flex", flexDirection: "row" }}
                      >
                        <Grid container>
                          <Grid
                            item
                            xs={5}
                            style={{
                              width: "40%",
                              padding: "9px",
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <DeleteIcon
                              className={classes.delete_button}
                              onClick={() => removeFromCart(item)}
                            />
                            <img
                              style={{ width: "90%" }}
                              src={`https://bountywin.com/${item.photo[0]}`}
                            />
                          </Grid>
                          <Grid
                            item
                            xs={7}
                            style={{
                              padding: "24px 20px",
                              display: "flex",
                              flexDirection: "column",
                            }}
                          >
                            <Link
                              to={`/competition/${item.id}`}
                              className={classes.table_link}
                            >
                              {item.name}
                            </Link>
                            <Typography
                              style={{
                                display: "inline-block",
                                textAlign: "left",
                                marginTop: "8px",
                                fontSize: "18px",
                              }}
                            >
                              Вопрос: <strong>{item.question}</strong>
                            </Typography>

                            <Typography
                              style={{
                                display: "inline-block",
                                textAlign: "left",
                                marginTop: "8px",
                                fontSize: "18px",
                              }}
                            >
                              Ваш ответ: <strong>{item.answer.text}</strong>
                            </Typography>

                            <Typography
                              style={{
                                display: "inline-block",
                                textAlign: "left",
                                marginTop: "8px",
                                fontSize: "18px",
                              }}
                            >
                              <strong>{item.price}₽</strong>
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid
                        item
                        xs={2}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <button
                          className={"competition_arrows prev_arrow"}
                          onClick={() => removeOne()}
                          disabled={item.quantity === 0}
                        >
                          <SvgIcon
                            style={{
                              color: "white",
                              fontSize: "12px",
                            }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="3 5 32 32"
                              width="32"
                              height="32"
                            >
                              <path
                                fill="#000"
                                d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                              />
                            </svg>
                          </SvgIcon>
                        </button>
                        <span style={{ fontSize: "16px", margin: "0 10px" }}>
                          {item.quantity}
                        </span>
                        <button
                          className={"competition_arrows"}
                          onClick={() => addOne()}
                          disabled={item.quantity >= item.limit_person}
                        >
                          <SvgIcon
                            style={{
                              color: "white",
                              fontSize: "12px",
                            }}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="3 5 32 32"
                              width="32"
                              height="32"
                            >
                              <path
                                fill="#000"
                                d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                              />
                            </svg>
                          </SvgIcon>
                        </button>
                      </Grid>
                      <Grid item xs={2}>
                        <span style={{ fontSize: "20px" }}>
                          {item.price * item.quantity}₽
                        </span>
                      </Grid>
                    </Grid>
                  );
                })}
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <Typography className={classes.title}>ИТОГ</Typography>
              <Grid
                container
                style={{
                  flexDirection: "column",
                  boxShadow: "0 10px 15px rgb(27 28 32 / 10%)",
                  fontSize: "16px",
                }}
              >
                <Grid
                  item
                  xs={12}
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    paddingTop: "10px",
                    borderBottom: "1px solid #c5c5c5",
                  }}
                >
                  <Grid
                    item
                    xs={6}
                    style={{
                      padding: "12px",
                    }}
                  >
                    <strong>Сумма</strong>
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      padding: "12px",
                    }}
                  >
                    {items.reduce(
                      (acc, item) => acc + item.quantity * item.price,
                      0
                    )}
                    ₽
                  </Grid>
                </Grid>
                {discount_percent > 0 ? (
                  <Fragment>
                    <Grid
                      item
                      xs={12}
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        paddingTop: "10px",
                        borderBottom: "1px solid #c5c5c5",
                      }}
                    >
                      <Grid
                        item
                        xs={6}
                        style={{
                          padding: "12px",
                        }}
                      >
                        <strong>Скидка</strong>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        style={{
                          padding: "12px",
                        }}
                      >
                        {discount_percent}%
                      </Grid>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Grid
                        item
                        xs={6}
                        style={{
                          padding: "12px",
                        }}
                      >
                        <strong>Итоговая сумма</strong>
                      </Grid>
                      <Grid
                        item
                        xs={6}
                        style={{
                          padding: "12px",
                        }}
                      >
                        {items
                          .reduce(
                            (acc, item) => acc + item.quantity * item.price,
                            0
                          )
                          .toFixed(2) *
                          (1 - discount_percent / 100)}
                        ₽
                      </Grid>
                    </Grid>
                  </Fragment>
                ) : (
                  discount_amount > 0 && (
                    <Fragment>
                      <Grid
                        item
                        xs={12}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          paddingTop: "10px",
                          borderBottom: "1px solid #c5c5c5",
                        }}
                      >
                        <Grid
                          item
                          xs={6}
                          style={{
                            padding: "12px",
                          }}
                        >
                          <strong>Скидка</strong>
                        </Grid>
                        <Grid
                          item
                          xs={6}
                          style={{
                            padding: "12px",
                          }}
                        >
                          {discount_amount}₽
                        </Grid>
                      </Grid>
                      <Grid
                        item
                        xs={12}
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                        }}
                      >
                        <Grid
                          item
                          xs={6}
                          style={{
                            padding: "12px",
                          }}
                        >
                          <strong>Итоговая сумма</strong>
                        </Grid>
                        <Grid
                          item
                          xs={6}
                          style={{
                            padding: "12px",
                          }}
                        >
                          {items
                            .reduce(
                              (acc, item) => acc + item.quantity * item.price,
                              0
                            )
                            .toFixed(2) - discount_amount}
                          ₽
                        </Grid>
                      </Grid>
                    </Fragment>
                  )
                )}
              </Grid>
              <Grid container style={{ paddingTop: "10px" }}>
                <CustomInput
                  label={"Введите купон"}
                  labelColor="black"
                  value={couponText}
                  onChange={(e) => setCouponText(e.target.value)}
                  disabled={loading}
                />
                <Grid
                  item
                  xs={12}
                  style={{ display: "flex", justifyContent: "flex-end" }}
                >
                  <LoadingButton
                    className={classes.button}
                    pending={loading}
                    onClick={handleClick}
                  >
                    Применить
                  </LoadingButton>
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={12}>
                  {isAuthenticate ? (
                    <LoadingButton
                      className={classes.button}
                      pending={loading && !info.first_name}
                      style={{ width: "100%" }}
                      onClick={createOrderClick}
                    >
                      Оформить заказ
                    </LoadingButton>
                  ) : (
                    <LoadingButton
                      className={classes.button}
                      pending={loading}
                      style={{ width: "100%" }}
                      onClick={authClick}
                    >
                      Авторизоваться
                    </LoadingButton>
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  profile: state.profile,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  updateCartItem,
  removeFromCart,
  addCoupon,
})(withStyles(styles)(CartPage));
