import React, { Fragment, useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Container,
  Card,
  CardMedia,
  CardContent,
  CardActionArea,
  Dialog,
  useMediaQuery,
  DialogActions,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  Slide,
  InputAdornment,
  CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { useTheme } from "@material-ui/core/styles";
import { styles } from "./styles";
import { paginate } from "../../utils/paginate";
import SearchIcon from "@material-ui/icons/Search";
import CustomInput from "../../components/common/CustomInput/CustomInput";
import Pagination from "../../components/common/Pagination/Pagination";
import { getWinners } from "../../actions/winnersActions";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" timeout={10000} ref={ref} {...props} />;
});

// const Data = [
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
//   {
//     id: 1,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "02/02/2021",
//   },
//   {
//     id: 2,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS4 Saloon & £1500",
//     date: "12/02/2021",
//   },
//   {
//     id: 3,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS5 Saloon & £1500",
//     date: "22/02/2021",
//   },
//   {
//     id: 4,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi S3 Saloon & £1500",
//     date: "05/03/2021",
//   },
//   {
//     id: 5,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi RS3 Saloon & £1500",
//     date: "15/03/2021",
//   },
//   {
//     id: 6,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R3 Saloon & £1500",
//     date: "25/03/2021",
//   },
//   {
//     id: 7,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R7 Saloon & £1500",
//     date: "04/04/2021",
//   },
//   {
//     id: 8,
//     photo:
//       "https://dreamcargiveaways.co.uk/wp-content/uploads/2021/02/Lewis-Findell-588x424.jpg",
//     name: "Audi R13 Saloon & £1500",
//     date: "14/04/2021",
//   },
// ];

const WinnersPage = ({
  classes,
  winners: { loading, winners },
  getWinners,
}) => {
  const [open, setOpen] = useState(false);
  const [card, setCard] = useState({});
  const [isFirstActive, setIsFirstActive] = useState(true);
  const [isSecondActive, setIsSecondActive] = useState(false);
  const [filter, setFilter] = useState("");
  const [newWinners, setNewWinners] = useState([]);

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const handleClickOpen = (item) => {
    setOpen(true);
    setCard(item);
  };

  useEffect(() => {
    getWinners();
    setNewWinners(winners);
  }, []);
  console.log("CARD", card);
  const handleClose = () => {
    setOpen(false);
    setCard({});
  };
  const handleChange = (e) => {
    setFilter(e.target.value);
  };

  const filteredWinners = winners.filter((item) => {
    console.log("ITEMS", item);
    return item.competition.toLowerCase() === "машины";
  });

  const handleFirstClick = () => {
    console.log("FIRST CLICK");
    setIsFirstActive(true);
    setIsSecondActive(false);
    setNewWinners(winners);
  };

  console.log("FILWINNERS", filteredWinners);

  const handleSecondClick = () => {
    console.log("SECOND CLICK");
    setIsFirstActive(false);
    setIsSecondActive(true);
    setNewWinners(filteredWinners);
  };

  const lowercasedFilter = filter.toLowerCase();
  const filteredData =
    newWinners.length > 0 &&
    newWinners.filter((item) =>
      item.event.toLowerCase().includes(lowercasedFilter)
    );

  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 12;

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const getPageData = () => {
    if (filteredData) {
      const paginationData = paginate(filteredData, currentPage, pageSize);
      return { totalCount: filteredData.length, data: paginationData };
    } else {
      const paginationData = paginate(newWinners, currentPage, pageSize);
      return { totalCount: newWinners.length, data: paginationData };
    }
  };

  const { totalCount, data } = getPageData();

  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>
            ПОБЕДИТЕЛИ РОЗЫГРЫШЕЙ
          </Typography>
        </Grid>
      </Grid>
      <Container maxWidth="xl" style={{ padding: "0 80px" }}>
        <Grid container>
          <Grid item xs={2} className={classes.switcher_container}>
            <div onClick={handleFirstClick}>
              <Link
                to="#"
                className={`${classes.switcher_link} ${
                  isFirstActive && classes.link_active
                }`}
              >
                ВСЕ
              </Link>
            </div>
            <div onClick={handleSecondClick}>
              <Link
                to="#"
                className={`${classes.switcher_link} ${
                  isSecondActive && classes.link_active
                }`}
              >
                АВТОМОБИЛИ
              </Link>
            </div>
          </Grid>
          <Grid
            item
            xs={12}
            style={{
              marginBottom: "10px",
              paddingBottom: "6px",
              borderBottom: "2px solid gainsboro",
            }}
          >
            <CustomInput
              placeholder="Найти..."
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
              onChange={(e) => handleChange(e)}
            />
          </Grid>
          <Grid item xs={12}>
            {loading ? (
              <Grid
                item
                xs={12}
                style={{
                  padding: "48px 0",
                  display: "flex",
                  justifyContent: "center",
                  marginTop: "50px",
                }}
              >
                <CircularProgress color="secondary" size={100} />
              </Grid>
            ) : (
              <Grid container style={{ marginTop: "10px" }}>
                {data[0] &&
                  data.map((item) => {
                    return (
                      <Fragment key={item.id}>
                        <Grid
                          item
                          xs={3}
                          style={{ padding: "5px 15px", marginBottom: "10px" }}
                        >
                          <Card className={classes.card_root}>
                            <CardActionArea
                              onClick={() => handleClickOpen(item)}
                            >
                              <CardMedia
                                className={classes.media}
                                image={`${item.photo}`}
                                title="Contemplative Reptile"
                              />
                              <CardContent>
                                <Typography
                                  gutterBottom
                                  variant="h5"
                                  component="h2"
                                >
                                  {item.event}
                                </Typography>
                                <Typography
                                  variant="body2"
                                  color="textSecondary"
                                  component="p"
                                >
                                  {item.date}
                                </Typography>
                              </CardContent>
                            </CardActionArea>
                          </Card>
                        </Grid>
                        <Dialog
                          fullScreen={fullScreen}
                          TransitionComponent={Transition}
                          open={open}
                          onClose={() => handleClose()}
                        >
                          <DialogTitle
                            id="responsive-dialog-title"
                            style={{
                              textTransform: "uppercase",
                              fontSize: "28px",
                              fontWeight: "700 !important",
                            }}
                          >
                            {card.event}
                          </DialogTitle>
                          <DialogContent>
                            <img
                              src={item.photo}
                              alt={item.photo}
                              style={{ width: "466px", margin: "15px 0" }}
                            />
                            <DialogContentText>{card.date}</DialogContentText>
                          </DialogContent>
                          <DialogActions>
                            <Button
                              onClick={() => handleClose()}
                              color="primary"
                              autoFocus
                            >
                              Закрыть
                            </Button>
                          </DialogActions>
                        </Dialog>
                      </Fragment>
                    );
                  })}
                <Grid item xs={12}>
                  <Pagination
                    itemsCount={totalCount}
                    pageSize={pageSize}
                    currentPage={currentPage}
                    onPageChange={handlePageChange}
                  />
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Container>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  winners: state.winners,
});

export default connect(mapStateToProps, { getWinners })(
  withStyles(styles)(WinnersPage)
);
