import React, { useEffect } from "react";
import {
  Grid,
  Typography,
  CircularProgress,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import { connect } from "react-redux";
import { getEndedEvents } from "../../actions/eventsActions";

const PastCompetitionsPage = ({
  classes,
  events: { ended_events, all_loading },
  getEndedEvents,
}) => {
  useEffect(() => {
    getEndedEvents();
  }, []);
  return (
    <Grid container>
      <Grid
        item
        xs={12}
        style={{
          borderTop: "2px solid #55a187",
          position: "relative",
          paddingBottom: "110px",
          backgroundColor: "#1c1b21",
        }}
      >
        <Grid container>
          <Grid item>
            <Typography className={classes.second_slider_header}>
              Завершившиеся розыгрыши
            </Typography>
          </Grid>
        </Grid>
        {all_loading ? (
          <Grid
            item
            xs={12}
            style={{
              padding: "48px 0",
              display: "flex",
              justifyContent: "center",
              marginTop: "70px",
            }}
          >
            <CircularProgress color="secondary" size={100} />
          </Grid>
        ) : (
          <Grid item xs={12}>
            <Grid container style={{ marginTop: "10px", padding: "0 40px" }}>
              {ended_events[0] &&
                ended_events.map((item) => {
                  return (
                    <Grid
                      xs={4}
                      style={{ padding: "5px 15px", marginBottom: "10px" }}
                    >
                      <Card className={classes.card_root}>
                        <CardActionArea>
                          <CardContent
                            style={{ padding: "0", position: "relative" }}
                          >
                            <CardMedia
                              className={classes.media}
                              image={`${item.images[0]}`}
                              title="Contemplative Reptile"
                              style={{
                                aspectRatio: "auto 588 / 424",
                                height: "20vw",
                              }}
                            />
                            <span className={classes.span}>
                              <span className={classes.sold_span_body}>
                                ЗАКОНЧЕН!
                              </span>
                            </span>
                          </CardContent>
                          <CardContent>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="h2"
                              className={classes.card_name}
                            >
                              {item.name}
                            </Typography>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    </Grid>
                  );
                })}
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  events: state.events,
});

export default connect(mapStateToProps, { getEndedEvents })(
  withStyles(styles)(PastCompetitionsPage)
);
