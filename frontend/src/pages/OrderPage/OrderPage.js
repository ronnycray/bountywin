import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { styles } from "./styles";

const OrderPage = ({ classes, cart: { items } }) => {
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>ВАШ ЗАКАЗ</Typography>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={8} className={classes.table_head}>
          Конкурс
        </Grid>
        <Grid item xs={2} className={classes.table_head}>
          Билеты
        </Grid>
        <Grid item xs={2} className={classes.table_head}>
          Сумма
        </Grid>
      </Grid>
      {items.map((item) => {
        return (
          <Grid
            container
            style={{
              backgroundColor: "white",
              borderBottom: "1px solid #c5c5c5",
              color: "black",
              textAlign: "left !imporant",
              alignItems: "center",
            }}
          >
            <Grid item xs={8} style={{ display: "flex", flexDirection: "row" }}>
              <Grid container>
                <Grid
                  item
                  xs={7}
                  style={{
                    padding: "24px 20px",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {item.name}
                  <Typography
                    style={{
                      display: "inline-block",
                      textAlign: "left",
                      marginTop: "8px",
                      fontSize: "18px",
                    }}
                  >
                    Вопрос: <strong>{item.question}</strong>
                  </Typography>

                  <Typography
                    style={{
                      display: "inline-block",
                      textAlign: "left",
                      marginTop: "8px",
                      fontSize: "18px",
                    }}
                  >
                    Ваш ответ: <strong>{item.answer.text}</strong>
                  </Typography>

                  <Typography
                    style={{
                      display: "inline-block",
                      textAlign: "left",
                      marginTop: "8px",
                      fontSize: "18px",
                    }}
                  >
                    <strong>{item.price}₽</strong>
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid
              item
              xs={2}
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <span style={{ fontSize: "16px", margin: "0 10px" }}>
                {item.quantity}
              </span>
            </Grid>
            <Grid item xs={2}>
              <span style={{ fontSize: "20px" }}>
                {item.price * item.quantity}₽
              </span>
            </Grid>
          </Grid>
        );
      })}
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  profile: state.profile,
  auth: state.auth,
});

export default connect(mapStateToProps)(withStyles(styles)(OrderPage));
