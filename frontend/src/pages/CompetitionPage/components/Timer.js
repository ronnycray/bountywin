import React, { useState, useEffect } from "react";
import { Box, CircularProgress, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import moment from "moment";

function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="determinate" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
        flexDirection="column"
      >
        <Typography
          variant="caption"
          component="div"
          color="textSecondary"
          style={{ fontSize: "25px", fontWeight: "700", color: "black" }}
        >{`${props.time}`}</Typography>
        <div style={{ fontSize: "10px" }}>{props.name_span}</div>
      </Box>
    </Box>
  );
}

const Timer = ({ start, end, classes }) => {
  const calculateTimeLeft = () => {
    const time = moment(moment(end).diff(moment(start))).format(
      "DD, hh, mm, ss"
    );
    return time;
  };

  const [timer, setTimer] = useState(calculateTimeLeft());
  const slicedTimer = timer.split(", ");
  const normaliseDays = (value) => 100 - ((value - 0) * 100) / (100 - 0);
  const normaliseHours = (value) => 100 - ((value - 0) * 100) / (24 - 0);
  const normaliseMins = (value) => 100 - ((value - 0) * 100) / (60 - 0);
  const normaliseSecs = (value) => 100 - ((value - 0) * 100) / (60 - 0);
  useEffect(() => {
    const timer = setTimeout(() => {
      setTimer(calculateTimeLeft());
    }, 1000);

    return () => clearTimeout(timer);
  });
  return (
    <div style={{ margin: "15px 0" }}>
      <CircularProgressWithLabel
        variant={"determinate"}
        value={normaliseDays(slicedTimer[0])}
        time={slicedTimer[0]}
        className={classes.timer_progress}
        style={{ color: "red" }}
        name_span="ДНЕЙ"
      />
      <CircularProgressWithLabel
        variant={"determinate"}
        value={normaliseHours(slicedTimer[1])}
        time={slicedTimer[1]}
        className={classes.timer_progress}
        style={{ color: "blue" }}
        name_span="ЧАСОВ"
      />
      <CircularProgressWithLabel
        variant={"determinate"}
        value={normaliseMins(slicedTimer[2])}
        time={slicedTimer[2]}
        className={classes.timer_progress}
        style={{ color: "purple" }}
        name_span="МИН"
      />
      <CircularProgressWithLabel
        variant={"determinate"}
        value={normaliseSecs(slicedTimer[3])}
        time={slicedTimer[3]}
        className={classes.timer_progress}
        style={{ color: "yellow" }}
        name_span="СЕК"
      />
    </div>
  );
};

export default withStyles(styles)(Timer);
