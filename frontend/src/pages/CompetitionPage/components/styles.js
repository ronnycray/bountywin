export const styles = (theme) => ({
  timer_progress: {
    fontSize: "40px",
    width: "100px !important",
    height: "100px !important",
    margin: "0 10px",
  },
});
