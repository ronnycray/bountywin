import React, { Fragment, useEffect, useState } from "react";
import {
  Grid,
  CircularProgress,
  Typography,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  SvgIcon,
  LinearProgress,
  Box,
  InputBase,
  MenuItem,
  Select,
  FormControl,
  NativeSelect,
} from "@material-ui/core";
import TimerIcon from "@material-ui/icons/Timer";
import Carousel, {
  Dots,
  slidesToShowPlugin,
  autoplayPlugin,
  infinitePlugin,
} from "@brainhubeu/react-carousel";
import PersonOutlineIcon from "@material-ui/icons/PersonOutline";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { styles } from "./styles";
import { getEvent } from "../../actions/eventsActions";
import Timer from "./components/Timer";
import { addToCart } from "../../actions/cartActions";

function LinearProgressWithLabel(props) {
  return (
    <Box display="flex" flexDirection="column" style={{ position: "relative" }}>
      <Box style={{ marginLeft: "-3%", position: "relative", width: "99.9%" }}>
        <Box
          style={{ marginLeft: `${props.value}%` }}
          className={"progress-span"}
        >
          <Typography
            variant="body2"
            color="textSecondary"
            style={{
              display: "flex",
              flexDirection: "column",
              color: "#55A187",
              fontWeight: "600",
              marginTop: "2px",
            }}
          >
            {`${Math.round(props.value)}%`}
          </Typography>
        </Box>
      </Box>
      <Box width="100%" mr={1}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box style={{ position: "relative", width: "99.9%" }}>
        <Box>
          <span
            style={{
              width: "2px",
              left: `${props.span_value}%`,
              height: "10px",
              backgroundColor: "black",
              position: "absolute",
              top: "-10px",
            }}
          ></span>
          <p
            style={{
              left: `${props.span_value - 20}%`,
              height: "43px",
              position: "absolute",

              fontSize: "12px",
              display: "flex",
              alignItems: "center",
              fontWeight: "600",
            }}
          >
            <TrendingUpIcon style={{ fontSize: "15px" }} />
            Поторопитесь! Цена увеличится когда процент достигнет 75%!
          </p>
        </Box>
      </Box>
    </Box>
  );
}

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: "#55A187",
  },
}))(LinearProgressWithLabel);

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 16,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 20,
    display: "flex",
    alignItems: "center",
    fontWeight: "600",
    width: "100%",
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 16,
      borderColor: "#ced4da",
      border: "3px",
      boxShadow: "0 0 0 0.2rem #55A187",
    },
  },
}))(InputBase);

const CompetitionPage = ({
  match,
  classes,
  events: { event, loading, all_loading, all_events },
  history,
  getEvent,
  addToCart,
}) => {
  const [answer, setAnswer] = useState({});
  const [quantity, setQuantity] = useState(1);
  const [photo, setPhoto] = useState(event.photo);
  const [description, setDescritpion] = useState(event.description);
  useEffect(async () => {
    await getEvent(match.params.id);
    event.competition_info &&
      setDescritpion(event.competition_info.description);
  }, [match]);
  const handleClick = (item) => {
    history.push(`/competition/${item}`);
  };

  const normalise = (value) => ((value - 0) * 100) / (event.tickets - 0);
  const limit_arr = [];
  for (let i = 0; i < event.limit_person; i++) {
    limit_arr[i] = i + 1;
  }
  const photos =
    event.photo &&
    event.photo.map((item) => {
      return <img src={`${item}`} alt={item} style={{ width: "100%" }} />;
    });
  const thumbnails =
    event.photo &&
    event.photo.map((item) => {
      return <img src={`${item}`} alt={item} style={{ width: "90%" }} />;
    });
  const onChange = (answer) => {
    setAnswer(answer);
  };
  const handleChange = (event) => {
    setQuantity(event.target.value);
  };
  const handleChangeSelect = (answer) => {
    setAnswer(answer);
  };

  const [value, setValue] = useState(0);
  const onChangeCarousel = (value) => {
    setValue(value);
  };
  const prevClick = () => {
    setValue(value * 1 - 1);
  };
  const nextClick = () => {
    setValue(value * 1 + 1);
  };
  const [isFirstActive, setIsFirstActive] = useState(true);
  const [isSecondActive, setIsSecondActive] = useState(false);
  const handleFirstClick = () => {
    console.log("FIRST CLICK");
    setIsFirstActive(true);
    setIsSecondActive(false);
  };

  const handleSecondClick = () => {
    console.log("SECOND CLICK");
    setIsFirstActive(false);
    setIsSecondActive(true);
    setDescritpion(event.competition_info.description);
  };
  console.log("description", description);

  const addToCartClick = (e) => {
    e.preventDefault();
    console.log("EVENT", event);
    console.log("QUAN", quantity);
    console.log("ANSWER", answer);
    const item = {
      ...event,
      quantity: quantity,
      answer: answer,
    };
    console.log("FORM ITEM", item);
    addToCart(item);
  };

  return (
    <Grid container className={classes.wrapper}>
      {loading ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
            marginBottom: "88px",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Grid container className={classes.product_container}>
          <Grid container className={classes.product_info_container}>
            <Grid
              item
              xs={6}
              className={classes.product_photo}
              style={{ position: "relative" }}
            >
              {event.tickets * 1 !== 0 && event.now_price * 1 > 0 && (
                <span className={classes.span} style={{ right: "60px" }}>
                  <span className={classes.launch_span_body}>СКИДКА!</span>
                </span>
              )}
              <Carousel
                plugins={[
                  "infinite",
                  {
                    resolve: autoplayPlugin,
                    options: {
                      interval: 3000,
                    },
                  },
                ]}
                animationSpeed={500}
                value={value}
                slides={photos}
                onChange={(value) => onChangeCarousel(value)}
              />
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <button
                  className={"competition_arrows prev_arrow"}
                  onClick={() => prevClick()}
                >
                  <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="3 5 32 32"
                      width="32"
                      height="32"
                    >
                      <path
                        fill="#000"
                        d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                      />
                    </svg>
                  </SvgIcon>
                </button>
                <Dots
                  number={thumbnails && thumbnails.length}
                  thumbnails={thumbnails}
                  value={value}
                  onChange={(value) => onChangeCarousel(value)}
                  number={photos && photos.length}
                />
                <button
                  className={"competition_arrows"}
                  onClick={() => nextClick()}
                >
                  <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="3 5 32 32"
                      width="32"
                      height="32"
                    >
                      <path
                        fill="#000"
                        d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                      />
                    </svg>
                  </SvgIcon>
                </button>
              </div>
            </Grid>
            <Grid item xs={5} className={classes.product_info}>
              <Typography className={classes.info_title}>
                <span className={classes.title_span}>
                  Купите сейчас, чтобы получить шанс выиграть
                </span>
                <br />
                {event.name}
              </Typography>
              <div className={classes.info_body}>
                <div className={classes.info_timer}>
                  <Typography className={classes.timer_title}>
                    Розыгрыш закончится через:
                  </Typography>
                  <Timer
                    start={event.date_create_event}
                    end={event.date_end_event}
                  />
                </div>
                <div className={classes.info_tickets}>
                  <div className={classes.tickets_title}>
                    Конкурс будет закрыт досрочно, если все билеты будут проданы
                    до даты окончания.
                  </div>
                  <div className={classes.tickets_info}>
                    <div className={classes.tickets_info_count}>
                      <SvgIcon style={{ fontSize: "20px" }}>
                        <svg
                          version="1.1"
                          id="Capa_1"
                          xmlns="http://www.w3.org/2000/svg"
                          x="0px"
                          y="0px"
                          viewBox="0 0 512 512"
                        >
                          <g>
                            <g>
                              <path
                                d="M448.678,128.219l-10.607,10.608c-8.667,8.667-20.191,13.44-32.449,13.44c-12.258,0-23.78-4.773-32.448-13.44
			c-8.667-8.667-13.44-20.191-13.44-32.448s4.773-23.781,13.44-32.449l10.608-10.608L320.459,0L0,320.459l63.322,63.322
			l10.608-10.608c8.667-8.667,20.191-13.44,32.449-13.44c12.258,0,23.78,4.773,32.448,13.44c8.667,8.667,13.44,20.191,13.44,32.448
			s-4.773,23.781-13.44,32.449l-10.608,10.608L191.541,512L512,191.541L448.678,128.219z M169.61,447.636
			c8.237-12.343,12.662-26.839,12.662-42.015c0-20.272-7.894-39.33-22.229-53.664c-14.334-14.335-33.393-22.229-53.664-22.229
			c-15.176,0-29.672,4.425-42.015,12.662l-21.932-21.931L320.459,42.432l21.931,21.932c-8.237,12.343-12.662,26.839-12.662,42.015
			c0,20.272,7.894,39.33,22.229,53.664c14.334,14.335,33.393,22.229,53.664,22.229c15.176,0,29.672-4.425,42.015-12.662
			l21.932,21.931L191.541,469.568L169.61,447.636z"
                              />
                            </g>
                          </g>
                          <g>
                            <g>
                              <rect
                                x="284.001"
                                y="197.94"
                                transform="matrix(0.7071 -0.7071 0.7071 0.7071 -63.0395 273.8137)"
                                width="30.004"
                                height="30.124"
                              />
                            </g>
                          </g>
                          <g>
                            <g>
                              <rect
                                x="241.404"
                                y="155.325"
                                transform="matrix(0.7071 -0.7071 0.7071 0.7071 -45.3819 231.2119)"
                                width="30.004"
                                height="30.124"
                              />
                            </g>
                          </g>
                          <g>
                            <g>
                              <rect
                                x="326.607"
                                y="240.541"
                                transform="matrix(0.7071 -0.7071 0.7071 0.7071 -80.684 316.4184)"
                                width="30.004"
                                height="30.124"
                              />
                            </g>
                          </g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                          <g></g>
                        </svg>
                      </SvgIcon>
                      <strong style={{ margin: "0 6px" }}>
                        {event.tickets}
                      </strong>
                      билетов.
                    </div>
                    <div className={classes.tickets_info_limit}>
                      <PersonOutlineIcon
                        style={{ fontSize: "23px", marginRight: "6px" }}
                      />{" "}
                      Не более{" "}
                      <span className={classes.limit_span}>
                        {event.limit_person}
                      </span>{" "}
                      одному человеку.
                    </div>
                  </div>
                  <div style={{ fontSize: "16px", marginBottom: "24px" }}>
                    Вы должны корректно ответить на вопрос когда будете заносить
                    товар в корзину.
                  </div>
                  <div className={classes.info_price}>
                    {event.now_price ? (
                      <span className={classes.price}>
                        <del>
                          <span
                            className={classes.del}
                            style={{
                              textAlign: "left",
                              textDecoration: "none",
                              top: "35%",
                              fontSize: "20px",
                              fontWeight: "600",
                              letterSpacing: "-0.5px",
                            }}
                          >
                            <div
                              style={{
                                textTransform: "uppercase",
                                whiteSpace: "nowrap",
                              }}
                            >
                              Первоначальная цена
                            </div>
                            <div
                              style={{
                                textDecoration: "line-through",
                                fontSize: "33px",
                              }}
                            >
                              {event.price}
                              <span>₽</span>
                            </div>
                          </span>
                        </del>
                        <ins
                          className={classes.ins}
                          style={{ textAlign: "left", fontSize: "57px" }}
                        >
                          <span>
                            <div
                              style={{
                                fontSize: "20px",
                                fontWeight: "600",
                                letterSpacing: "-0.5px",
                              }}
                            >
                              ЦЕНА СО СКИДКОЙ
                            </div>
                            {event.now_price}
                            <span>₽</span>
                          </span>
                        </ins>
                      </span>
                    ) : (
                      <span className={classes.price}>
                        <span>
                          {event.price}
                          <span>₽</span>
                        </span>
                      </span>
                    )}
                  </div>
                  <div style={{ margin: "68px 0 25px 0" }}>
                    <BorderLinearProgress
                      variant="determinate"
                      color="secondary"
                      value={normalise(event.tickets - event.left)}
                      span_value={(event.limit_off_sale / event.tickets) * 100}
                    />
                    <div
                      style={{
                        marginTop: "15px",
                        fontSize: "12px",
                        fontWeight: "600",
                      }}
                    >
                      Билетов продано:{" "}
                      <span style={{ color: "#55A187" }}>
                        {event.tickets - event.left}
                      </span>
                      /{event.tickets}
                    </div>
                    <div
                      style={{
                        fontSize: "12px",
                        fontWeight: "600",
                      }}
                    >
                      Билетов осталось: {event.left}
                    </div>
                  </div>
                  <form onSubmit={addToCartClick}>
                    <div style={{ marginTop: "45px", marginBottom: "10px" }}>
                      <FormControl style={{ width: "100%" }}>
                        <Typography
                          style={{
                            fontSize: "25px",
                            fontWeight: "600",
                            textTransform: "uppercase",
                          }}
                        >
                          {event.question}
                        </Typography>
                        <Select
                          labelId="demo-customized-select-label"
                          id="demo-customized-select"
                          value={answer.text ? answer.text : ""}
                          input={<BootstrapInput />}
                          displayEmpty
                        >
                          <MenuItem value="" disabled>
                            Выберите ответ.
                          </MenuItem>
                          {event.answers &&
                            event.answers.map((answer) => (
                              <MenuItem
                                key={answer.id}
                                value={answer.text}
                                onClick={() => handleChangeSelect(answer)}
                              >
                                {answer.text}
                              </MenuItem>
                            ))}
                        </Select>
                      </FormControl>
                    </div>
                    <div>
                      <FormControl
                        style={{
                          marginBottom: "20px",
                          width: "50%",
                          display: "flex",
                          flexDirection: "row",
                        }}
                      >
                        <NativeSelect
                          id="demo-customized-select-native"
                          value={quantity}
                          onChange={handleChange}
                          required={true}
                          input={<BootstrapInput />}
                          style={{
                            width: "50%",
                            color: "#55A187",
                          }}
                        >
                          {limit_arr[0] &&
                            limit_arr.map((item) => (
                              <option
                                aria-label="None"
                                value={item}
                                style={{ color: "#55A187" }}
                              >
                                {item}
                              </option>
                            ))}
                        </NativeSelect>
                        <button
                          className={classes.add_button}
                          // onClick={() => {
                          //   addToCartClick();
                          // }}
                        >
                          В корзину
                        </button>
                      </FormControl>
                    </div>
                  </form>
                </div>
              </div>
            </Grid>
          </Grid>
          <Grid
            container
            style={{
              borderTop: "3px solid #55A187",
              backgroundColor: "#1c1b21",
            }}
          >
            <Grid item xs={11} style={{ margin: "auto" }}>
              <Grid
                item
                style={{
                  padding: "38px 0 ",
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                  }}
                >
                  <div
                    className={"stat-bhp-white"}
                    style={{
                      width: "80px",
                      height: "80px",
                    }}
                  />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      marginLeft: "20px",
                      justifyContent: "center",
                    }}
                  >
                    <span style={{ fontSize: "18px", color: "#55A187" }}>
                      л.с.
                    </span>
                    <span
                      style={{
                        fontSize: "25px",
                        color: "white",
                        fontWeight: "600",
                      }}
                    >
                      {event.bhp}
                    </span>
                  </div>
                </div>
                <Grid item>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className={"stat-0-60-white"}
                      style={{
                        width: "80px",
                        height: "80px",
                      }}
                    />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        marginLeft: "20px",
                        justifyContent: "center",
                      }}
                    >
                      <span style={{ fontSize: "18px", color: "#55A187" }}>
                        0-100
                      </span>
                      <span
                        style={{
                          fontSize: "25px",
                          color: "white",
                          fontWeight: "600",
                        }}
                      >
                        {event.speed}
                      </span>
                    </div>
                  </div>
                </Grid>
                <Grid item>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className={"stat-oil-white"}
                      style={{
                        width: "80px",
                        height: "80px",
                      }}
                    />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        marginLeft: "20px",
                        justifyContent: "center",
                      }}
                    >
                      <span style={{ fontSize: "18px", color: "#55A187" }}>
                        л./100км
                      </span>
                      <span
                        style={{
                          fontSize: "25px",
                          color: "white",
                          fontWeight: "600",
                        }}
                      >
                        {event.mpg}
                      </span>
                    </div>
                  </div>
                </Grid>
                <Grid item>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className={"stat-miles-white"}
                      style={{
                        width: "80px",
                        height: "80px",
                      }}
                    />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        marginLeft: "20px",
                        justifyContent: "center",
                      }}
                    >
                      <span style={{ fontSize: "18px", color: "#55A187" }}>
                        км.
                      </span>
                      <span
                        style={{
                          fontSize: "25px",
                          color: "white",
                          fontWeight: "600",
                        }}
                      >
                        {event.miles}
                      </span>
                    </div>
                  </div>
                </Grid>
                <Grid item>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      className={"stat-year-white"}
                      style={{
                        width: "80px",
                        height: "80px",
                      }}
                    />
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        marginLeft: "20px",
                        justifyContent: "center",
                      }}
                    >
                      <span style={{ fontSize: "18px", color: "#55A187" }}>
                        год
                      </span>
                      <span
                        style={{
                          fontSize: "25px",
                          color: "white",
                          fontWeight: "600",
                        }}
                      >
                        {event.year}
                      </span>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid container style={{ padding: "78px 70px 0 70px" }}>
            <Grid item xs={2} className={classes.switcher_container}>
              <div onClick={handleFirstClick}>
                <div
                  className={`${classes.switcher_link} ${
                    isFirstActive && classes.link_active
                  }`}
                  style={{ cursor: "pointer" }}
                >
                  ВСЕ
                </div>
              </div>
              <div onClick={handleSecondClick}>
                <div
                  className={`${classes.switcher_link} ${
                    isSecondActive && classes.link_active
                  }`}
                  style={{ cursor: "pointer" }}
                >
                  АВТОМОБИЛИ
                </div>
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              style={{ backgroundColor: "#F2F2F2", padding: "77px" }}
            >
              {event.description && (
                <>
                  {isFirstActive ? (
                    <div
                      dangerouslySetInnerHTML={{ __html: event.description }}
                    ></div>
                  ) : (
                    <div
                      dangerouslySetInnerHTML={{ __html: description }}
                    ></div>
                  )}
                </>
              )}
            </Grid>
          </Grid>
        </Grid>
      )}

      <Grid container>
        <Grid
          item
          xs={12}
          style={{
            borderTop: "2px solid #55a187",
            position: "relative",
            paddingBottom: "110px",
            backgroundColor: "#1c1b21",
          }}
        >
          <Grid container>
            <Grid item>
              <Typography className={classes.second_slider_header}>
                Новые розыгрыши
              </Typography>
            </Grid>
          </Grid>
          {all_loading ? (
            <Grid
              item
              xs={12}
              style={{
                padding: "48px 0",
                display: "flex",
                justifyContent: "center",
                marginTop: "70px",
              }}
            >
              <CircularProgress color="secondary" size={100} />
            </Grid>
          ) : (
            <Grid item xs={12}>
              <Grid container style={{ marginTop: "10px", padding: "0 40px" }}>
                {all_events[0] &&
                  all_events.map((item) => {
                    return (
                      <Grid
                        xs={4}
                        style={{ padding: "5px 15px", marginBottom: "10px" }}
                      >
                        <Card className={classes.card_root}>
                          <CardActionArea onClick={() => handleClick(item.id)}>
                            <CardContent
                              style={{ padding: "0", position: "relative" }}
                            >
                              <CardMedia
                                className={classes.media}
                                image={`${item.photo[0]}`}
                                title="Contemplative Reptile"
                                style={{
                                  aspectRatio: "auto 588 / 424",
                                  height: "20vw",
                                }}
                              />
                              {item.tickets * 1 === 0 && (
                                <span className={classes.span}>
                                  <span className={classes.sold_span_body}>
                                    ПРОДАНО!
                                  </span>
                                </span>
                              )}
                              {item.tickets * 1 !== 0 &&
                                item.now_price * 1 > 0 && (
                                  <span className={classes.span}>
                                    <span className={classes.launch_span_body}>
                                      СКИДКА!
                                    </span>
                                  </span>
                                )}
                              {item.tickets * 1 === 0 && (
                                <div className={classes.today_span}>
                                  Розыгрыш сегодня в 9:15 pm!
                                </div>
                              )}
                            </CardContent>
                            <CardContent>
                              <Typography
                                gutterBottom
                                variant="h5"
                                component="h2"
                                className={classes.card_name}
                              >
                                {item.name}
                              </Typography>
                              {item.tickets * 1 !== 0 && (
                                <Fragment>
                                  {" "}
                                  {item.now_price ? (
                                    <span className={classes.price}>
                                      <del>
                                        <span className={classes.del}>
                                          {item.price}
                                          <span>₽</span>
                                        </span>
                                      </del>
                                      <ins className={classes.ins}>
                                        <span>
                                          {item.now_price}
                                          <span>₽</span>
                                        </span>
                                      </ins>
                                    </span>
                                  ) : (
                                    <span className={classes.price}>
                                      <span>
                                        {item.price}
                                        <span>₽</span>
                                      </span>
                                    </span>
                                  )}
                                </Fragment>
                              )}
                            </CardContent>
                            {item.tickets * 1 === 0 ? (
                              <CardContent className={classes.cart_footer}>
                                <div className={classes.footer_text}>
                                  <TimerIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  Draw Date: {item.date}
                                </div>
                              </CardContent>
                            ) : (
                              <CardContent className={classes.cart_footer}>
                                <div className={classes.footer_text}>
                                  <TimerIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  {item.time}
                                </div>
                                <div className={classes.footer_text}>
                                  <SvgIcon
                                    style={{
                                      fontSize: "18px",
                                      marginRight: "5px",
                                    }}
                                  >
                                    <svg
                                      version="1.1"
                                      id="Capa_1"
                                      xmlns="http://www.w3.org/2000/svg"
                                      x="0px"
                                      y="0px"
                                      viewBox="0 0 512 512"
                                    >
                                      <g>
                                        <g>
                                          <path
                                            d="M448.678,128.219l-10.607,10.608c-8.667,8.667-20.191,13.44-32.449,13.44c-12.258,0-23.78-4.773-32.448-13.44
			c-8.667-8.667-13.44-20.191-13.44-32.448s4.773-23.781,13.44-32.449l10.608-10.608L320.459,0L0,320.459l63.322,63.322
			l10.608-10.608c8.667-8.667,20.191-13.44,32.449-13.44c12.258,0,23.78,4.773,32.448,13.44c8.667,8.667,13.44,20.191,13.44,32.448
			s-4.773,23.781-13.44,32.449l-10.608,10.608L191.541,512L512,191.541L448.678,128.219z M169.61,447.636
			c8.237-12.343,12.662-26.839,12.662-42.015c0-20.272-7.894-39.33-22.229-53.664c-14.334-14.335-33.393-22.229-53.664-22.229
			c-15.176,0-29.672,4.425-42.015,12.662l-21.932-21.931L320.459,42.432l21.931,21.932c-8.237,12.343-12.662,26.839-12.662,42.015
			c0,20.272,7.894,39.33,22.229,53.664c14.334,14.335,33.393,22.229,53.664,22.229c15.176,0,29.672-4.425,42.015-12.662
			l21.932,21.931L191.541,469.568L169.61,447.636z"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="284.001"
                                            y="197.94"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -63.0395 273.8137)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="241.404"
                                            y="155.325"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -45.3819 231.2119)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g>
                                        <g>
                                          <rect
                                            x="326.607"
                                            y="240.541"
                                            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -80.684 316.4184)"
                                            width="30.004"
                                            height="30.124"
                                          />
                                        </g>
                                      </g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                      <g></g>
                                    </svg>
                                  </SvgIcon>
                                  {item.left} Осталось
                                </div>
                                <div className={classes.footer_text}>
                                  <PersonOutlineIcon
                                    style={{
                                      fontSize: "20px",
                                      marginRight: "2px",
                                    }}
                                  />
                                  макс. {item.limit_person} н.ч.
                                </div>
                              </CardContent>
                            )}
                          </CardActionArea>
                        </Card>
                      </Grid>
                    );
                  })}
              </Grid>
            </Grid>
          )}
          <Link to={"/latest-competitions"} className={"link-to"}>
            Все розыгрыши
          </Link>
        </Grid>
      </Grid>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  events: state.events,
});

export default connect(mapStateToProps, { getEvent, addToCart })(
  withStyles(styles)(CompetitionPage)
);
