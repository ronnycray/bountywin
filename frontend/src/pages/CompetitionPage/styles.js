export const styles = (theme) => ({
  wrapper: {
    paddingTop: "72px",
  },
  product_container: {
    paddingBottom: "110px",
  },
  product_info_container: {
    padding: "0 70px",
  },
  product_photo: {
    padding: "0 60px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  prev_arrow: {},
  next_arrow: {},
  product_info: {},
  info_title: {
    fontSize: "42px",
    textTransform: "uppercase",
    fontWeight: "600",
    lineHeight: "1.2",
    marginBottom: "20px",
  },
  title_span: {
    fontSize: "27px",
    color: "#55A187",
    marginBottom: "10px",
  },
  info_timer: {},
  timer_title: {
    fontSize: "19px",
    fontWeight: "700",
    marginBottom: "12px",
  },
  tickets_title: {
    fontSize: "19px",
    fontWeight: "bold",
    marginBottom: "19px",
  },
  tickets_info: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: "10px",
    marginBottom: "24px",
  },
  tickets_info_count: {
    display: "flex",
    alignItems: "center",
    fontSize: "16px",
    marginRight: "20px",
  },
  tickets_info_limit: {
    display: "flex",
    alignItems: "center",
    fontSize: "16px",
  },
  limit_span: {
    color: "#55A187",
    fontWeight: "700",
    margin: "0 6px",
  },
  info_prices: {
    color: "#55A187",
    display: "inline-block",
  },
  info_del: { textDecoration: "none" },
  del_span: { textDecoration: "line-through" },
  info_ins: {},

  second_slider_header: {
    color: "white",
    fontSize: "34px",
    padding: "12px 8px 10px 40px",
    margin: "0 0 30px 0",
    zIndex: 1,
    background: "#55a187",
    position: "relative",
    "&::after": {
      content: '""',
      transform: "skew(-18deg)",
      position: "absolute",
      width: "40%",
      height: "100%",
      background: "inherit",
      zIndex: -1,
      top: 0,
      bottom: 0,
      right: "-12%",
      boxSizing: "border-box",
    },
  },

  media: {
    position: "relative",
  },
  span: {
    position: "absolute",
    top: "15px",
    right: 0,
    margin: 0,
    color: "white",
  },
  sold_span_body: {
    backgroundColor: "#1c1b21",
    borderRadius: 0,
    minHeight: "auto",
    lineHeight: "normal",
    padding: "14px 16px",
    display: "inline-block",
    top: "auto",
    color: "white",
    left: "auto",
    fontSize: "14px",
    fontWeight: "700",
    position: "relative",
    margin: 0,
    textTransform: "uppercase",
    zIndex: 2,
    "&::before": {
      content: "''",
      position: "absolute",
      left: "-12%",
      bottom: 0,
      height: "100%",
      width: "45%",
      transform: "skew(-18deg)",
      background: "inherit",
      zIndex: -1,
    },
  },
  launch_span_body: {
    backgroundColor: "#55a187",
    borderRadius: 0,
    minHeight: "auto",
    lineHeight: "normal",
    padding: "14px 16px",
    display: "inline-block",
    top: "auto",
    color: "white",
    left: "auto",
    fontSize: "14px",
    fontWeight: "700",
    position: "relative",
    margin: 0,
    textTransform: "uppercase",
    zIndex: 2,
    "&::before": {
      content: "''",
      position: "absolute",
      left: "-12%",
      bottom: 0,
      height: "100%",
      width: "45%",
      transform: "skew(-18deg)",
      background: "inherit",
      zIndex: -1,
    },
  },
  today_span: {
    position: "absolute",
    bottom: 0,
    right: 0,
    left: 0,
    textAlign: "center",
    backgroundColor: "#1c1b21",
    color: "#f2f2f2",
    letterSpacing: "0.2px",
    fontWeight: "700",
    fontSize: "15px",
    textTransform: "uppercase",
    padding: "8px 0",
    "&::before": {
      content: "''",
      width: "10px",
      height: "10px",
      display: "inline-block",
      borderRadius: "50%",
      backgroundColor: "#55a187",
      marginRight: "5px",
      WebkitAnimation: "blink 2s forwards infinite",
      animation: "blink 2s forwards infinite",
    },
  },
  card_name: {
    fontSize: "18px !important",
    fontWeight: "600",
    marginBottom: "5px",
    lineHeight: "1.2",
  },
  price: {
    color: "#55a187",
    fontWeight: "700",
    fontSize: "31px",
    position: "relative",
    textAlign: "center",
    marginBottom: 0,
    display: "inline-block",
  },
  del: {
    color: "#888",
    position: "absolute",
    left: "110%",
    fontSize: "16px",
    top: "50%",
    transform: "translateY(-50%)",
    opacity: ".5",
    display: "inline-block",
    fontWeight: "200",
    textDecoration: "line-through",
  },
  ins: {
    background: "0 0",
    fontWeight: "700",
    display: "inline-block",
    textDecoration: "none",
  },
  cart_footer: {
    padding: "16px",
    borderTop: "2px solid #55a187",
    display: "flex",
    justifyContent: "space-between",
  },
  footer_timer: {},
  footer_tickets: {},
  footer_max: {},
  footer_text: {
    fontSize: "13px",
    backgroundSize: "11px",
    paddingLeft: "13px",
    color: "#808080",
    display: "flex",
    alignItems: "center",
  },

  switcher_container: {
    paddingTop: "16px",
    display: "flex",
  },
  switcher_link: {
    padding: "25px 75px 25px 44px",
    display: "inline-block",
    lineHeight: "normal",
    color: "#515151",
    position: "relative",
    fontSize: "32px",
    letterSpacing: ".02em",
    fontWeight: "500",
    backgroundColor: "#d9d9d9",
    textDecoration: "none",
    "&::after": {
      width: "22%",
      height: "100%",
      transform: "skew(-18deg)",
      background: "inherit",
      position: "absolute",
      top: 0,
      content: "''",
      zIndex: 1,
      left: "85%",
    },
  },
  link_active: {
    backgroundColor: "#55a187 !important",
    color: "#f2f2f2",
  },
  add_button: {
    display: "flex",
    alignItems: "center",
    color: "white",
    backgroundColor: "#55A187",
    padding: "5px 15px",
    fontSize: "20px",
    borderRadius: "0 15px 15px 0",
    cursor: "pointer",
    transition: "0.3s",
    border: "1px solid #55A187",
    "&:hover": {
      opacity: 0.5,
    },
  },
});
