export const styles = (theme) => ({
  top_container: {
    backgroundColor: "#1c1b21 !important",
  },
  header: {
    fontSize: "34px",
    margin: "0 0 30px",
    padding: "12px 8px 10px 40px",
    backgroundColor: "#55A187",
    color: "white",
    zIndex: 2,
    position: "relative",
    "&::after": {
      content: '""',
      transform: "skew(-18deg)",
      position: "absolute",
      width: "40%",
      height: "100%",
      backgroundColor: "#55A187",
      zIndex: -1,
      boxSizing: "border-box",
      display: "block",
      top: 0,
      bottom: 0,
      right: "-12%",
    },
  },
  avatar: {
    width: "200px",
    height: "200px",
  },
  link_edit: {
    textDecoration: "none",
    color: "#55A187",
    transition: "0.3s",
    fontSize: "19px",
    "&:hover": {
      opacity: 0.5,
    },
  },
  menu_item: {
    padding: 0,
    transition: "0.3s",
    display: "flex",
    justifyContent: "space-between",
    "&:hover": {
      opacity: 0.9,
      color: "#55A187",
    },
    borderBottom: "1px solid gray",
  },
  menu_item_text: {
    fontSize: "20px",
    fontWeight: "600",
    display: "flex",
    justifyContent: "flex-start",
  },
  navlink: {
    padding: "20px 15px",
    color: "inherit",
    textDecoration: "none",
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
  },
  active_navlink: {
    backgroundColor: "#55A187",
    color: "white",
  },
  content: {
    boxShadow: "0 10px 15px rgb(27 28 32 / 10%)",
    padding: "28px 38px",
  },
});
