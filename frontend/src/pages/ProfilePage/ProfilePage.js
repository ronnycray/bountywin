import React, { useEffect, Fragment } from "react";
import {
  Grid,
  Typography,
  Container,
  Avatar,
  MenuList,
  MenuItem,
  SvgIcon,
  CircularProgress,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { styles } from "./styles";
import { Link, NavLink, Route } from "react-router-dom";
import EventNoteOutlinedIcon from "@material-ui/icons/EventNoteOutlined";
import ControlPointDuplicateOutlinedIcon from "@material-ui/icons/ControlPointDuplicateOutlined";
import AssignmentIndOutlinedIcon from "@material-ui/icons/AssignmentIndOutlined";
import ExitToAppOutlinedIcon from "@material-ui/icons/ExitToAppOutlined";
import { getProfile } from "../../actions/profileActions";
import { logout } from "../../actions/authActions";

const ProfilePage = ({
  classes,
  profile: { info, loading },
  getProfile,
  logout,
  children,
  ...rest
}) => {
  useEffect(() => {
    getProfile();
  }, []);
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container style={{ backgroundColor: "#1c1b21" }}>
        <Grid item>
          <Typography className={classes.header}>МОЙ АККАУНТ</Typography>
        </Grid>
      </Grid>
      {loading ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
            marginTop: "50px",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Fragment>
          <Grid
            container
            style={{
              backgroundColor: "#1c1b21",
              justifyContent: "center",
              paddingBottom: "80px",
            }}
          >
            <Grid
              item
              xs={10}
              style={{ display: "flex", flexDirection: "row" }}
            >
              <Avatar
                src={info.photo ? info.photo : "/broken-image.jpg"}
                classes={{
                  root: classes.avatar,
                }}
                style={{ marginRight: "25px" }}
              />
              <div
                style={{
                  color: "white",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                }}
              >
                <div style={{ fontSize: "45px", marginBottom: "10px" }}>
                  {info.username}
                </div>
                <div style={{ fontSize: "19px", marginBottom: "19px" }}>
                  {info.email}
                </div>
                <Link to={"/profile/account"} className={classes.link_edit}>
                  Edit info
                </Link>
              </div>
            </Grid>
          </Grid>
          <Container
            maxWidth={"xl"}
            style={{
              marginTop: "60px",
              width: "93%",
            }}
          >
            <Grid
              container
              style={{
                justifyContent: "space-between",
              }}
            >
              <Grid item xs={3}>
                <MenuList
                  style={{
                    backgroundColor: "#1c1b21",
                    color: "white",
                    padding: 0,
                    borderRadius: "8px",
                  }}
                >
                  <MenuItem className={classes.menu_item}>
                    <NavLink
                      to="/profile/entries"
                      className={classes.navlink}
                      activeClassName={classes.active_navlink}
                    >
                      <span className={classes.menu_item_text}>
                        <SvgIcon
                          style={{
                            fontSize: "20px",
                            margin: "5px 15px 5px 0",
                          }}
                        >
                          <svg
                            version="1.1"
                            id="Capa_1"
                            xmlns="http://www.w3.org/2000/svg"
                            x="0px"
                            y="0px"
                            viewBox="0 0 512 512"
                          >
                            <g>
                              <g>
                                <path
                                  d="M448.678,128.219l-10.607,10.608c-8.667,8.667-20.191,13.44-32.449,13.44c-12.258,0-23.78-4.773-32.448-13.44
			c-8.667-8.667-13.44-20.191-13.44-32.448s4.773-23.781,13.44-32.449l10.608-10.608L320.459,0L0,320.459l63.322,63.322
			l10.608-10.608c8.667-8.667,20.191-13.44,32.449-13.44c12.258,0,23.78,4.773,32.448,13.44c8.667,8.667,13.44,20.191,13.44,32.448
			s-4.773,23.781-13.44,32.449l-10.608,10.608L191.541,512L512,191.541L448.678,128.219z M169.61,447.636
			c8.237-12.343,12.662-26.839,12.662-42.015c0-20.272-7.894-39.33-22.229-53.664c-14.334-14.335-33.393-22.229-53.664-22.229
			c-15.176,0-29.672,4.425-42.015,12.662l-21.932-21.931L320.459,42.432l21.931,21.932c-8.237,12.343-12.662,26.839-12.662,42.015
			c0,20.272,7.894,39.33,22.229,53.664c14.334,14.335,33.393,22.229,53.664,22.229c15.176,0,29.672-4.425,42.015-12.662
			l21.932,21.931L191.541,469.568L169.61,447.636z"
                                />
                              </g>
                            </g>
                            <g>
                              <g>
                                <rect
                                  x="284.001"
                                  y="197.94"
                                  transform="matrix(0.7071 -0.7071 0.7071 0.7071 -63.0395 273.8137)"
                                  width="30.004"
                                  height="30.124"
                                />
                              </g>
                            </g>
                            <g>
                              <g>
                                <rect
                                  x="241.404"
                                  y="155.325"
                                  transform="matrix(0.7071 -0.7071 0.7071 0.7071 -45.3819 231.2119)"
                                  width="30.004"
                                  height="30.124"
                                />
                              </g>
                            </g>
                            <g>
                              <g>
                                <rect
                                  x="326.607"
                                  y="240.541"
                                  transform="matrix(0.7071 -0.7071 0.7071 0.7071 -80.684 316.4184)"
                                  width="30.004"
                                  height="30.124"
                                />
                              </g>
                            </g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                            <g></g>
                          </svg>
                        </SvgIcon>
                        Мои Конкурсы
                      </span>
                      <button
                        className={"competition_arrows"}
                        // onClick={() => nextClick()}
                      >
                        <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="3 5 32 32"
                            width="32"
                            height="32"
                          >
                            <path
                              fill="#000"
                              d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                            />
                          </svg>
                        </SvgIcon>
                      </button>
                    </NavLink>
                  </MenuItem>
                  <MenuItem className={classes.menu_item}>
                    <NavLink
                      to="/profile/orders"
                      className={classes.navlink}
                      activeClassName={classes.active_navlink}
                    >
                      <span className={classes.menu_item_text}>
                        <EventNoteOutlinedIcon
                          style={{
                            fontSize: "20px",
                            margin: "5px 15px 5px 0",
                          }}
                        />
                        Мои Покупки
                      </span>
                      <button
                        className={"competition_arrows"}
                        // onClick={() => nextClick()}
                      >
                        <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="3 5 32 32"
                            width="32"
                            height="32"
                          >
                            <path
                              fill="#000"
                              d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                            />
                          </svg>
                        </SvgIcon>
                      </button>
                    </NavLink>
                  </MenuItem>
                  <MenuItem className={classes.menu_item}>
                    <NavLink
                      to="/profile/points"
                      className={classes.navlink}
                      activeClassName={classes.active_navlink}
                    >
                      <span className={classes.menu_item_text}>
                        <ControlPointDuplicateOutlinedIcon
                          style={{
                            fontSize: "20px",
                            margin: "5px 15px 5px 0",
                          }}
                        />
                        Мои Очки
                      </span>
                      <button className={"competition_arrows"}>
                        <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="3 5 32 32"
                            width="32"
                            height="32"
                          >
                            <path
                              fill="#000"
                              d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                            />
                          </svg>
                        </SvgIcon>
                      </button>
                    </NavLink>
                  </MenuItem>
                  <MenuItem className={classes.menu_item}>
                    <NavLink
                      to="/profile/account"
                      className={classes.navlink}
                      activeClassName={classes.active_navlink}
                    >
                      <span className={classes.menu_item_text}>
                        <AssignmentIndOutlinedIcon
                          style={{
                            fontSize: "20px",
                            margin: "5px 15px 5px 0",
                          }}
                        />
                        Моя Информация
                      </span>
                      <button className={"competition_arrows"}>
                        <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="3 5 32 32"
                            width="32"
                            height="32"
                          >
                            <path
                              fill="#000"
                              d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                            />
                          </svg>
                        </SvgIcon>
                      </button>
                    </NavLink>
                  </MenuItem>
                  <MenuItem className={classes.menu_item}>
                    <NavLink
                      to="/#"
                      onClick={logout}
                      className={classes.navlink}
                    >
                      <span className={classes.menu_item_text}>
                        <ExitToAppOutlinedIcon
                          style={{
                            fontSize: "20px",
                            margin: "5px 15px 5px 0",
                          }}
                        />
                        Выйти
                      </span>
                      <button className={"competition_arrows"}>
                        <SvgIcon style={{ color: "white", fontSize: "12px" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="3 5 32 32"
                            width="32"
                            height="32"
                          >
                            <path
                              fill="#000"
                              d="M24.3 14.3l-9.6-9.6c-.9-.9-2.3-.9-3.2 0l-.8.8c-.9.9-.9 2.3 0 3.2L18 16l-7.3 7.3c-.9.9-.9 2.3 0 3.2l.8.8c.9.9 2.3.9 3.2 0l9.6-9.6c.5-.5.7-1.1.6-1.7.1-.6-.1-1.3-.6-1.7z"
                            />
                          </svg>
                        </SvgIcon>
                      </button>
                    </NavLink>
                  </MenuItem>
                </MenuList>
              </Grid>
              <Grid item xs={8}>
                <div className={classes.content}>{children}</div>
              </Grid>
            </Grid>
          </Container>
        </Fragment>
      )}
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, { getProfile, logout })(
  withStyles(styles)(ProfilePage)
);
