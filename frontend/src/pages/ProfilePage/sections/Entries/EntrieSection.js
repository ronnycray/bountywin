import React from "react";
import { connect } from "react-redux";

const EntrieSection = ({ profile: { info } }) => {
  return (
    <div>
      Приветствуем {info.usermame} (Не {info.usermame}? Выйти)
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(EntrieSection);
