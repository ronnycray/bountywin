import React from "react";
import { connect } from "react-redux";

const OrdersSection = ({ profile: { info } }) => {
  return (
    <div>
      Приветствуем {info.usermame} (Не {info.usermame}? Выйти)
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(OrdersSection);
