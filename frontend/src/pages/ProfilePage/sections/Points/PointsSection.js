import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { connect } from "react-redux";
import CustomInput from "../../../../components/common/CustomInput/CustomInput";

const styles = (theme) => ({
  header: {
    fontSize: "30px",
    fontWeight: "600",
    margin: "19px 0",
  },

  title: {
    fontSize: "28px",
    marginBottom: "9px",
  },
});

const PointsSection = ({ profile: { info }, classes }) => {
  const [code, setCode] = useState("");
  return (
    <div>
      <Typography className={classes.header}>МОИ ОЧКИ</Typography>
      <div style={{ fontSize: "16px", marginBottom: "10px" }}>
        У вас {info.dream_points} очков.
      </div>
      <Typography className={classes.title}>
        Активация подарочной карты
      </Typography>
      <div style={{ fontSize: "16px" }}>
        Введите код подарочной карты чтобы получить очки.
      </div>
      <CustomInput
        label="Код"
        labelColor="black"
        name="code"
        type="text"
        value={code}
        onChange={(e) => setCode(e.target.value)}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps)(withStyles(styles)(PointsSection));
