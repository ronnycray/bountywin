import React from "react";
import { connect } from "react-redux";
import { logout } from "../../../../actions/authActions";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const styles = (theme) => ({
  link: {
    color: "#55A187",
    textDecoration: "none",
    transition: "0.3s",
    "&:hover": {
      opacity: "0.5",
    },
  },

  link_edit: {
    textDecoration: "none",
    color: "#55A187",
    transition: "0.3s",
    fontSize: "16px",
    "&:hover": {
      opacity: 0.5,
    },
  },
});

const MainSection = ({ profile: { info }, logout, classes }) => {
  console.log("INFo", info);
  return (
    <div style={{ fontSize: "16px" }}>
      <div style={{ marginBottom: "19px" }}>
        Приветствуем <strong>{info.username}</strong> (Не{" "}
        <strong>{info.username}</strong>?{" "}
        <Link to={"/login"} onClick={logout} className={classes.link}>
          Выйти
        </Link>
        )
      </div>
      На панели управления учетной записью вы можете{" "}
      <Link to={"/profile/entries"} className={classes.link_edit}>
        просматривать розыгрыши
      </Link>{" "}
      в которых вы участвуете,{" "}
      <Link to={"/profile/orders"} className={classes.link_edit}>
        ваши заказы
      </Link>
      ,{" "}
      <Link to={"/profile/points"} className={classes.link_edit}>
        посмотреть количество очков и использовать подарочную карту
      </Link>{" "}
      а также{" "}
      <Link to={"/profile/account"} className={classes.link_edit}>
        изменить пароль, e-mail и данные учетной записи.
      </Link>
    </div>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, { logout })(
  withStyles(styles)(MainSection)
);
