import {
  Typography,
  Button,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Grid,
  CircularProgress,
  Select,
  InputBase,
  MenuItem,
} from "@material-ui/core";
import React, { useState, useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import LoadingButton from "@material-ui/lab/LoadingButton";
import { useFormik } from "formik";
import CustomInput from "../../../../components/common/CustomInput";
import {
  changePasswordValidationSchema,
  updateProfileValidationSchema,
  changeEmailValidationSchema,
} from "../../../../utils/validationSchemas";
import {
  getRegions,
  updateProfile,
  changeEmail,
  changePassword,
} from "../../../../actions/profileActions";

const styles = (theme) => ({
  header: {
    fontSize: "30px",
    fontWeight: "600",
    margin: "19px 0",
  },
  section: {
    borderBottom: "1px solid #c5c5c5",
    paddingBottom: "38px",
    marginBottom: "38px",
  },
  title: {
    fontSize: "28px",
    marginBottom: "9px",
  },
  input_file: {
    display: "none",
  },

  button: {
    backgroundColor: "#55a187",
    color: "white",
    marginTop: "10px",
    "&:hover": {
      background: "rgba(85, 161, 135, 0.8)",
    },
    padding: "6px 16px",
  },
  button_cancel: {
    backgroundColor: "#1c1b21",
    color: "white",
    marginTop: "10px",
    "&:hover": {
      background: "rgba(28, 27, 33, 0.8)",
    },
    padding: "6px 16px",
  },

  input_label_black: {
    padding: "10px 0",
    color: "black",
    fontSize: "16px",
  },
  required: {
    fontWeight: "700",
    color: "#55a187",
    textDecoration: "none",
    border: 0,
  },
});
const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 16,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    display: "flex",
    alignItems: "center",
    width: "100%",
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 16,
      borderColor: "#ced4da",
      border: "3px",
      boxShadow: "0 0 0 0.2rem #55A187",
    },
  },
}))(InputBase);

const InfoSection = ({
  profile: { info, regions, loading_regions, loading },
  getRegions,
  changePassword,
  updateProfile,
  changeEmail,
  classes,
}) => {
  const [photo, setPhoto] = useState();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [open, setOpen] = useState(false);
  const [countrys, setCountrys] = useState(regions);
  const [country, setCountry] = useState({});
  const [city, setCity] = useState({});

  console.log("PHOTO", photo);
  console.log("COUNTYS", countrys);
  const onChangePhoto = (event) => {
    const [files] = Array.from(event.target.files);
    setPhoto(files);
  };

  useEffect(() => {
    getRegions();
  }, []);
  const formikUpdateProfile = useFormik({
    initialValues: {
      first_name: info.first_name,
      last_name: info.second_name,
      company_name: info.company,
      country: country,
      city: city,
      address: info.address,
      postal: info.postcode,
      phone: info.phone,
      date_of_birth: info.day_birth,
    },
    validationSchema: updateProfileValidationSchema,
    onSubmit: (values) => {
      console.log("UPDATE", values);
      console.log("PHOtooOO", photo);
      updateProfile(values, photo);
    },
  });

  const handleChangeCountry = (country) => {
    setCountry(country);
    formikUpdateProfile.values.country = country;
  };

  const handleChangeCity = (city) => {
    setCity(city);
    formikUpdateProfile.values.city = city;
  };

  const onOpenClick = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const handleClickEmail = () => {
    changeEmail(email, password);
  };

  const formikChangeEmail = useFormik({
    initialValues: { email: "" },
    validationSchema: changeEmailValidationSchema,
    onSubmit: (values) => {
      setEmail(values.email);
      onOpenClick();
    },
  });

  const formikChangePassword = useFormik({
    initialValues: {
      old_password: "",
      password: "",
      confirm_password: "",
    },
    validationSchema: changePasswordValidationSchema,
    onSubmit: (values) => {
      console.log("REGISTER", values);
      changePassword(values.old_password, values.password);
    },
  });
  return (
    <Fragment>
      {loading_regions ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
            marginTop: "50px",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Fragment>
          <div>
            <Typography className={classes.header}>МОЙ ПРОФИЛЬ</Typography>
            <div className={classes.section}>
              <Typography className={classes.title}>Ваше Фото</Typography>
              <div
                style={{
                  width: "13%",
                }}
              >
                {console.log("value", formikUpdateProfile.values)}
                <input
                  accept="image/*"
                  className={classes.input_file}
                  id="contained-button-file"
                  type="file"
                  onChange={(event) => onChangePhoto(event)}
                />
                <label
                  htmlFor="contained-button-file"
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <LoadingButton
                    variant="contained"
                    color="primary"
                    component="span"
                    pending={loading}
                    className={classes.button}
                  >
                    Загрузить
                  </LoadingButton>
                  <label style={{ marginTop: "5px" }}>
                    Макс. размер: 50мб.
                  </label>
                </label>
              </div>
            </div>

            <form onSubmit={formikUpdateProfile.handleSubmit}>
              <div className={classes.section}>
                <Typography className={classes.title}>Личные Данные</Typography>
                <Grid container style={{ justifyContent: "space-between" }}>
                  <Grid item xs={5}>
                    <CustomInput
                      label="Имя"
                      labelColor="black"
                      isReq={true}
                      name="first_name"
                      type="text"
                      disabled={loading}
                      value={formikUpdateProfile.values.first_name}
                      onChange={formikUpdateProfile.handleChange}
                      error={
                        formikUpdateProfile.touched.first_name &&
                        Boolean(formikUpdateProfile.touched.first_name)
                      }
                      helperText={
                        formikUpdateProfile.touched.first_name &&
                        formikUpdateProfile.errors.first_name
                      }
                    />
                  </Grid>
                  <Grid item xs={5}>
                    <CustomInput
                      label="Фамилия"
                      labelColor="black"
                      isReq={true}
                      name="last_name"
                      type="text"
                      disabled={loading}
                      value={formikUpdateProfile.values.last_name}
                      onChange={formikUpdateProfile.handleChange}
                      error={
                        formikUpdateProfile.touched.last_name &&
                        Boolean(formikUpdateProfile.touched.last_name)
                      }
                      helperText={
                        formikUpdateProfile.touched.last_name &&
                        formikUpdateProfile.errors.last_name
                      }
                    />
                  </Grid>
                </Grid>
                <CustomInput
                  label="Название Компании"
                  labelColor="black"
                  name="company_name"
                  type="text"
                  disabled={loading}
                  value={formikUpdateProfile.values.company_name}
                  onChange={formikUpdateProfile.handleChange}
                  error={
                    formikUpdateProfile.touched.company_name &&
                    Boolean(formikUpdateProfile.touched.company_name)
                  }
                  helperText={
                    formikUpdateProfile.touched.company_name &&
                    formikUpdateProfile.errors.company_name
                  }
                />
                <div style={{ display: "flex" }}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      marginRight: "50px",
                    }}
                  >
                    <div className={classes.input_label_black}>
                      Выберите страну{" "}
                      <span className={classes.required}>*</span>
                    </div>
                    <Select
                      labelId="demo-customized-select-label"
                      id="demo-customized-select"
                      value={country.country ? country.country : ""}
                      input={<BootstrapInput />}
                      displayEmpty
                    >
                      <MenuItem value="" disabled>
                        Выберите страну.
                      </MenuItem>
                      {countrys[0] &&
                        countrys.map((country) => (
                          <MenuItem
                            key={country.id}
                            value={country.country}
                            onClick={() => {
                              handleChangeCountry(country);
                            }}
                          >
                            {country.country}
                          </MenuItem>
                        ))}
                    </Select>
                  </div>
                  {country.regions && (
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      <div className={classes.input_label_black}>
                        Выберите город{" "}
                        <span className={classes.required}>*</span>
                      </div>
                      <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={city.city ? city.city : ""}
                        input={<BootstrapInput />}
                        displayEmpty
                      >
                        <MenuItem value="" disabled>
                          Выберите город.
                        </MenuItem>
                        {country.regions &&
                          country.regions.map((city) => (
                            <MenuItem
                              key={city.id}
                              value={city.city}
                              onClick={() => {
                                handleChangeCity(city);
                              }}
                            >
                              {city.city}
                            </MenuItem>
                          ))}
                      </Select>
                    </div>
                  )}
                </div>
                <CustomInput
                  label="Адрес"
                  labelColor="black"
                  name="address"
                  isReq={true}
                  type="text"
                  disabled={loading}
                  value={formikUpdateProfile.values.address}
                  onChange={formikUpdateProfile.handleChange}
                  error={
                    formikUpdateProfile.touched.address &&
                    Boolean(formikUpdateProfile.touched.address)
                  }
                  helperText={
                    formikUpdateProfile.touched.address &&
                    formikUpdateProfile.errors.address
                  }
                />
                <CustomInput
                  label="Индекс"
                  labelColor="black"
                  name="postal"
                  isReq={true}
                  type="number"
                  disabled={loading}
                  value={formikUpdateProfile.values.postal}
                  onChange={formikUpdateProfile.handleChange}
                  error={
                    formikUpdateProfile.touched.postal &&
                    Boolean(formikUpdateProfile.touched.postal)
                  }
                  helperText={
                    formikUpdateProfile.touched.postal &&
                    formikUpdateProfile.errors.postal
                  }
                />
                <CustomInput
                  label="Телефон"
                  labelColor="black"
                  name="phone"
                  isReq={true}
                  type="number"
                  disabled={loading}
                  value={formikUpdateProfile.values.phone}
                  onChange={formikUpdateProfile.handleChange}
                  error={
                    formikUpdateProfile.touched.phone &&
                    Boolean(formikUpdateProfile.touched.phone)
                  }
                  helperText={
                    formikUpdateProfile.touched.phone &&
                    formikUpdateProfile.errors.phone
                  }
                />
                <CustomInput
                  label="Дата Рождения"
                  labelColor="black"
                  name="date_of_birth"
                  type="date"
                  disabled={loading}
                  value={formikUpdateProfile.values.date_of_birth}
                  onChange={formikUpdateProfile.handleChange}
                  error={
                    formikUpdateProfile.touched.date_of_birth &&
                    Boolean(formikUpdateProfile.touched.date_of_birth)
                  }
                  helperText={
                    formikUpdateProfile.touched.date_of_birth &&
                    formikUpdateProfile.errors.date_of_birth
                  }
                />
                <LoadingButton
                  variant="contained"
                  className={classes.button}
                  pending={loading}
                  type="submit"
                  onClick={formikUpdateProfile.handleSubmit}
                >
                  <span className={classes.register_button_text}>
                    Сохранить
                  </span>
                </LoadingButton>
              </div>
            </form>
            <div className={classes.section}>
              <form onSubmit={formikChangeEmail.handleSubmit}>
                <Typography className={classes.title}>
                  Изменить E-mail
                </Typography>
                <CustomInput
                  label="Введите новый E-mail"
                  labelColor="black"
                  isReq={true}
                  name="email"
                  type="email"
                  disabled={loading}
                  value={formikChangeEmail.values.email}
                  onChange={formikChangeEmail.handleChange}
                  error={
                    formikChangeEmail.touched.email &&
                    Boolean(formikChangeEmail.touched.email)
                  }
                  helperText={
                    formikChangeEmail.touched.email &&
                    formikChangeEmail.errors.email
                  }
                />
                <LoadingButton
                  variant="contained"
                  className={classes.button}
                  pending={loading}
                  type="submit"
                >
                  <span className={classes.register_button_text}>
                    Сменить E-mail
                  </span>
                </LoadingButton>
                <Dialog
                  open={open}
                  onClose={onClose}
                  aria-labelledby="form-dialog-title"
                >
                  <DialogTitle id="form-dialog-title">
                    Подтверждение смены E-mail
                  </DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Для подтверждения смены email введите пароль.
                    </DialogContentText>
                    <CustomInput
                      autoFocus
                      margin="dense"
                      id="name"
                      label="Пароль"
                      type="password"
                      fullWidth
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      disabled={loading}
                    />
                  </DialogContent>
                  <DialogActions>
                    <LoadingButton
                      onClick={onClose}
                      pending={loading}
                      color="primary"
                      className={classes.button_cancel}
                    >
                      Отмена
                    </LoadingButton>
                    <LoadingButton
                      onClick={() => handleClickEmail()}
                      pending={loading}
                      color="primary"
                      className={classes.button}
                    >
                      Сменить e-mail
                    </LoadingButton>
                  </DialogActions>
                </Dialog>
              </form>
            </div>
            <div className={classes.section}>
              <Typography className={classes.title}>Изменить Пароль</Typography>
              <form onSubmit={formikChangePassword.handleSubmit}>
                <Box className={classes.register_card}>
                  <CustomInput
                    label="Старый Пароль"
                    labelColor="black"
                    isReq={true}
                    name="old_password"
                    type="password"
                    disabled={loading}
                    value={formikChangePassword.values.old_password}
                    onChange={formikChangePassword.handleChange}
                    error={
                      formikChangePassword.touched.old_password &&
                      Boolean(formikChangePassword.touched.old_password)
                    }
                    helperText={
                      formikChangePassword.touched.old_password &&
                      formikChangePassword.errors.old_password
                    }
                  />
                  <CustomInput
                    label="Новый Пароль"
                    labelColor="black"
                    isReq={true}
                    name="password"
                    type="password"
                    disabled={loading}
                    value={formikChangePassword.values.password}
                    onChange={formikChangePassword.handleChange}
                    error={
                      formikChangePassword.touched.password &&
                      Boolean(formikChangePassword.touched.password)
                    }
                    helperText={
                      formikChangePassword.touched.password &&
                      formikChangePassword.errors.password
                    }
                  />
                  <CustomInput
                    label="Подтвердите Пароль"
                    labelColor="black"
                    isReq={true}
                    name="confirm_password"
                    type="password"
                    disabled={loading}
                    value={formikChangePassword.values.confirm_password}
                    onChange={formikChangePassword.handleChange}
                    error={
                      formikChangePassword.touched.confirm_password &&
                      Boolean(formikChangePassword.touched.confirm_password)
                    }
                    helperText={
                      formikChangePassword.touched.confirm_password &&
                      formikChangePassword.errors.confirm_password
                    }
                  />
                  <LoadingButton
                    variant="contained"
                    className={classes.button}
                    pending={loading}
                    type="submit"
                    onClick={formikChangePassword.handleSubmit}
                  >
                    <span className={classes.register_button_text}>
                      Сменить пароль
                    </span>
                  </LoadingButton>
                </Box>
              </form>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});

export default connect(mapStateToProps, {
  getRegions,
  updateProfile,
  changeEmail,
  changePassword,
})(withStyles(styles)(InfoSection));
