import React, { Fragment, useEffect } from "react";
import {
  Grid,
  Typography,
  Container,
  CircularProgress,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import YouTube from "react-youtube";
import { connect } from "react-redux";
import { getLives } from "../../actions/livesActions";
import { styles } from "./styles";

const LiveDrawsPage = ({ classes, getLives, lives: { lives, loading } }) => {
  useEffect(() => {
    getLives();
  }, []);
  const inLive = lives.filter((item) => {
    if (item.live === true) {
      return item;
    }
  });
  const videos = lives.filter((item) => {
    if (item.live === false) {
      return item;
    }
  });
  console.log("VIDEOS", videos);
  console.log("IN LIVE", inLive);
  const opts = { height: "100%", width: "100%" };
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      {loading ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
            marginTop: "140px",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Fragment>
          <Grid container className={classes.first_header_container}>
            <Grid item>
              <Typography className={classes.first_header}>
                ТРАНСЛЯЦИИ РОЗЫГРЫШЕЙ
              </Typography>
            </Grid>
            <Grid container style={{ justifyContent: "center" }}>
              <Grid
                item
                xs={10}
                style={{ display: "flex", justifyContent: "center" }}
              >
                {inLive[0] ? (
                  inLive.map((item) => (
                    <Grid item xs={12} style={{ height: "65vh" }}>
                      <YouTube
                        videoId={item.id_video}
                        opts={opts}
                        containerClassName={classes.youtube_top}
                      />
                      <div
                        style={{
                          color: "white",
                          padding: "10px",
                          fontSize: "18px",
                        }}
                      >
                        Розыгрыш{" "}
                        <span style={{ color: "#55a187" }}>{item.event}</span>
                      </div>
                    </Grid>
                  ))
                ) : (
                  <Typography>Нет текущих онлайн трансляций</Typography>
                )}
              </Grid>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item>
              <Typography className={classes.second_header}>
                ПРОШЛЫЕ ТРАНСЛЯЦИИ
              </Typography>
            </Grid>
          </Grid>
          {loading ? (
            <Grid
              item
              xs={12}
              style={{
                padding: "48px 0",
                display: "flex",
                justifyContent: "center",
                marginTop: "140px",
              }}
            >
              <CircularProgress color="secondary" size={100} />
            </Grid>
          ) : (
            <Container maxWidth="xl" style={{ padding: "0 100px" }}>
              <Grid container spacing={2}>
                {videos[0] ? (
                  videos.map((item) => {
                    return (
                      <Grid item xs={6} style={{ height: "53vh" }}>
                        <YouTube
                          videoId={item.id_video}
                          opts={opts}
                          containerClassName={classes.youtube}
                        />

                        <div
                          style={{
                            color: "black",
                            padding: "10px",
                            fontSize: "14px",
                          }}
                        >
                          Розыгрыш{" "}
                          <span style={{ color: "#55a187" }}>{item.event}</span>
                        </div>
                      </Grid>
                    );
                  })
                ) : (
                  <Grid item xs={12}>
                    Нет записей прошедших трансляций
                  </Grid>
                )}
              </Grid>
            </Container>
          )}
        </Fragment>
      )}
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  lives: state.lives,
});

export default connect(mapStateToProps, { getLives })(
  withStyles(styles)(LiveDrawsPage)
);
