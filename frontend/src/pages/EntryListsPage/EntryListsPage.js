import React, { useEffect, useState } from "react";
import {
  Grid,
  Typography,
  Container,
  CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { styles } from "./styles";
import { paginate } from "../../utils/paginate";
import Pagination from "../../components/common/Pagination/Pagination";
import { getEntryList } from "../../actions/eventsActions";

const EntryListsPage = ({
  classes,
  events: { entry_list, entry_loading },
  getEntryList,
}) => {
  console.log("entry_list", entry_list.length);
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 12;

  useEffect(() => {
    getEntryList();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const getPageData = () => {
    const paginationData = paginate(entry_list, currentPage, pageSize);
    return { totalCount: entry_list.length, data: paginationData };
  };

  const { totalCount, data } = getPageData();
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>СПИСКИ УЧАСТНИКОВ</Typography>
        </Grid>
      </Grid>
      {entry_loading ? (
        <Grid
          item
          xs={12}
          style={{
            padding: "48px 0",
            display: "flex",
            justifyContent: "center",
            marginTop: "50px",
          }}
        >
          <CircularProgress color="secondary" size={100} />
        </Grid>
      ) : (
        <Grid container style={{ margin: "0 52px", padding: "0 25px" }}>
          {data[0] &&
            data.map((item) => (
              <Grid item xs={4} className={classes.card_container}>
                <div className={classes.card_body}>
                  <div className={classes.card_title}>{item.name}</div>
                  <div className={classes.card_dates}>
                    Опубликован: {item.create_at.date}
                  </div>
                  <div className={classes.card_dates}>
                    Розыгран: {item.create_at.date}, {item.create_at.time}
                  </div>
                  <Link to={"/"} className={classes.card_button}>
                    Показать
                  </Link>
                </div>
              </Grid>
            ))}
          <Grid item xs={12}>
            <Pagination
              itemsCount={totalCount}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={handlePageChange}
            />
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  events: state.events,
});

export default connect(mapStateToProps, { getEntryList })(
  withStyles(styles)(EntryListsPage)
);
