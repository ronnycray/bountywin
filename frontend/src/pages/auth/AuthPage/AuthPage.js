import React, { useState } from "react";
import { Box, Container, Grid, TextField, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "./styles";
import LoadingButton from "@material-ui/lab/LoadingButton";
import { useFormik } from "formik";
import {
  loginValdationSchema,
  registerValdationSchema,
} from "../../../utils/validationSchemas";
import { Link } from "react-router-dom";
import CustomInput from "../../../components/common/CustomInput/CustomInput";
import CustomCheckbox from "../../../components/common/CustomCheckbox/CustomCheckbox";
import { connect } from "react-redux";
import { login, register } from "../../../actions/authActions";

const AuthPage = ({ classes, login, register }) => {
  const [loading, setLoading] = useState(false);
  const formikLogin = useFormik({
    initialValues: {
      username: "",
      password: "",
      isRemember: false,
    },
    validationSchema: loginValdationSchema,
    onSubmit: (values) => {
      console.log("LOGIN", values);
      login(values);
      //   changePassword(values);
    },
  });

  const formikRegister = useFormik({
    initialValues: {
      username: "",
      email: "",
      password: "",
    },
    validationSchema: registerValdationSchema,
    onSubmit: (values) => {
      console.log("REGISTER", values);
      register(values);
      //   changePassword(values);
    },
  });

  return (
    <Grid container>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>АВТОРИЗАЦИЯ</Typography>
        </Grid>
      </Grid>
      <Container>
        <Grid container>
          <Grid item xs={6} className={classes.login}>
            <Typography className={classes.title}>АВТОРИЗОВАТЬСЯ</Typography>
            <form onSubmit={formikLogin.handleSubmit}>
              <Box className={classes.login_card}>
                <CustomInput
                  label="Имя пользователя или E-mail"
                  labelColor="white"
                  isReq={true}
                  name="username"
                  type="text"
                  value={formikLogin.values.username}
                  onChange={formikLogin.handleChange}
                  error={
                    formikLogin.touched.username &&
                    Boolean(formikLogin.touched.username)
                  }
                  helperText={
                    formikLogin.touched.username && formikLogin.errors.email
                  }
                />
                <CustomInput
                  label="Пароль"
                  labelColor="white"
                  isReq={true}
                  name="password"
                  type="password"
                  value={formikLogin.values.password}
                  onChange={formikLogin.handleChange}
                  error={
                    formikLogin.touched.password &&
                    Boolean(formikLogin.touched.password)
                  }
                  helperText={
                    formikLogin.touched.password && formikLogin.errors.password
                  }
                />
                <CustomCheckbox
                  label="Запомнить меня"
                  name="isRemember"
                  labelColor="white"
                  checked={formikLogin.values.isRemember}
                  value={formikLogin.values.isRemember}
                  onChange={formikLogin.handleChange}
                />
                <LoadingButton
                  variant="contained"
                  className={classes.login_button}
                  pending={loading}
                  type="submit"
                  onClick={formikLogin.handleSubmit}
                >
                  <span className={classes.login_button_text}>Войти</span>
                </LoadingButton>
                <Box style={{ width: "100%", margin: "16px 0 16px 1px" }}>
                  <Link to="/forgot-password" className={classes.login_link}>
                    Забыли пароль?
                  </Link>
                </Box>
              </Box>
            </form>
          </Grid>
          <Grid item xs={6} className={classes.register}>
            <Typography className={classes.title}>
              ЗАРЕГИСТРИРОВАТЬСЯ
            </Typography>
            <form onSubmit={formikRegister.handleSubmit}>
              <Box className={classes.register_card}>
                <CustomInput
                  label="Имя пользователя"
                  labelColor="black"
                  isReq={true}
                  name="username"
                  type="text"
                  value={formikRegister.values.username}
                  onChange={formikRegister.handleChange}
                  error={
                    formikRegister.touched.username &&
                    Boolean(formikRegister.touched.username)
                  }
                  helperText={
                    formikRegister.touched.username &&
                    formikRegister.errors.username
                  }
                />
                <CustomInput
                  label="E-mail"
                  labelColor="black"
                  isReq={true}
                  name="email"
                  type="email"
                  value={formikRegister.values.email}
                  onChange={formikRegister.handleChange}
                  error={
                    formikRegister.touched.email &&
                    Boolean(formikRegister.touched.email)
                  }
                  helperText={
                    formikRegister.touched.email && formikRegister.errors.email
                  }
                />
                <CustomInput
                  label="Пароль"
                  labelColor="black"
                  isReq={true}
                  name="password"
                  type="password"
                  value={formikRegister.values.password}
                  onChange={formikRegister.handleChange}
                  error={
                    formikRegister.touched.password &&
                    Boolean(formikRegister.touched.password)
                  }
                  helperText={
                    formikRegister.touched.password &&
                    formikRegister.errors.password
                  }
                />
                <Box
                  style={{ width: "100%", marginTop: "6px", fontSize: "16px" }}
                >
                  Ваши персональные данные будут использоваться для поддержки
                  вашего опыта работы на этом веб-сайте, для управления доступом
                  к вашей учетной записи и для других целей, описанных в нашей{" "}
                  <Link to="/privacy-policy" className={classes.register_link}>
                    политике конфиденциальности
                  </Link>
                  .
                </Box>
                <LoadingButton
                  variant="contained"
                  className={classes.register_button}
                  pending={loading}
                  type="submit"
                  onClick={formikRegister.handleSubmit}
                >
                  <span className={classes.register_button_text}>
                    Зарегестрироваться
                  </span>
                </LoadingButton>
              </Box>
            </form>
          </Grid>
        </Grid>
      </Container>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  auth: state.authReducer,
});

export default connect(mapStateToProps, { login, register })(
  withStyles(styles)(AuthPage)
);
