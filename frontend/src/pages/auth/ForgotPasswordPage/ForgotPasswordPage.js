import React, { useState } from "react";
import { Grid, Typography, Container } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { forgotPassword } from "../../../actions/authActions";
import { styles } from "./styles";
import CustomInput from "../../../components/common/CustomInput";
import LoadingButton from "@material-ui/lab/LoadingButton";

const ForgotPasswordPage = ({ classes, auth: { loading }, forgotPassword }) => {
  const [email, setEmail] = useState("");
  const onChangeHandler = (e) => {
    setEmail(e.target.value);
  };
  console.log("email", email);
  return (
    <Grid container style={{ paddingBottom: "110px" }}>
      <Grid container>
        <Grid item>
          <Typography className={classes.header}>АВТОРИЗАЦИЯ</Typography>
        </Grid>
      </Grid>
      <Container>
        <Typography style={{ marginBottom: "20px" }}>
          Забыли пароль? Пожалуйста, введите свой адрес электронной почты. Вы
          получите ссылку для создания нового пароля по электронной почте.
        </Typography>
        <Grid item xs={6}>
          <CustomInput
            label="E-mail"
            labelColor="black"
            name="email"
            type="email"
            value={email}
            onChange={(e) => onChangeHandler(e)}
          />
          <LoadingButton
            variant="contained"
            style={{ marginTop: "15px" }}
            pending={loading}
            onClick={() => forgotPassword({ email })}
          >
            Сбросить пароль
          </LoadingButton>
        </Grid>
      </Container>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  auth: state.authReducer,
});

export default connect(mapStateToProps, { forgotPassword })(
  withStyles(styles)(ForgotPasswordPage)
);
