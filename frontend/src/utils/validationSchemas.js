import * as Yup from "yup";

export const changePasswordValidationSchema = Yup.object({
  password: Yup.string()
    .min(8, "Новый пароль должен быть не менее 8 символов")
    .required("Необходимо ввести новый пароль."),
  confirm_password: Yup.string()
    .required("Необходимо ввести новый пароль еще раз.")
    .test("passwords-match", "Пароли должны совпадать", function (value) {
      return this.parent.password === value;
    }),
  old_password: Yup.string()
    .required("Необходимо ввести старый пароль.")
    .test(
      "passwords-match",
      "Старый пароль и новый пароль не должны совпадать.",
      function (value) {
        return this.parent.password !== value;
      }
    ),
});

export const loginValdationSchema = Yup.object({
  username: Yup.string().required(
    "Необходимо ввести имя пользователя или e-mail."
  ),
  password: Yup.string().required("Необходимо ввести пароль."),
});

export const registerValdationSchema = Yup.object({
  username: Yup.string().required("Необходимо ввести имя пользователя"),
  email: Yup.string()
    .email("Некорректно введен e-mail")
    .required("Необходимо ввести e-mail."),
  password: Yup.string()
    .required("Необходимо ввести пароль.")
    .min(8, "Пароль слишком короткий - должен быть не менее 8 символов."),
});

export const updateProfileValidationSchema = Yup.object({
  first_name: Yup.string().required("Необходимо ввести имя"),
  last_name: Yup.string().required("Необходимо ввести фамилию"),
  company_name: Yup.string(),
  address: Yup.string().required("Необходимо ввести адрес"),
  postal: Yup.number().required("Необходимо ввести индекс"),
  phone: Yup.number().required("Необходимо ввести номер телефона"),
  date_of_birth: Yup.date(),
});

export const changeEmailValidationSchema = Yup.object({
  email: Yup.string().required("Необходимо ввести e-mail."),
});
