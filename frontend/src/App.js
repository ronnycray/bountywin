import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
// import ChangePassword from "./pages/auth/ChangePassword";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { connect } from "react-redux";
import Header from "./components/layout/Header";
import "./App.css";
import HomePage from "./pages/HomePage";
import HowItWorksPage from "./pages/HowItWorksPage";
import Footer from "./components/layout/Footer";
import AuthPage from "./pages/auth/AuthPage";
import ForgotPasswordPage from "./pages/auth/ForgotPasswordPage";
import WinnersPage from "./pages/WinnersPage/WinnersPage";
import LatestCompetitionsPage from "./pages/LatestCompetitionsPage/LatestCompetitionsPage";
import EntryListsPage from "./pages/EntryListsPage/EntryListsPage";
import LiveDrawsPage from "./pages/LiveDrawsPage/LiveDrawsPage";
import CartPage from "./pages/CartPage";
import { getLastEvents, getAllEvents } from "./actions/eventsActions";
import { getStatistics } from "./actions/statisticsActions";
import { getWinners } from "./actions/winnersActions";
import "@brainhubeu/react-carousel/lib/style.css";
import CompetitionPage from "./pages/CompetitionPage/CompetitionPage";
import Alert from "./components/common/Alert/Alert";
import PrivateRoute from "./components/common/Route/PrivateRoute";
import ProfilePage from "./pages/ProfilePage";
import LoginRoute from "./components/common/Route/LoginRoute";
import ProfileRoute from "./components/common/Route/ProfileRoute/ProfileRoute";
import InfoSection from "./pages/ProfilePage/sections/Info/InfoSection";
import EntrieSection from "./pages/ProfilePage/sections/Entries/EntrieSection";
import MainSection from "./pages/ProfilePage/sections/MainSection/MainSection";
import OrdersSection from "./pages/ProfilePage/sections/Orders/OrdersSection";
import PointsSection from "./pages/ProfilePage/sections/Points/PointsSection";
import OrderPage from "./pages/OrderPage/OrderPage";
import PastCompetitionsPage from "./pages/PastCompetitionsPage/PastCompetitionsPage";

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: "#55A187",
    },
    primary: {
      main: "#1c1b21",
    },
  },
});

const App = ({ getLastEvents, getAllEvents, getStatistics, getWinners }) => {
  useEffect(() => {
    getLastEvents();
    getStatistics();
    getAllEvents();
    getWinners();
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <CssBaseline />
        <Header />
        <main className="app">
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/how-it-works" component={HowItWorksPage} />
            <LoginRoute exact path="/login" component={AuthPage} />
            <Route
              exact
              path="/forgot-password"
              component={ForgotPasswordPage}
            />
            <Route exact path="/competition-winners" component={WinnersPage} />
            <Route
              exact
              path="/latest-competitions"
              component={LatestCompetitionsPage}
            />
            <Route
              exact
              path="/past-competitions"
              component={PastCompetitionsPage}
            />
            <Route exact path="/entry-lists" component={EntryListsPage} />
            <Route exact path="/live-draws" component={LiveDrawsPage} />
            <Route exact path="/cart" component={CartPage} />
            <Route exact path="/competition/:id" component={CompetitionPage} />
            <ProfileRoute exact path="/profile" component={MainSection} />
            <ProfileRoute
              exact
              path="/profile/account"
              component={InfoSection}
            />
            <ProfileRoute
              exact
              path="/profile/orders"
              component={OrdersSection}
            />
            <ProfileRoute
              exact
              path="/profile/points"
              component={PointsSection}
            />
            <ProfileRoute
              exact
              path="/profile/entries"
              component={EntrieSection}
            />
            <PrivateRoute exact path="/order" component={OrderPage} />
          </Switch>
        </main>
        <Footer />
        <Alert />
      </Router>
    </ThemeProvider>
  );
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {
  getLastEvents,
  getAllEvents,
  getStatistics,
  getWinners,
})(App);
